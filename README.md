# Building

To build CelOS, you need to have Rust nightly (at least 1.68.0), rustfmt 2.0 (you will need to build it from source), cargo-make, nasm, and ld installed. To build a bootable ISO:

`cargo make iso`

# Running

To run, you can use `cargo make run` to launch QEMU with a freshly built bootable ISO, or launch your preferred VM software of choice.

# Contributing

While I don't expect this is of interest to anyone except myself, contributions are welcome if anyone wants.
