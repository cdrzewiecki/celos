# CelOS Design Goals

* Microkernel architecture
* All objects in the OS are Rust objects (like Powershell makes everything an object). Stupid? idk
* Learning experience
* Multi-user
* Everything is a URI, examples (not exhaustive):
    * Devices should be at dev://
    * Processes should be at proc://
    * file:// namespace for accessing files, perhaps with mapping posix "files" into URIs behind the scenes so it's easier to port apps
* In the shell you should be able to click things to launch GUI operations. Ideas for this:
    * Click on the path displayed in the shell prompt, a file browser opens up
    * Click on files in the output from ls (or whatever it gets called) to launch an application that is associated with that file type
    * Maybe breadcrumb in the shell so you can click up levels, but then again maybe not

# 1.0 requirements

* Boot on x86_64 hardware
* Able to display text and graphics to a graphics adapter
* Networking stack
* CLI
* Pre-emptive multitasking
* Memory management
* Filesystem of some kind, probably not something I invent