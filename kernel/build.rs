use std::{env, fs, path::PathBuf};

const TRAMPOLINE_NAME: &str = "trampoline";
const KERNEL_ASM_NAME: &str = "kernel-asm";

fn main() {
  #[cfg(target_arch = "x86_64")]
  {
    let arch_src_dir = format!("{}/src/arch/x86_64", env::var("CARGO_MANIFEST_DIR").unwrap());

    println!("cargo:rustc-link-arg=-n");
    println!("cargo:rustc-link-arg=--no-gc-sections");
    println!("cargo:rustc-link-arg=--no-pie");
    println!("cargo:rustc-link-arg=--script={}/linker.ld", arch_src_dir);
    println!("cargo:rustc-link-lib=static:-bundle,+whole-archive={}", TRAMPOLINE_NAME);
    println!("cargo:rustc-link-lib=static:-bundle,+whole-archive={}", KERNEL_ASM_NAME);

    let src_files = || {
      fs::read_dir(&arch_src_dir)
        .unwrap()
        .filter_map(|entry| entry.ok())
        .filter(|entry| entry.path().is_file())
        .filter(|entry| entry.path().extension().unwrap() == "asm")
    };
    let trampoline_files: Vec<PathBuf> = src_files()
      .filter(|entry| {
        let path = entry.path();
        let file_name = path.file_name().unwrap().to_string_lossy();
        file_name.starts_with("trampoline") || file_name.starts_with("multiboot_header")
      })
      .map(|entry| entry.path())
      .collect();

    let other_files: Vec<PathBuf> = src_files()
      .filter(|entry| {
        let path = entry.path();
        let file_name = path.file_name().unwrap().to_string_lossy();
        !file_name.starts_with("trampoline")
          && !file_name.starts_with("constants")
          && !file_name.starts_with("multiboot_header")
      })
      .map(|entry| entry.path())
      .collect();

    for file in &trampoline_files {
      println!("cargo:rerun-if-changed={}", file.display());
    }

    for file in &other_files {
      println!("cargo:rerun-if-changed={}", file.display());
    }
    println!("cargo:rerun-if-changed={}/constants.asm", arch_src_dir);
    println!("cargo:rerun-if-changed={}/linker.ld", arch_src_dir);

    let include_flag = format!("-i {}", arch_src_dir);
    let format_flag = "-f elf64";

    nasm_rs::compile_library_args(TRAMPOLINE_NAME, trampoline_files.as_slice(), &[&include_flag, format_flag]).unwrap();
    nasm_rs::compile_library_args(KERNEL_ASM_NAME, other_files.as_slice(), &[&include_flag, format_flag]).unwrap();
  }
}
