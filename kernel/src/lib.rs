#![no_std]
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]
#![feature(let_chains)]
#![warn(clippy::pedantic)]
#![allow(clippy::doc_markdown)]
#![allow(clippy::manual_let_else)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::must_use_candidate)]
#![allow(clippy::similar_names)]
#![allow(clippy::single_match_else)]
#![cfg_attr(debug_assertions, allow(clippy::wildcard_imports))]
#![cfg_attr(debug_assertions, allow(dead_code))]

extern crate alloc;

pub mod arch;
mod concurrency;
pub mod cpu;
pub mod interrupts;
pub mod memory;
pub mod multitasking;
pub mod serial;
pub mod util;
pub mod vga_buffer;
