pub use crate::arch::memory::*;

pub mod region;

#[derive(Clone, Copy, Debug)]
pub enum BitmapError {
  AlreadyInitialized,
  NotInitialized,
  BadFrameSize,
  BadRegionSize,
  BaseAddressNotAligned,
  FrameOutsideBitmap,
  MaxAddressTooLarge,
}
