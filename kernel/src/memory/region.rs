use core::{
  fmt,
  ops::{Add, Div, Mul, Rem},
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[repr(usize)]
pub enum RegionSize {
  Size1GB = 0x4000_0000,
  Size2MB = 0x20_0000,
  Size32KB = 0x8000,
  Size16KB = 0x4000,
  Size8KB = 0x2000,
  Size4KB = 0x1000,
}

impl RegionSize {
  pub const fn all_sizes() -> [Self; 6] {
    [
      RegionSize::Size1GB,
      RegionSize::Size2MB,
      RegionSize::Size32KB,
      RegionSize::Size16KB,
      RegionSize::Size8KB,
      RegionSize::Size4KB,
    ]
  }

  pub const fn largest_4kb_region() -> Self {
    RegionSize::Size32KB
  }

  #[allow(clippy::trivially_copy_pass_by_ref)]
  #[must_use]
  pub fn smaller(&self) -> Self {
    match *self {
      RegionSize::Size1GB => RegionSize::Size2MB,
      RegionSize::Size2MB => RegionSize::Size32KB,
      RegionSize::Size32KB => RegionSize::Size16KB,
      RegionSize::Size16KB => RegionSize::Size8KB,
      RegionSize::Size8KB | RegionSize::Size4KB => RegionSize::Size4KB,
    }
  }

  #[allow(clippy::trivially_copy_pass_by_ref)]
  #[must_use]
  pub fn larger(&self) -> Self {
    match *self {
      RegionSize::Size2MB | RegionSize::Size1GB => RegionSize::Size1GB,
      RegionSize::Size32KB => RegionSize::Size2MB,
      RegionSize::Size16KB => RegionSize::Size32KB,
      RegionSize::Size8KB => RegionSize::Size16KB,
      RegionSize::Size4KB => RegionSize::Size8KB,
    }
  }
}

impl fmt::Display for RegionSize {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let size = match self {
      Self::Size1GB => "1 GB",
      Self::Size2MB => "2 MB",
      Self::Size32KB => "32 KB",
      Self::Size16KB => "16 KB",
      Self::Size8KB => "8 KB",
      Self::Size4KB => "4 KB",
    };
    f.write_str(size)
  }
}

impl Add<RegionSize> for u64 {
  type Output = Self;

  fn add(self, rhs: RegionSize) -> Self::Output {
    self + rhs as u64
  }
}

impl Rem<RegionSize> for u64 {
  type Output = Self;

  fn rem(self, modulus: RegionSize) -> Self::Output {
    self % modulus as u64
  }
}

impl Rem<RegionSize> for usize {
  type Output = Self;

  fn rem(self, modulus: RegionSize) -> Self::Output {
    self % modulus as usize
  }
}

impl Mul<RegionSize> for u64 {
  type Output = Self;

  fn mul(self, rhs: RegionSize) -> Self::Output {
    self * rhs as u64
  }
}

impl Mul<RegionSize> for usize {
  type Output = Self;

  fn mul(self, rhs: RegionSize) -> Self::Output {
    self * rhs as usize
  }
}

impl Mul<u64> for RegionSize {
  type Output = u64;

  fn mul(self, rhs: u64) -> Self::Output {
    self as u64 * rhs
  }
}

impl Div<RegionSize> for u64 {
  type Output = Self;

  fn div(self, denominator: RegionSize) -> Self::Output {
    self / denominator as u64
  }
}

impl Div<RegionSize> for usize {
  type Output = Self;

  fn div(self, denominator: RegionSize) -> Self::Output {
    self / denominator as usize
  }
}

impl From<RegionSize> for usize {
  fn from(other: RegionSize) -> Self {
    other as usize
  }
}

impl From<RegionSize> for u64 {
  fn from(other: RegionSize) -> Self {
    other as u64
  }
}
