use core::{convert::TryFrom, fmt};

use lazy_static::lazy_static;
use multiboot2::{FramebufferTag, FramebufferType};
use rusttype::{point, Font, IntoGlyphId, Scale};
use spin::Mutex;
use volatile::Volatile;
use x86_64::{instructions::interrupts, VirtAddr};

use crate::arch::memory;

#[allow(dead_code)]
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EGAColor {
  Black = 0,
  Blue = 1,
  Green = 2,
  Cyan = 3,
  Red = 4,
  Magenta = 5,
  Brown = 6,
  LightGray = 7,
  DarkGray = 8,
  LightBlue = 9,
  LightGreen = 10,
  LightCyan = 11,
  LightRed = 12,
  Pink = 13,
  Yellow = 14,
  White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct EGAColorCode(u8);

impl EGAColorCode {
  const fn new(foreground: EGAColor, background: EGAColor) -> EGAColorCode {
    EGAColorCode((background as u8) << 4 | (foreground as u8))
  }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct TextModeChar {
  ascii_character: u8,
  color_code: EGAColorCode,
}

const TEXT_BUFFER_HEIGHT: usize = 25;
const TEXT_BUFFER_WIDTH: usize = 80;
const DEFAULT_TAB_SIZE: u8 = 5;
static mut BUFFER_TYPE: BufferType = BufferType::Text;
static BUFFER: Mutex<Buffer> = Mutex::new(Buffer::new());
lazy_static! {
  static ref WRITER: Mutex<PixelModeWriter> = Mutex::new(PixelModeWriter::new());
}

const CONSOLE_FONT_SIZE: f32 = 16.0;
const CONSOLE_FONT_DATA: &[u8] = include_bytes!("../../resources/fonts/Inconsolata-Bold.ttf") as &[u8];

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum BufferType {
  Text,
  Rgb,
}

/// Initializes the VGA driver.
///
/// # Panics
/// This function panics if the framebuffer from GRUB is not either a text or RGB framebuffer.
///
/// # Safety
/// `framebuffer_tag` must be a reference to a valid framebuffer tag from GRUB.
#[allow(clippy::match_wildcard_for_single_variants)]
pub unsafe fn init(framebuffer_tag: &FramebufferTag) {
  BUFFER_TYPE = match framebuffer_tag.buffer_type {
    FramebufferType::Text => BufferType::Text,
    FramebufferType::RGB { .. } => BufferType::Rgb,
    _ => panic!("Unsupported framebuffer type: {:?}", framebuffer_tag.buffer_type),
  };

  if BUFFER_TYPE == BufferType::Rgb {
    let address = memory::PHYSICAL_MEMORY_OFFSET + framebuffer_tag.address;

    interrupts::without_interrupts(|| {
      BUFFER.lock().init(
        address,
        framebuffer_tag.width,
        framebuffer_tag.height,
        framebuffer_tag.pitch,
        framebuffer_tag.bpp,
      );
    });

    interrupts::without_interrupts(|| {
      WRITER.lock().init();
    });
  }
}

#[repr(transparent)]
struct TextBuffer {
  chars: [[Volatile<TextModeChar>; TEXT_BUFFER_WIDTH]; TEXT_BUFFER_HEIGHT],
}

pub struct Buffer {
  addr: Option<VirtAddr>,
  width: u32,
  height: u32,
  pitch: u32,
  bpp: u8,
}

impl Buffer {
  pub const fn new() -> Self {
    Buffer { addr: None, width: 0, height: 0, pitch: 0, bpp: 0 }
  }

  /// Initializes the buffer object.
  ///
  /// # Safety
  /// The caller must guarantee that the address for the buffer is valid. Don't come crying to me
  /// if you give the wrong address.
  pub unsafe fn init(&mut self, addr: VirtAddr, width: u32, height: u32, pitch: u32, bpp: u8) {
    self.addr = Some(addr);
    self.width = width;
    self.height = height;
    self.pitch = pitch;
    self.bpp = bpp;
  }

  pub fn put_pixel(&mut self, x: u32, y: u32, color: u32) {
    let addr = self.addr.expect("Attempt to write to an uninitialized framebuffer!");
    let pixel_offset = y * self.pitch + x * (u32::from(self.bpp) / 8);
    let mut pixel_address = usize::try_from(addr.as_u64()).expect("Tried to run on a 32-bit target");
    pixel_address += pixel_offset as usize;
    unsafe {
      (pixel_address as *mut u32).write_volatile(color);
    }
  }

  pub fn shift_up(&mut self, rows: u32) {
    let base_ptr: *mut u8 = self.addr.expect("Attempt to write to an uninitialized framebuffer!").as_mut_ptr();

    let last_row = self.height - rows;
    for y in 0..last_row - 1 {
      unsafe {
        let dest = base_ptr.add(y as usize * self.pitch as usize);
        let src = base_ptr.add((y + rows) as usize * self.pitch as usize);
        let num_bytes = self.pitch * (u32::from(self.bpp) / 8);
        core::ptr::copy_nonoverlapping(src, dest, num_bytes as usize);
      }
    }

    for y in last_row..self.height {
      self.clear_row(y);
    }
  }

  pub fn clear_row(&mut self, y: u32) {
    let base_ptr: *mut u8 = self.addr.expect("Attempt to write to an uninitialized framebuffer!").as_mut_ptr();
    let y_offset = y * self.pitch;

    unsafe {
      core::ptr::write_bytes(base_ptr.add(y_offset as usize), 0, self.pitch as usize);
    }
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct TextColor {
  fore_color: u32,
  back_color: u32,
}

struct PixelModeWriter {
  column_position: u16,
  row_position: u16,
  max_column: u16,
  max_row: u16,
  color: Option<TextColor>,
  tab_size: u8,
  font_scale: Scale,
  char_height: u32,
}

impl PixelModeWriter {
  pub fn new() -> Self {
    PixelModeWriter {
      column_position: 0,
      row_position: 0,
      max_column: 0,
      max_row: 0,
      color: None,
      tab_size: DEFAULT_TAB_SIZE,
      font_scale: Scale::uniform(CONSOLE_FONT_SIZE),
      char_height: 0,
    }
  }

  #[allow(clippy::cast_possible_truncation)]
  #[allow(clippy::cast_sign_loss)]
  pub fn init(&mut self) {
    let font = Font::try_from_bytes(CONSOLE_FONT_DATA).unwrap();
    let v_metrics = font.v_metrics(self.font_scale);
    let char_height = (v_metrics.ascent - v_metrics.descent) as u32;
    self.char_height = char_height;

    // Since we are using a monospace font, we can pick any character to get the width
    let glyph = font.glyph('x'.into_glyph_id(&font)).scaled(self.font_scale);
    let char_width = glyph.h_metrics().advance_width;

    interrupts::without_interrupts(|| {
      #[allow(clippy::cast_precision_loss)]
      let float_max_column = BUFFER.lock().width as f32 / char_width;
      let max_column = (float_max_column + 0.5) as u32;
      self.max_column = u16::try_from(max_column).unwrap_or(u16::MAX);
      self.max_row = u16::try_from(BUFFER.lock().height / char_height).unwrap_or(u16::MAX);
    });
  }

  #[allow(dead_code)]
  pub fn set_color(&mut self, color: TextColor) {
    self.color = Some(color);
  }

  #[allow(clippy::cast_possible_truncation)]
  #[allow(clippy::cast_sign_loss)]
  #[allow(clippy::unreadable_literal)]
  pub fn write_char(&mut self, char: char) {
    let color = match self.color {
      Some(color) => color,
      _ => TextColor { fore_color: 0xffffff, back_color: 0x0 },
    };

    if self.column_position >= self.max_column {
      self.new_line();
    }
    match char {
      '\n' => self.new_line(),
      '\t' => self.tab(),
      '\r' => {}
      _ => {
        let font = Font::try_from_bytes(CONSOLE_FONT_DATA).unwrap();
        let v_metrics = font.v_metrics(self.font_scale);
        let y = f32::from(self.row_position) * (v_metrics.ascent - v_metrics.descent) + v_metrics.ascent;

        let glyph = font.glyph(char.into_glyph_id(&font)).scaled(self.font_scale);
        let x = f32::from(self.column_position) * glyph.h_metrics().advance_width;
        let glyph = glyph.positioned(point(x, y));
        if let Some(bounding_box) = glyph.pixel_bounding_box() {
          glyph.draw(|x, y, v| {
            // For some reason sometimes we get a "v" value back from the rusttype lib that is greater than 1.0, but it must not be > 1.0.
            let v = if v > 1.0 { 1.0 } else { v };
            let alpha = (v * 255.0) as u8;
            let fore_red = ((color.fore_color & 0xff0000) >> 16) as u8;
            let fore_green = ((color.fore_color & 0xff00) >> 8) as u8;
            let fore_blue = (color.fore_color & 0xff) as u8;
            let back_red = ((color.back_color & 0xff0000) >> 16) as u8;
            let back_green = ((color.back_color & 0xff00) >> 8) as u8;
            let back_blue = (color.back_color & 0xff) as u8;
            let blended_red = (1.0 - v) * f32::from(back_red) + v * f32::from(fore_red);
            let blended_green = (1.0 - v) * f32::from(back_green) + v * f32::from(fore_green);
            let blended_blue = (1.0 - v) * f32::from(back_blue) + v * f32::from(fore_blue);
            let final_color = ((u32::from(alpha)) << 24)
              + ((blended_red as u32) << 16)
              + ((blended_green as u32) << 8)
              + blended_blue as u32;
            BUFFER.lock().put_pixel(
              x + u32::try_from(bounding_box.min.x).unwrap_or(0),
              y + u32::try_from(bounding_box.min.y).unwrap_or(0),
              final_color,
            );
          });
        }
        self.column_position += 1;
      }
    }
  }

  pub fn write_string(&mut self, s: &str) {
    for char in s.chars() {
      self.write_char(char);
    }
  }

  fn new_line(&mut self) {
    self.column_position = 0;
    if self.row_position >= self.max_row - 1 {
      interrupts::without_interrupts(|| {
        BUFFER.lock().shift_up(self.char_height);
      });
      self.row_position = self.max_row - 1;
    } else {
      self.row_position += 1;
    }
  }

  fn tab(&mut self) {
    if self.column_position + 1 < self.max_column {
      self.column_position += 1;
    }
    while self.column_position % u16::from(self.tab_size) != 0 {
      if self.column_position + 1 > self.max_column {
        self.column_position = self.max_column;
        break;
      }
      self.column_position += 1;
    }
    self.column_position += 1;
  }
}

impl fmt::Write for PixelModeWriter {
  fn write_str(&mut self, s: &str) -> fmt::Result {
    self.write_string(s);
    Ok(())
  }
}

pub struct TextModeWriter {
  column_position: usize,
  row_position: usize,
  color_code: EGAColorCode,
  buffer: &'static mut TextBuffer,
}

impl TextModeWriter {
  pub fn write_byte(&mut self, byte: u8) {
    match byte {
      b'\n' => self.new_line(),
      byte => {
        if self.column_position >= TEXT_BUFFER_WIDTH {
          self.new_line();
        }

        let row = self.row_position;
        let col = self.column_position;

        let color_code = self.color_code;
        self.buffer.chars[row][col].write(TextModeChar { ascii_character: byte, color_code });
        self.column_position += 1;
      }
    }
  }

  pub fn write_string(&mut self, s: &str) {
    for byte in s.bytes() {
      match byte {
        // printable ASCII byte or newline
        0x20..=0x7e | b'\n' => self.write_byte(byte),
        b'\t' => self.tab(),
        // not part of printable ASCII range
        _ => self.write_byte(0xfe),
      }
    }
  }

  fn tab(&mut self) {
    if self.column_position < TEXT_BUFFER_WIDTH {
      self.column_position += 1;
    }
    while self.column_position % DEFAULT_TAB_SIZE as usize != 0 {
      if self.column_position + 1 >= TEXT_BUFFER_WIDTH {
        self.column_position = TEXT_BUFFER_WIDTH;
        break;
      }
      self.column_position += 1;
    }
    self.column_position += 1;
  }

  fn new_line(&mut self) {
    if self.row_position == TEXT_BUFFER_HEIGHT - 1 {
      for row in 1..TEXT_BUFFER_HEIGHT {
        for col in 0..TEXT_BUFFER_WIDTH {
          let character = self.buffer.chars[row][col].read();
          self.buffer.chars[row - 1][col].write(character);
        }
      }

      self.clear_row(TEXT_BUFFER_HEIGHT - 1);
    } else {
      self.row_position += 1;
    }

    self.column_position = 0;
  }

  fn clear_row(&mut self, row: usize) {
    let blank = TextModeChar { ascii_character: b' ', color_code: self.color_code };
    for col in 0..TEXT_BUFFER_WIDTH {
      self.buffer.chars[row][col].write(blank);
    }
  }
}

impl fmt::Write for TextModeWriter {
  fn write_str(&mut self, s: &str) -> fmt::Result {
    self.write_string(s);
    Ok(())
  }
}

lazy_static! {
  pub static ref TEXT_MODE_WRITER: Mutex<TextModeWriter> = Mutex::new(TextModeWriter {
    column_position: 0,
    row_position: 0,
    color_code: EGAColorCode::new(EGAColor::White, EGAColor::Black),
    buffer: unsafe { &mut *(0xb8000 as *mut TextBuffer) },
  });
}

#[macro_export]
macro_rules! print {
  ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
  () => ($crate::print!("\n"));
  ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
  use core::fmt::Write;
  let buffer_type = unsafe { BUFFER_TYPE };
  if buffer_type == BufferType::Text {
    interrupts::without_interrupts(|| {
      TEXT_MODE_WRITER.lock().write_fmt(args).unwrap();
    });
  } else {
    interrupts::without_interrupts(|| {
      WRITER.lock().write_fmt(args).expect("Printing to VGA output failed");
    });
  }
}
