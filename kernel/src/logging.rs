use log::{Level, Log, Metadata, Record};

use crate::{concurrency::Locked, println, serial_println};

pub struct KernelLogger {
  use_serial: bool,
  use_console: bool,
  enabled_level: Level,
}

impl KernelLogger {
  pub const fn new() -> Self {
    Self {
      use_serial: false,
      use_console: false,
      enabled_level: Level::Info,
    }
  }

  pub fn enable_serial(&mut self) {
    self.use_serial = true;
  }

  #[allow(dead_code)]
  pub fn disable_serial(&mut self) {
    self.use_serial = false;
  }

  pub fn enable_console(&mut self) {
    self.use_console = true;
  }

  #[allow(dead_code)]
  pub fn disable_console(&mut self) {
    self.use_console = false;
  }

  pub fn set_level(&mut self, level: Level) {
    self.enabled_level = level;
  }
}

impl Log for KernelLogger {
  fn enabled(&self, metadata: &Metadata) -> bool {
    (self.use_console || self.use_serial) && metadata.level() <= self.enabled_level
  }

  fn log(&self, record: &Record) {
    if record.level() <= self.enabled_level {
      if self.use_serial {
        serial_println!("{} - {}", record.level(), record.args());
      }
      if self.use_console {
        println!("{} - {}", record.level(), record.args());
      }
    }
  }

  fn flush(&self) {}
}

impl Log for Locked<KernelLogger> {
  fn enabled(&self, metadata: &Metadata) -> bool {
    let logger = self.lock();
    (logger.use_console || logger.use_serial) && metadata.level() <= logger.enabled_level
  }

  fn log(&self, record: &Record) {
    x86_64::instructions::interrupts::without_interrupts(|| {
      let logger = self.lock();

      if record.level() <= logger.enabled_level {
        if logger.use_serial {
          serial_println!("{} - {}", record.level(), record.args());
        }
        if logger.use_console {
          println!("{} - {}", record.level(), record.args());
        }
      }
    });
  }

  fn flush(&self) {}
}
