pub use crate::arch::cpu::cpu_index;

pub fn init() {
  crate::arch::cpu::init();
}
