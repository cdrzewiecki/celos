use core::time::Duration;

pub const SECOND: Duration = Duration::from_secs(1);
