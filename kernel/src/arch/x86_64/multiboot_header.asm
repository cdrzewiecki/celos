section .multiboot_header
header_start:
  dd 0xe85250d6                                                   ; magic number for multiboot v2
  dd 0                                                            ; architecture 0 (protected mode i386)
  dd header_end - header_start                                    ; header length
  dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start)) ; checksum

  ; insert optional multiboot tags here
  ; NOTE: if a tag does not contain a multiple of 8 bytes, you have to align 8 the next tag

  ; framebuffer tag
  dw 5 ; type = framebuffer
  dw 0 ; flags
  dd 20 ; size in bytes - this isn't a multiple of 8, so remember to align the next tag!
  dd 1024 ; width
  dd 768 ; height
  dd 32 ; color depth

  ; required end tag
  align 8
  dw 0                                                            ; type
  dw 0                                                            ; flags
  dd 8                                                            ; size
header_end:
