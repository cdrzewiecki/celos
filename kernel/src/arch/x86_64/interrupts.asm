section .text
bits 64

global xapic_start_compare_pit
; Rust declaration of function:
; extern "C" {
;   fn xapic_start_compare_pit(count: u16, reg_ptr: *mut u32);
; }

global x2apic_start_compare_pit
; Rust declaration of function:
; extern "C" {
;   fn x2apic_start_compare_pit(count: u16);
; }

global tsc_start_compare_pit
; Rust function declaration:
; extern "C" {
;   fn tsc_start_compare_pit(count: u16);
; }

nop ; for some reason if this nop doesn't appear, the disassembly assigns wrong labels to things
xapic_start_compare_pit:
  ; Initialize timer using PIT registers
  mov al, 0b00110000 ; channel 0, low-byte/high-byte mode, one-shot timer
  out 0x43, al
  ; Write first byte of count value
  mov ax, di
  out 0x40, al
  in al, 0x80 ; brief delay
  ; Write second byte of count value
  mov al, ah
  out 0x40, al

  ; write timer initial count to MMIO APIC register to start timer
  mov dword [rsi], 0xffffffff
  ret
x2apic_start_compare_pit:
  ; set MSR value for APIC timer initial count register
  mov ecx, 0x838
  mov edx, 0 ; clear edx because it is used by wrmsr later

  ; Initialize timer using PIT registers
  mov al, 0b00110000 ; This sets a one-shot timer on channel 0 (which in turn fires IRQ0) with counter to be set using low-byte/high-bite mode
  out 0x43, al ; Write timer mode to PIT mode/command register

  ; Move value passed to function (rdi, but since it's a 16-bit value we read di) to ax
  mov ax, di
  out 0x40, al ; Write the low byte of ax to channel 0
  in al, 0x80 ; Insert a brief delay
  mov al, ah ; Copy the high bytes of ax to al so we can write them
  out 0x40, al ; Now write the high byte of ax to channel 0

  ; Start local APIC timer by writing the initial counter value, and return
  mov eax, 0xffffffff
  wrmsr
  ret

tsc_start_compare_pit:
  ; Write values to setup PIT timer to calibrate against
  mov al, 0b00110000 ; channel 0, low-byte/high-byte, one-shot
  out 0x43, al ; write mode to PIT
  mov ax, di ; number of ticks is passed in rdi
  out 0x40, al ; write low byte of ticks value
  in al, 0x80 ; brief delay
  mov al, ah 
  out 0x40, al ; write high byte of ticks value
  ret
