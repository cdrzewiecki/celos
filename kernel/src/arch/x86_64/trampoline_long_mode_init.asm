%include "constants.asm"

PRESENT equ 1
WRITABLE equ 1 << 1
HUGE equ 1 << 7
NO_EXECUTE equ 1 << 63

global long_mode_start

section .text
bits 64
long_mode_start:
  ; load 0 into all segment registers
  mov ax, 0
  mov ss, ax
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax

  ; setup higher-half page table entries
  extern KERNEL_VMA
  extern p4_table
  extern p3_table_high
  extern p2_table_high
  extern p2_stack_table
  extern p1_stack_table
  extern high_stack_bottom
  
  ; map high P3 table to appropriate entry for higher half kernel
  mov rax, KERNEL_VMA
  ; mask off the bits so only bits 39-47 are left by using an AND operation
  mov rcx, 0xff8000000000 ; the AND instruction only lets you use a 64-bit value in the second operand if you use a register
  and rax, rcx ; strip off bits 48+ and bits 38-
  mov rcx, 0x8000000000 ; we're going to divide the higher-half address so that we have just the index into the P4 table
  div rcx ; result is in rax
  
  ; now that we've calculated the offset (it's in rax) for KERNEL_VMA, actually do the page mapping
  mov rcx, p3_table_high
  or rcx, PRESENT | WRITABLE
  mov [p4_table + rax * 8], rcx

  ; map high P2 table to appropriate entry for higher half kernel
  mov rax, KERNEL_VMA
  mov rcx, 0x7fc0000000 ; again we want to mask off the other bits, this masks off bits 30-38
  and rax, rcx
  mov rcx, 0x40000000 ; divide by this value to strip off the insignificant bits
  div rcx ; again result is in rax

  ; actually map P2 table
  mov rcx, p2_table_high
  or rcx, PRESENT | WRITABLE
  mov [p3_table_high + rax * 8], rcx

  ; Map entries for P3 and P2 tables to hold the stack
  mov rax, 0xffffffffffffffff ; tried to put this in a symbol and got AWFUL linker errors, so it's hard coded
  mov rcx, 0xff8000000000
  and rax, rcx
  mov rcx, 0x8000000000
  div rcx

  mov rcx, p3_table_high
  or rcx, PRESENT | WRITABLE
  mov [p4_table + rax * 8], rcx

  mov rax, 0xffffffffffffffff ; tried to put this in a symbol and got AWFUL linker errors, so it's hard coded
  mov rcx, 0x7fc0000000
  and rax, rcx
  mov rcx, 0x40000000
  div rcx
  mov rcx, p2_stack_table
  or rcx, PRESENT | WRITABLE
  mov [p3_table_high + rax * 8], rcx

  ; map the huge entries in the high P2 table
  mov rcx, 0
.map_p2_high:
  mov rax, 0x200000 ; 2 MB
  mul rcx
  or rax, PRESENT | WRITABLE | HUGE
  mov [p2_table_high + rcx * 8], rax

  inc rcx
  cmp rcx, 512
  jne .map_p2_high

  ; map the P2 entry for the stack's P1 table
  mov rax, 0xffffffffffffffff
  mov rcx, 0x1ff000 ; mask off all bits except the ones for the P2 index
  and rax, rcx
  mov rcx, 0x1000 ; strip off insignificant bits
  div rcx
  mov rcx, p1_stack_table
  or rcx, PRESENT | WRITABLE
  mov [p2_stack_table + rax * 8], rcx

  ; map the pages for the stack we want to use
  mov rcx, 0
.map_p1_stack:
  ; we're not identity mapping any more (unlike above). So the formula for the correct frame should be:
  ; counter - 496 * 4 KB + (address of bottom of stack reserved bytes). Thus, rcx * 0x1000 + high_stack_bottom
  mov rax, 0x1000
  mul rcx
  mov rbx, high_stack_bottom
  mov r8, KERNEL_VMA
  sub rbx, r8
  add rax, rbx
  mov r8, PRESENT | WRITABLE | NO_EXECUTE
  or rax, r8
  ; To calculate the index into the page table, we take the first index we should use (which is 512 - the number of stack pages,
  ; as the pages are all being mapped at the end of the table). Then we add the current loop counter to it. That is then the index
  ; into the P1 table.
  mov rbx, (512 - NUM_STACK_PAGES)
  add rbx, rcx
  mov [p1_stack_table + rbx * 8], rax

  inc rcx
  cmp rcx, NUM_STACK_PAGES
  jne .map_p1_stack

  ; set rsp to the top of memory so that the stack is in the higher half.
  ; Rust generated code assumes it gets an 8 byte aligned stack and then realigns the stack to 16 bytes (due to SysV ABI)
  mov rsp, 0xfffffffffffffff8

  ; enable SSE
  mov rax, cr0
  and ax, 0xfffb
  or ax, 0x2
  mov cr0, rax
  mov rax, cr4
  or ax, 3 << 9
  mov cr4, rax

  ; launch Rust kernel main function
  ; put kernel start and end addresses into registers so main function will pick it up
  extern _kernel_start
  extern _kernel_physical_start
  extern _kernel_end
  extern _kernel_physical_end
  mov rsi, _kernel_start
  mov rdx, _kernel_physical_start
  mov rcx, _kernel_end
  mov r8, _kernel_physical_end
  extern _start
  mov rax, _start
  jmp rax
