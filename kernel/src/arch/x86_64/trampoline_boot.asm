%include "constants.asm"

global start
global p4_table
global p3_table_high
global p2_table_high
global p2_stack_table
global p1_stack_table
global high_stack_bottom
global high_stack_top
extern long_mode_start

section .text
bits 32
start:
  mov esp, stack_top
  mov edi, ebx ; set multiboot info address in rdi so that we can access it from Rust

  push eax
  call clear_screen
  pop eax
  call check_multiboot
  call check_cpuid
  call check_long_mode

  call set_up_page_tables
  call enable_paging

  ; load 64-bit GDT
  lgdt [gdt64.pointer]

  jmp gdt64.code:long_mode_start

; Clear the VGA text mode buffer so that we can print text legibly
clear_screen:
  mov eax, 0x0 ; blank text
  mov ecx, 0 ; counter

.clear_next_char:
  mov dword [0xb8000 + ecx * 2], eax ; write blank char to ecx-th position in VGA text buffer
  inc ecx ; increment counter
  cmp ecx, 2000 ; 80x25 buffer means we have 2000 characters to clear
  jne .clear_next_char ; if we haven't reached 2000 loop

  ret

check_multiboot:
  cmp eax, 0x36d76289
  jne .no_multiboot
  ret

.no_multiboot:
  mov al, "0"
  jmp error

check_cpuid:
  ; Check if CPUID is supported by attempting to flip the ID bit (bit 21)
  ; in the FLAGS register. If we can flip it, CPUID is available.

  ; Copy FLAGS in to EAX via stack
  pushfd
  pop eax

  ; Copy to ECX as well for comparing later on
  mov ecx, eax

  ; Flip the ID bit
  xor eax, 1 << 21

  ; Copy EAX to FLAGS via the stack
  push eax
  popfd

  ; Copy FLAGS back to EAX (with the flipped bit if CPUID is supported)
  pushfd
  pop eax

  ; Restore FLAGS from the old version stored in ECX (i.e. flipping the
  ; ID bit back if it was ever flipped).
  push ecx
  popfd

  ; Compare EAX and ECX. If they are equal then that means the bit
  ; wasn't flipped, and CPUID isn't supported.
  cmp eax, ecx
  je .no_cpuid
  ret

.no_cpuid:
  mov al, "1"
  jmp error

check_long_mode:
  ; test if extended processor info in available
  mov eax, 0x80000000    ; implicit argument for cpuid
  cpuid                  ; get highest supported argument
  cmp eax, 0x80000001    ; it needs to be at least 0x80000001
  jb .no_long_mode       ; if it's less, the CPU is too old for long mode

  ; use extended info to test if long mode is available
  mov eax, 0x80000001    ; argument for extended processor info
  cpuid                  ; returns various feature bits in ecx and edx
  test edx, 1 << 29      ; test if the LM-bit is set in the D-register
  jz .no_long_mode       ; If it's not set, there is no long mode
  ret
.no_long_mode:
  mov al, "2"
  jmp error

; Prints 'ERR: ' and the given error code to the screen and hangs.
; Parameter: error code (in ascii) in al
error:
  mov dword [0xb8000], 0x4f524f45
  mov dword [0xb8004], 0x4f3a4f52
  mov dword [0xb8008], 0x4f204f20
  mov byte [0xb800a], al
  hlt

set_up_page_tables:
  ; map first P4 entry to P3 table
  mov eax, p3_table
  or eax, 0b11 ; present + writable
  mov [p4_table], eax

  ; map first P3 entry to P2 table
  mov eax, p2_table
  or eax, 0b11 ; present + writable
  mov [p3_table], eax

  ; map each P2 entry to a huge 2MB page
  mov ecx, 0 ; counter variable

.map_p2_table:
  ; map ecx-th P2 entry to a huge page that starts at address 2MB * ecx
  mov eax, 0x200000             ; 2 MB
  mul ecx                       ; start address of ecx-th page
  or eax, 0b10000011            ; present + writable + huge
  mov [p2_table + ecx * 8], eax ; map ecx-th entry

  inc ecx                       ; increment counter
  cmp ecx, 512                  ; if counter >= 512, then entire P2 table has been mapped
  jne .map_p2_table             ; otherwise map the next entry

  ret

enable_paging:
  ; load P4 to CR3 register (used to hold the page table)
  mov eax, p4_table
  mov cr3, eax

  ; enable PAE and PGE flags in CR4
  mov eax, cr4
  or eax, 1 << 5
  or eax, 1 << 7
  mov cr4, eax

  ; set long mode and NXE bits in EFER MSR (model-specific register)
  mov ecx, 0xC0000080
  rdmsr
  or eax, 1 << 8
  or eax, 1 << 11
  wrmsr

  ; enable paging and WP in CR0 register
  mov eax, cr0
  or eax, 1 << 31 ; paging enable bit
  or eax, 1 << 16 ; WP bit
  mov cr0, eax

  ret

section .bss
align 4096
p4_table:
  resb 4096
p3_table:
  resb 4096
p3_table_high:
  resb 4096
p2_table:
  resb 4096
p2_table_high:
  resb 4096
p2_stack_table:
  resb 4096
p1_stack_table:
  resb 4096
stack_bottom:
  resb 4096 * 4
stack_top:

section .bss.high_stack
align 4096
high_stack_bottom:
  resb 4096 * NUM_STACK_PAGES
high_stack_top:

section .rodata
gdt64:
  dq 0 ; zero entry
.code: equ $ - gdt64
  dq (1<<43) | (1 << 44) | (1 << 47) | (1 <<53) ; code segment
.pointer:
  dw $ - gdt64 -1
  dq gdt64
