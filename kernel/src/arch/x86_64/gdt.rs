use lazy_static::lazy_static;
use x86_64::{
  instructions::{
    segmentation::{Segment, CS},
    tables::load_tss,
  },
  structures::{
    gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
    tss::TaskStateSegment,
  },
  VirtAddr,
};

pub(in crate::arch::x86_64) const DOUBLE_FAULT_IST_INDEX: u16 = 0;
pub(in crate::arch::x86_64) const PAGE_FAULT_IST_INDEX: u16 = 1;
pub(in crate::arch::x86_64) const USED_IST_INDICES: [u16; 2] = [DOUBLE_FAULT_IST_INDEX, PAGE_FAULT_IST_INDEX];

// The IST stack size is 20 KB
pub(in crate::arch::x86_64) const IST_STACK_SIZE: usize = 4096 * 5;

lazy_static! {
  pub(in crate::arch::x86_64) static ref TSS: TaskStateSegment = {
    let mut tss = TaskStateSegment::new();
    tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
      static mut STACK: [u8; IST_STACK_SIZE] = [0; IST_STACK_SIZE];

      let stack_start = VirtAddr::from_ptr(unsafe { &STACK });
      #[allow(clippy::let_and_return)]
      let stack_end = stack_start + IST_STACK_SIZE;
      stack_end
    };
    tss.interrupt_stack_table[PAGE_FAULT_IST_INDEX as usize] = {
      static mut STACK: [u8; IST_STACK_SIZE] = [0; IST_STACK_SIZE];

      let stack_start = VirtAddr::from_ptr(unsafe { &STACK });
      #[allow(clippy::let_and_return)]
      let stack_end = stack_start + IST_STACK_SIZE;
      stack_end
    };
    tss
  };
}

lazy_static! {
  static ref GDT: (GlobalDescriptorTable, Selectors) = {
    let mut gdt = GlobalDescriptorTable::new();
    let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
    let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
    (gdt, Selectors { code_selector, tss_selector })
  };
}

struct Selectors {
  code_selector: SegmentSelector,
  tss_selector: SegmentSelector,
}

pub fn init() {
  GDT.0.load();
  unsafe {
    CS::set_reg(GDT.1.code_selector);
    load_tss(GDT.1.tss_selector);
  }
}
