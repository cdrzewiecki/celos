use alloc::{collections::VecDeque, vec::Vec};
use core::mem::size_of;

use bitflags::bitflags;
use contracts::*;
use x86_64::{
  align_up,
  registers::control::Cr3,
  structures::paging::{
    mapper::{MapToError, UnmapError},
    page::PageRange,
    FrameAllocator, FrameDeallocator, Mapper, OffsetPageTable, Page, PageSize, PageTable, PageTableFlags, PhysFrame,
  },
  PhysAddr, VirtAddr,
};

use super::{
  allocators::page::{PageAllocator, PageDeallocator},
  FRAME_ALLOCATOR, KERNEL_VMA, PHYSICAL_MEMORY_OFFSET,
};
use crate::multitasking::{Process, PROCESSES};

pub struct SharedMapping<S: PageSize> {
  frame: PhysFrame<S>,
  process_ids: Vec<u32>,
}

impl<S: PageSize> SharedMapping<S> {
  pub fn new(process: Process, frame: PhysFrame<S>) -> Self {
    let mut process_ids = Vec::<u32>::with_capacity(2);
    process_ids.push(process.process_id());

    Self { frame, process_ids }
  }

  pub fn add_process(&mut self, process: Process) {
    self.process_ids.push(process.process_id());
  }

  /// Removes `process` from the shared memory mapping.
  ///
  /// # Errors
  /// Returns `Err` if the process does not exist in the shared memory mapping.
  #[allow(clippy::result_unit_err)]
  pub fn remove_process(&mut self, process: Process) -> Result<(), ()> {
    match self.process_ids.binary_search(&process.process_id()) {
      Ok(index) => {
        self.process_ids.remove(index);
        Ok(())
      }
      Err(_) => Err(()),
    }
  }

  pub fn frame(&self) -> PhysFrame<S> {
    self.frame
  }

  pub fn contains_process(&self, process: Process) -> bool {
    self.process_ids.contains(&process.process_id())
  }

  pub fn in_use(&self) -> bool {
    !self.process_ids.is_empty()
  }
}

bitflags! {
  pub struct ProtectionFlags: u8 {
    const Present = 0b_0000_0001;
    const Writable = 0b_0000_0010;
    const Executable = 0b_0000_0100;
    const Global = 0b_0000_1000;
    const UserAccessible = 0b_0001_0000;
  }
}

bitflags! {
  pub struct MappingFlags: u16 {
    const GuardPage = 0b0000_0000_0000_0001;
    const PhysicallyContiguous = 0b0000_0000_0000_0010;
    const ExactMapping = 0b0000_0000_0000_0100;
    const GrowDown = 0b0000_0000_0000_1000;
    const HugePage2MB = 0b0000_0000_0001_0000;
    const HugePage1GB = 0b0000_0000_0010_0000;
    const OtherAddressSpace = 0b0000_0000_0100_0000;
    const SharedFrame = 0b0000_0000_1000_0000;
  }
}

bitflags! {
  pub struct CustomPageFlags: u16 {
    const GuardPage = 0b0000_0000_0000_0001;
    const SharedFrame = 0b0000_0000_0000_0010;
  }
}

impl From<CustomPageFlags> for PageTableFlags {
  fn from(value: CustomPageFlags) -> Self {
    let mut flags = PageTableFlags::empty();

    if value.contains(CustomPageFlags::GuardPage) {
      flags |= PageTableFlags::BIT_9;
    }

    if value.contains(CustomPageFlags::SharedFrame) {
      flags |= PageTableFlags::BIT_10;
    }

    flags
  }
}

impl From<PageTableFlags> for CustomPageFlags {
  fn from(value: PageTableFlags) -> Self {
    let mut flags = CustomPageFlags::empty();

    if value.contains(PageTableFlags::BIT_9) {
      flags |= CustomPageFlags::GuardPage;
    }

    if value.contains(PageTableFlags::BIT_10) {
      flags |= CustomPageFlags::SharedFrame;
    }

    flags
  }
}

impl From<ProtectionFlags> for PageTableFlags {
  fn from(protection: ProtectionFlags) -> Self {
    let mut flags = PageTableFlags::empty();

    if !protection.contains(ProtectionFlags::Executable) {
      flags |= PageTableFlags::NO_EXECUTE;
    }

    if protection.contains(ProtectionFlags::Global) {
      flags |= PageTableFlags::GLOBAL;
    }

    if protection.contains(ProtectionFlags::Present) {
      flags |= PageTableFlags::PRESENT;
    }

    if protection.contains(ProtectionFlags::UserAccessible) {
      flags |= PageTableFlags::USER_ACCESSIBLE;
    }

    if protection.contains(ProtectionFlags::Writable) {
      flags |= PageTableFlags::WRITABLE;
    }

    flags
  }
}

#[derive(Debug)]
#[allow(clippy::enum_variant_names)]
pub enum MappingError<S: PageSize> {
  MappingError(MapToError<S>),
  NoFreePages,
  NoFreeFrames,
  RequestedAddrUnavailable,
  StartAddrRequired,
  PageAllocatorNotFound,
  IncompatibleFlags(MappingFlags),
}

#[allow(clippy::too_many_lines)]
#[requires(u64::MAX - align_up(size as u64, S::SIZE) >= virt.unwrap_or_else(VirtAddr::zero).as_u64())]
#[requires(u64::MAX - align_up(size as u64, S::SIZE) >= phys.unwrap_or_else(|| PhysAddr::new(0xf_ffff_ffff_ffff)).as_u64())]
pub unsafe fn create_mapping<S: PageSize>(
  p4_address: PhysAddr,
  virt: Option<VirtAddr>,
  phys: Option<PhysAddr>,
  size: usize,
  mut flags: MappingFlags,
  protection: ProtectionFlags,
) -> Result<PageRange<S>, MappingError<S>>
where
  OffsetPageTable<'static>: Mapper<S>,
{
  if p4_address != Cr3::read().0.start_address() {
    flags |= MappingFlags::OtherAddressSpace;
  }

  let mut page_allocator_lock = super::PAGE_ALLOCATORS.lock();
  let page_allocator = match page_allocator_lock.get_mut(&p4_address) {
    Some(allocator) => allocator,
    None => return Err(MappingError::PageAllocatorNotFound),
  };
  let mut offset_table = p4_to_page_table(p4_address);
  let page_range = create_mapping_inner::<S>(page_allocator, &mut offset_table, virt, phys, size, flags, protection)?;

  for page in page_range {
    if u16::from(page.p4_index()) >= 256 {
      // This page is in the kernel's area, so we need to make sure that the P4 entry gets copied to every process if it isn't already there
      x86_64::instructions::interrupts::without_interrupts(|| {
        let processes = PROCESSES.lock();

        for (_, process) in processes.iter() {
          let process_p4_address = process.paging_base_addr();

          if process_p4_address == p4_address {
            continue;
          }

          let p4_virt = PHYSICAL_MEMORY_OFFSET + p4_address.as_u64();
          let process_p4_virt = PHYSICAL_MEMORY_OFFSET + process_p4_address.as_u64();

          let p4_table = &(*p4_virt.as_ptr::<PageTable>());
          let process_p4_table = &mut (*process_p4_virt.as_mut_ptr::<PageTable>());
          let index = page.p4_index();

          let p4_entry = &p4_table[index];
          let process_p4_entry = &process_p4_table[index];

          assert!(p4_entry.addr() == process_p4_entry.addr(), "Entry {index:?} exists in both {p4_address:?} and {process_p4_address:?} as a kernel entry, but with differing addresses");

          if !process_p4_entry.flags().contains(PageTableFlags::PRESENT) {
            process_p4_table[index] = p4_table[index].clone();
          }
        }
      });
    }
  }
  Ok(page_range)
}

#[allow(clippy::too_many_lines)]
unsafe fn create_mapping_inner<S: PageSize>(
  page_allocator: &mut (impl PageAllocator<S> + PageDeallocator<S>),
  page_table: &mut impl Mapper<S>,
  virt: Option<VirtAddr>,
  phys: Option<PhysAddr>,
  size: usize,
  flags: MappingFlags,
  protection: ProtectionFlags,
) -> Result<PageRange<S>, MappingError<S>> {
  if flags.contains(MappingFlags::ExactMapping) && virt.is_none() {
    return Err(MappingError::StartAddrRequired);
  }

  if flags.contains(MappingFlags::ExactMapping) && flags.contains(MappingFlags::GuardPage) {
    let incompatible_flags = MappingFlags::ExactMapping | MappingFlags::GuardPage;
    return Err(MappingError::IncompatibleFlags(incompatible_flags));
  }

  let mut length = if let Some(phys) = phys {
    let start_frame: PhysFrame<S> = PhysFrame::containing_address(phys);
    let end_frame: PhysFrame<S> = PhysFrame::containing_address(phys + size);
    let range = PhysFrame::range_inclusive(start_frame, end_frame);
    1 + (range.end - range.start)
  } else {
    let adjusted_size = align_up(size as u64, S::SIZE);
    adjusted_size / S::SIZE
  };

  if flags.contains(MappingFlags::GuardPage) {
    length += 1;
  }

  let page_range: Option<PageRange<S>> = page_allocator.allocate_pages(virt, None, Some(length));

  match page_range {
    Some(mut page_range) => {
      if flags.contains(MappingFlags::ExactMapping) {
        // Because we checked whether there was a value in the option at function start, we can safely unwrap this
        let start_addr = virt.unwrap();
        if page_range.start.start_address() != start_addr {
          for page in page_range {
            page_allocator.deallocate_page(page);
          }
          return Err(MappingError::RequestedAddrUnavailable);
        }
      }

      let mut allocated_frames = VecDeque::<PhysFrame<S>>::new();
      let mut mapped_pages = Vec::<Page<S>>::new();
      let mut teardown_allocations = |allocated_frames, page_range| {
        for frame in allocated_frames {
          FRAME_ALLOCATOR.lock().deallocate_frame(frame);
        }

        for page in page_range {
          page_allocator.deallocate_page(page);
        }
      };

      #[allow(clippy::collapsible_else_if)]
      if let Some(phys) = phys {
        let start_frame: PhysFrame<S> = PhysFrame::containing_address(phys);
        let end_frame: PhysFrame<S> = PhysFrame::containing_address(phys + size);

        for frame in PhysFrame::range_inclusive(start_frame, end_frame) {
          allocated_frames.push_back(frame);
        }

        // Right now the implementation of the page allocator can allocate *more* pages than required. If that happens
        // when a physical mapping is specified, we will have pages that have no frame to map them to. Currently not sure
        // what to do in this case - putting this panic here so that I can revisit the scenario when it comes up and I have
        // a better idea of the right solution.
        let mut num_pages = page_range.end - page_range.start;

        if flags.contains(MappingFlags::GuardPage) {
          num_pages -= 1;
        }

        assert!(
          num_pages == allocated_frames.len() as u64,
          "Page allocator returned {} pages and we have {} frames",
          num_pages,
          allocated_frames.len()
        );
      } else {
        if flags.contains(MappingFlags::PhysicallyContiguous) {
          todo!("Physically contiguous frame allocation not yet implemented")
        } else {
          let mut num_pages = page_range.end - page_range.start;
          if flags.contains(MappingFlags::GuardPage) {
            num_pages -= 1;
          }

          for _ in 0..num_pages {
            let frame: Option<PhysFrame<S>> = FRAME_ALLOCATOR.lock().allocate_frame();
            if let Some(frame) = frame {
              allocated_frames.push_back(frame);
            } else {
              teardown_allocations(allocated_frames, page_range);
              return Err(MappingError::NoFreeFrames);
            }
          }
        }
      }

      let mut page_flags = PageTableFlags::from(protection);
      if page_range.end.start_address() < KERNEL_VMA {
        page_flags |= PageTableFlags::USER_ACCESSIBLE;
      }
      if flags.contains(MappingFlags::SharedFrame) {
        page_flags |= PageTableFlags::from(CustomPageFlags::SharedFrame);
      }

      let kernel_start_page = Page::<S>::containing_address(super::KERNEL_MEMORY.lock().kernel_start);
      let kernel_end_page = Page::<S>::containing_address(super::KERNEL_MEMORY.lock().kernel_end);

      // Map the guard page
      if flags.contains(MappingFlags::GuardPage) {
        let guard_page_flags = PageTableFlags::from(CustomPageFlags::GuardPage);
        let guard_page = if flags.contains(MappingFlags::GrowDown) {
          page_range.next().expect("No pages in page range")
        } else {
          let page = page_range.end - 1;
          page_range.end -= 1;
          page
        };

        let frame = PhysFrame::<S>::from_start_address(PhysAddr::zero()).unwrap();
        let frame_allocator = &mut *FRAME_ALLOCATOR.lock();

        let mut parent_flags =
          page_flags & (PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::USER_ACCESSIBLE);
        if guard_page.p4_index() == kernel_start_page.p4_index() || guard_page.p4_index() == kernel_end_page.p4_index()
        {
          // Guard page is in the same place in the P4 page table, ensure that we don't screw up the kernel paging entries
          parent_flags.set(PageTableFlags::USER_ACCESSIBLE, false);
        }

        match page_table.map_to_with_table_flags(guard_page, frame, guard_page_flags, parent_flags, frame_allocator) {
          Ok(flusher) => {
            flusher.flush();
          }
          Err(e) => {
            teardown_allocations(allocated_frames, page_range);
            return Err(MappingError::MappingError(e));
          }
        }
      }

      for page in page_range {
        let frame = allocated_frames.pop_front().expect("Error getting allocated frame from vec");
        let frame_allocator = &mut *FRAME_ALLOCATOR.lock();

        let mut parent_flags =
          page_flags & (PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::USER_ACCESSIBLE);
        if page.p4_index() == kernel_start_page.p4_index() || page.p4_index() == kernel_end_page.p4_index() {
          // Page is in the same place in the P4 page table, ensure that we don't screw up the kernel paging entries
          parent_flags.set(PageTableFlags::USER_ACCESSIBLE, false);
        }

        match page_table.map_to(page, frame, page_flags, frame_allocator) {
          Ok(flusher) => {
            mapped_pages.push(page);
            flusher.flush();
          }
          Err(e) => {
            let frame_zero = PhysFrame::<S>::from_start_address(PhysAddr::zero()).unwrap();

            for page in mapped_pages {
              match page_table.unmap(page) {
                Ok((frame, flusher)) => {
                  flusher.flush();

                  if frame != frame_zero {
                    allocated_frames.push_back(frame); // Put frame back in the Vec to be torn down
                  }
                }

                Err(e) => {
                  panic!("Got error while unmapping page {page:?} during teardown of create_mapping!\nError: {e:?}");
                }
              }
            }

            teardown_allocations(allocated_frames, page_range);
            return Err(MappingError::MappingError(e));
          }
        }
      }

      if phys.is_none() {
        // Zero the pages before handing them out
        #[allow(clippy::cast_possible_truncation)]
        let size = S::SIZE as usize / size_of::<u8>();

        for page in page_range {
          let dest = if flags.contains(MappingFlags::OtherAddressSpace) {
            let frame = page_table.translate_page(page).unwrap();
            VirtAddr::new(frame.start_address().as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64()).as_mut_ptr::<u8>()
          } else {
            page.start_address().as_mut_ptr::<u8>()
          };

          core::ptr::write_bytes(dest, 0, size);
        }
      }

      Ok(Page::<S>::range(page_range.start, page_range.end))
    }
    None => Err(MappingError::NoFreePages),
  }
}

unsafe fn p4_to_page_table<'a>(p4_address: PhysAddr) -> OffsetPageTable<'a> {
  let p4 = &mut *((p4_address.as_u64() + super::PHYSICAL_MEMORY_OFFSET.as_u64()) as *mut PageTable);
  OffsetPageTable::new(p4, super::PHYSICAL_MEMORY_OFFSET)
}

/// Removes a page from the page table at `p4_address`.
///
/// # Safety
/// The caller must guarantee that the page is no longer in use
///
/// # Errors
/// Returns `UnmapError` if there was an issue removing the page mapping
pub unsafe fn remove_mapping<S: 'static + PageSize>(
  p4_address: PhysAddr,
  page: Page<S>,
) -> Result<PhysFrame<S>, UnmapError>
where
  OffsetPageTable<'static>: Mapper<S>,
{
  let mut page_table = p4_to_page_table(p4_address);
  let mut page_allocator_lock = super::PAGE_ALLOCATORS.lock();
  let page_allocator = page_allocator_lock.get_mut(&p4_address).expect("No page allocator found for process ID");

  match page_table.unmap(page) {
    Ok((frame, flush)) => {
      flush.flush();
      page_allocator.deallocate_page(page);
      Ok(frame)
    }
    Err(e) => Err(e),
  }
}
