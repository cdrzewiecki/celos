use core::convert::TryFrom;

use x86_64::{
  structures::paging::{Page, PageTableIndex},
  VirtAddr,
};

pub struct UsedLevel4Entries {
  entry_state: [bool; 512], // whether an entry is in use by the kernel
}

impl UsedLevel4Entries {
  pub fn new(kernel_start: VirtAddr, kernel_end: VirtAddr) -> Self {
    let mut used = UsedLevel4Entries { entry_state: [false; 512] };

    used.entry_state[0] = true;
    let start_page: Page = Page::containing_address(kernel_start);
    let end_page: Page = Page::containing_address(kernel_end);

    for p4_index in u64::from(start_page.p4_index())..u64::from(end_page.p4_index()) {
      used.entry_state[usize::try_from(p4_index).unwrap()] = true;
    }

    let start_page: Page = Page::containing_address(VirtAddr::new(0xffff_ffff_8000_0000));
    let end_page: Page = Page::containing_address(VirtAddr::new(0xffff_ffff_ffff_ffff));

    for p4_index in u64::from(start_page.p4_index())..u64::from(end_page.p4_index()) {
      used.entry_state[usize::try_from(p4_index).unwrap()] = true;
    }

    used
  }

  pub fn get_free_entry(&mut self) -> PageTableIndex {
    let (idx, entry) = self
      .entry_state
      .iter_mut()
      .enumerate()
      .find(|(_, &mut entry)| !entry)
      .expect("No usable level 4 entries found");

    *entry = true;
    PageTableIndex::new(u16::try_from(idx).unwrap())
  }
}
