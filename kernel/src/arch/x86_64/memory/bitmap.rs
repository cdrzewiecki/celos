use core::fmt::Debug;

use log::{debug, error, trace};
use x86_64::{
  structures::paging::{
    page::AddressNotAligned, PageSize, PhysFrame, Size1GiB as Size1GB, Size2MiB as Size2MB, Size4KiB as Size4KB,
  },
  PhysAddr,
};

use super::allocators::frame::FrameMarker;
use crate::memory::{region::RegionSize, BitmapError};

#[derive(Clone, Copy)]
pub struct FrameBitmap {
  initialized: bool,
  base_addr: PhysAddr,
  /// NOTE: max_addr is exclusive, not inclusive.
  max_addr: PhysAddr,
  bitmap: u64,
  region_size: RegionSize,
}

impl FrameBitmap {
  pub const fn new() -> Self {
    Self {
      initialized: false,
      base_addr: PhysAddr::zero(),
      max_addr: PhysAddr::zero(),
      bitmap: 0,
      region_size: RegionSize::Size4KB,
    }
  }

  pub fn init(&mut self, base_addr: PhysAddr, max_addr: PhysAddr, region_size: RegionSize) -> Result<(), BitmapError> {
    if self.initialized {
      return Err(BitmapError::AlreadyInitialized);
    }
    if !base_addr.is_aligned(region_size) {
      return Err(BitmapError::BaseAddressNotAligned);
    }
    if max_addr > base_addr + (region_size * 64) {
      return Err(BitmapError::MaxAddressTooLarge);
    }

    self.base_addr = base_addr;
    self.max_addr = max_addr;
    self.region_size = region_size;
    self.initialized = true;
    Ok(())
  }
}

unsafe impl<S: PageSize> FrameMarker<S> for FrameBitmap {
  fn mark_frame_used(&mut self, frame: PhysFrame<S>, _region_size: Option<RegionSize>) -> Result<(), BitmapError> {
    // First do sanity checks
    if !self.initialized {
      return Err(BitmapError::NotInitialized);
    }

    if frame.start_address() < self.base_addr || frame.start_address() >= self.max_addr {
      return Err(BitmapError::FrameOutsideBitmap);
    }

    match S::SIZE {
      Size1GB::SIZE => {
        if self.region_size != RegionSize::Size1GB {
          error!("Attempted to use a 1 GB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size2MB::SIZE => {
        if self.region_size != RegionSize::Size2MB {
          error!("Attempted to use a 2 MB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size4KB::SIZE => {
        if self.region_size == RegionSize::Size1GB || self.region_size == RegionSize::Size2MB {
          error!("Attempted to use a 4 KB frame in a {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      _ => return Err(BitmapError::BadFrameSize),
    }

    // To generate the bitmask that will be OR-ed with the existing bitmap, we need a bit of 1 shifted by
    // some number of places. Dividing address by the region size gives us something that is a natural
    // multiple of the region size (since we are dividing u64 by u64, there will be no fractional part).
    // Then to handle the case where the quotient is higher than 63, we modulo that by 64 to get a value in
    // the range 0-63. Finally, we subtract the modulo result from 63 to get the number of places to shift over.
    // Remember that the number of bits to shift is 63 when we want to set the first bit, 62 when we want to set
    // the second bit, and so on. So for example: if we have 0x0 as the address, then the modulo will work out
    // to 0, and the shift value will then be 63. If we have 0x1000 as the address (with a 4 KB region size), then
    // the modulo works out to 1, and the shift value will be 62. And so on.
    trace!("Bitmap is {:064b}", self.bitmap);
    let shift_value = 63 - (frame.start_address().as_u64() / self.region_size) % 64;
    let mask = 1 << shift_value;

    // Finally, actually OR the current bitmap with the mask we calculated
    self.bitmap |= mask;
    trace!("Bitmap is {:064b}", self.bitmap);
    trace!("{frame:?}");
    trace!("{:?}", *self);
    Ok(())
  }

  unsafe fn mark_frame_free(
    &mut self,
    frame: PhysFrame<S>,
    _region_size: Option<RegionSize>,
  ) -> Result<(), BitmapError> {
    // First do sanity checks
    if !self.initialized {
      return Err(BitmapError::NotInitialized);
    }

    if frame.start_address() < self.base_addr || frame.start_address() >= self.max_addr {
      return Err(BitmapError::FrameOutsideBitmap);
    }

    match S::SIZE {
      Size1GB::SIZE => {
        if self.region_size != RegionSize::Size1GB {
          error!("Attempted to free a 1 GB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size2MB::SIZE => {
        if self.region_size != RegionSize::Size2MB {
          error!("Attempted to free a 2 MB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size4KB::SIZE => {
        if self.region_size == RegionSize::Size1GB || self.region_size == RegionSize::Size2MB {
          error!("Attempted to free a 4 KB frame in a {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      _ => return Err(BitmapError::BadFrameSize),
    }

    // See mark_frame_used for an explanation of this logic
    let shift_value = 63 - (frame.start_address().as_u64() / self.region_size) % 64;

    // This part is a bit different. Because we need to set a bit to 0, we need to AND the mask with the bitmap.
    // That means that our mask must be all 1s, except for the bit we want to flip. To do this, we NOT
    // what the mask would be if it were going to set the bit to 1. That gives us a 64-bit mask where everything is set to 1
    // except what we want.
    let mask = !(1 << shift_value);

    // Also, since we are setting a bit to 0, this time the operation is AND rather than OR
    self.bitmap &= mask;
    Ok(())
  }

  fn next_free_frame(&mut self) -> Option<PhysFrame<S>> {
    if !self.initialized {
      return None;
    }

    for i in (0_u64..64_u64).rev() {
      let bit = (self.bitmap >> i) & 1;
      if bit == 0 {
        let position = 63 - i;
        let start_address = self.base_addr + (position * self.region_size);
        let frame = PhysFrame::<S>::from_start_address(start_address);
        match frame {
          Err(AddressNotAligned) => panic!("Tried to construct a frame but address was not aligned.\nBitmap range: {:?} to {:?}\nPosition in bitmap: {}\nAddress calculated for frame: {:?}", self.base_addr, self.max_addr, position, start_address),
          Ok(frame) => {
            if frame.start_address() < self.max_addr {
              match self.mark_frame_used(frame, None) {
                Ok(_) => return Some(frame),
                Err(e) => {
                  debug!("Error while marking frame used. Frame is {frame:?}, position is {position}, start_address is {start_address:?}");
                  panic!("Error while marking frame used: {e:?}");
                }
              }
            }
          },
        }
      }
    }
    None
  }

  fn is_frame_free(&self, frame: PhysFrame<S>) -> Result<bool, BitmapError> {
    if !self.initialized {
      return Err(BitmapError::NotInitialized);
    }
    if frame.start_address() < self.base_addr || frame.start_address() >= self.max_addr {
      return Err(BitmapError::FrameOutsideBitmap);
    }

    match S::SIZE {
      Size1GB::SIZE => {
        if self.region_size != RegionSize::Size1GB {
          error!("Attempted to lookup a 1 GB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size2MB::SIZE => {
        if self.region_size != RegionSize::Size2MB {
          error!("Attempted to lookup a 2 MB frame in a(n) {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      Size4KB::SIZE => {
        if self.region_size == RegionSize::Size1GB || self.region_size == RegionSize::Size2MB {
          error!("Attempted to lookup a 4 KB frame in a {} bitmap", self.region_size);
          return Err(BitmapError::BadRegionSize);
        }
      }
      _ => return Err(BitmapError::BadFrameSize),
    }

    let shift_value = 63 - (frame.start_address().as_u64() / self.region_size) % 64;
    let bit = (self.bitmap >> shift_value) & 1;
    if bit == 0 {
      Ok(true)
    } else {
      Ok(false)
    }
  }
}

impl Debug for FrameBitmap {
  fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
    f.write_fmt(format_args!(
      "FrameBitmap {:?} {:#x}-{:#x}",
      self.region_size,
      self.base_addr.as_u64(),
      self.max_addr.as_u64()
    ))
  }
}
