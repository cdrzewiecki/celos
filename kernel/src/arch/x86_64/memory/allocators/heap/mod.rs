use crate::concurrency::Locked;

mod fixed_size_block;

use self::fixed_size_block::FixedSizeBlockAllocator;

#[global_allocator]
pub static ALLOCATOR: Locked<FixedSizeBlockAllocator> = Locked::new(FixedSizeBlockAllocator::new());
