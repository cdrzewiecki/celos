use multiboot2::{MemoryArea, MemoryMapTag};
use x86_64::{
  structures::paging::{
    frame::PhysFrameRangeInclusive, FrameAllocator, PhysFrame, Size1GiB as Size1GB, Size2MiB as Size2MB,
    Size4KiB as Size4KB,
  },
  PhysAddr, VirtAddr,
};

use super::super::super::super::gdt;
use crate::{
  arch::{memory, memory::allocators::frame::FrameRegion},
  memory::region::RegionSize,
};

pub struct AreaFrameAllocator<'a> {
  next_free_frame: PhysFrame<Size4KB>,
  current_area: Option<&'a MemoryArea>,
  memory_map: &'a MemoryMapTag,
  pub kernel_start: PhysFrame<Size4KB>,
  pub kernel_end: PhysFrame<Size4KB>,
  pub multiboot_start: PhysFrame<Size4KB>,
  pub multiboot_end: PhysFrame<Size4KB>,
  allocated_frames: [Option<PhysFrame>; 1024],
  allocated_frames_index: usize,
  test_allocated_frames: [PhysFrame; 1024],
}

impl<'a> AreaFrameAllocator<'a> {
  pub fn new(
    kernel_start: PhysAddr,
    kernel_end: PhysAddr,
    multiboot_start: VirtAddr,
    multiboot_end: VirtAddr,
    memory_map: &'a MemoryMapTag,
  ) -> AreaFrameAllocator {
    let mut allocator = AreaFrameAllocator {
      next_free_frame: PhysFrame::containing_address(PhysAddr::new(0)),
      current_area: None,
      memory_map,
      kernel_start: PhysFrame::containing_address(kernel_start),
      kernel_end: PhysFrame::containing_address(kernel_end),
      multiboot_start: PhysFrame::containing_address(PhysAddr::new(multiboot_start.as_u64())),
      multiboot_end: PhysFrame::containing_address(PhysAddr::new(multiboot_end.as_u64())),
      allocated_frames: [None; 1024],
      allocated_frames_index: 0,
      test_allocated_frames: [PhysFrame::from_start_address(PhysAddr::new(0x0)).unwrap(); 1024],
    };
    allocator.choose_next_area();
    allocator
  }

  fn choose_next_area(&mut self) {
    self.current_area = self
      .memory_map
      .memory_areas()
      .filter(|&area| PhysFrame::containing_address(PhysAddr::new(area.end_address() - 1)) >= self.next_free_frame)
      .min_by_key(|&area| area.start_address());

    if let Some(area) = self.current_area {
      let start_frame = PhysFrame::containing_address(PhysAddr::new(area.start_address()));
      if self.next_free_frame < start_frame {
        self.next_free_frame = start_frame;
      }
    }
  }

  pub fn is_frame_allocated(&self, frame: PhysFrame) -> bool {
    for other_frame in self.allocated_frames.iter().filter(|x| x.is_some()) {
      if other_frame.unwrap() == frame {
        return true;
      }
    }
    false
  }

  pub fn is_2mb_frame_allocated(&self, frame: PhysFrame<Size2MB>) -> bool {
    let smaller_frame = PhysFrame::<Size4KB>::containing_address(frame.start_address());
    self.is_frame_allocated(smaller_frame)
  }

  pub fn is_1gb_frame_allocated(&self, frame: PhysFrame<Size1GB>) -> bool {
    let smaller_frame = PhysFrame::<Size4KB>::containing_address(frame.start_address());
    self.is_frame_allocated(smaller_frame)
  }
}

unsafe impl FrameAllocator<Size4KB> for AreaFrameAllocator<'_> {
  fn allocate_frame(&mut self) -> Option<PhysFrame> {
    if let Some(area) = self.current_area {
      let frame = self.next_free_frame;

      let current_area_last_frame = {
        let address = area.end_address() - 1;
        PhysFrame::containing_address(PhysAddr::new(address))
      };

      let mut tss_stack_frames: [Option<(PhysFrame, PhysFrame)>; 6] = [None; 6];
      for i in &gdt::USED_IST_INDICES {
        let ist_entry_end = gdt::TSS.interrupt_stack_table[*i as usize];
        let ist_entry_start = ist_entry_end - gdt::IST_STACK_SIZE;

        let phys_ist_end = PhysAddr::new(ist_entry_end.as_u64() - memory::KERNEL_VMA.as_u64());
        let phys_ist_start = PhysAddr::new(ist_entry_start.as_u64() - memory::KERNEL_VMA.as_u64());

        let ist_start_frame = PhysFrame::<Size4KB>::containing_address(phys_ist_start);
        let ist_end_frame = PhysFrame::<Size4KB>::containing_address(phys_ist_end);

        tss_stack_frames[*i as usize] = Some((ist_start_frame, ist_end_frame));
      }

      let min_stack_frame = tss_stack_frames
        .iter()
        .filter(|x| x.is_some())
        .min_by_key(|x| x.unwrap().0.start_address().as_u64())
        .unwrap()
        .unwrap()
        .0;
      let max_stack_frame = tss_stack_frames
        .iter()
        .filter(|x| x.is_some())
        .max_by_key(|x| x.unwrap().1.start_address().as_u64())
        .unwrap()
        .unwrap()
        .1;

      if frame > current_area_last_frame {
        // all free frames in this area are used, switch to next area
        self.choose_next_area();
      } else if frame >= self.kernel_start && frame <= self.kernel_end {
        // frame is in use by the kernel
        self.next_free_frame = self.kernel_end + 1;
      } else if frame >= self.multiboot_start && frame <= self.multiboot_end {
        // frame is in use by the multiboot info structure
        self.next_free_frame = self.multiboot_end + 1;
      } else if frame.start_address().as_u64() < 0x10_0000 {
        // frame is in the region < 1 MB, going to leave this area be as there's a lot in use in that area
        self.next_free_frame = PhysFrame::from_start_address(PhysAddr::new(0x10_0000)).unwrap();
      } else if frame >= min_stack_frame && frame < max_stack_frame {
        // frame is in the IST stack frames, which aren't part of the kernel range for some reason - move past them
        self.next_free_frame = max_stack_frame + 1;
      } else {
        // frame is unused, increment next_free_frame and return it
        self.next_free_frame =
          PhysFrame::from_start_address(PhysAddr::new(frame.start_address().as_u64() + 4096)).unwrap();
        self.allocated_frames[self.allocated_frames_index] = Some(frame);
        self.test_allocated_frames[self.allocated_frames_index] = frame;
        self.allocated_frames_index += 1;
        return Some(frame);
      }

      // frame was not valid, try again now that we've updated next_free_frame
      self.allocate_frame()
    } else {
      None // no free frames left
    }
  }
}

unsafe impl super::FrameAllocator for AreaFrameAllocator<'_> {
  #[allow(clippy::type_complexity)]
  fn get_used_frames(
    &self,
  ) -> (Option<&[PhysFrame<Size4KB>]>, Option<&[PhysFrame<Size2MB>]>, Option<&[PhysFrame<Size1GB>]>) {
    (Some(&self.test_allocated_frames[0..self.allocated_frames_index]), None, None)
  }

  fn get_kernel_bounds(&self) -> PhysFrameRangeInclusive<Size4KB> {
    PhysFrame::range_inclusive(self.kernel_start, self.kernel_end)
  }

  fn get_multiboot_bounds(&self) -> PhysFrameRangeInclusive<Size4KB> {
    PhysFrame::range_inclusive(self.multiboot_start, self.multiboot_end)
  }

  fn allocate_region(&mut self, _region_size: RegionSize) -> Option<FrameRegion> {
    unimplemented!()
  }
}
