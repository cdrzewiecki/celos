use core::fmt::Debug;

use x86_64::structures::paging::{
  frame::PhysFrameRangeInclusive, PageSize, PhysFrame, Size1GiB as Size1GB, Size2MiB as Size2MB, Size4KiB as Size4KB,
};

use crate::memory::{region::RegionSize, BitmapError};

pub mod area;
pub mod buddy;

/// Trait for frame allocators used by the OS
///
/// # Safety
/// `allocate_region` must allocate only unique, free regions of memory
pub unsafe trait FrameAllocator {
  #[allow(clippy::type_complexity)]
  fn get_used_frames(
    &self,
  ) -> (Option<&[PhysFrame<Size4KB>]>, Option<&[PhysFrame<Size2MB>]>, Option<&[PhysFrame<Size1GB>]>);
  fn get_kernel_bounds(&self) -> PhysFrameRangeInclusive<Size4KB>;
  fn get_multiboot_bounds(&self) -> PhysFrameRangeInclusive<Size4KB>;
  fn allocate_region(&mut self, region_size: RegionSize) -> Option<FrameRegion>;
}

pub trait FrameDeallocator {
  unsafe fn deallocate_region(&mut self, region: FrameRegion);
}

/// Trait for marking frames within a bitmap either free or used
///
/// # Safety
/// Because this trait is for a physical memory allocator, great care needs to be taken to ensure that frames are
/// correctly marked used or free
pub unsafe trait FrameMarker<S: PageSize> {
  fn mark_frame_used(&mut self, frame: PhysFrame<S>, region_size: Option<RegionSize>) -> Result<(), BitmapError>;
  unsafe fn mark_frame_free(&mut self, frame: PhysFrame<S>, region_size: Option<RegionSize>)
    -> Result<(), BitmapError>;
  fn next_free_frame(&mut self) -> Option<PhysFrame<S>>;
  fn is_frame_free(&self, frame: PhysFrame<S>) -> Result<bool, BitmapError>;
}

#[derive(Clone, Copy, Debug)]
pub enum RegionError {
  AddressNotAligned,
}

pub struct FrameRegion {
  start_frame: PhysFrame<Size4KB>,
  size: RegionSize,
}

impl FrameRegion {
  pub fn from_start_frame(frame: PhysFrame, size: RegionSize) -> Result<Self, RegionError> {
    if frame.start_address().is_aligned(size) {
      Ok(Self { start_frame: frame, size })
    } else {
      Err(RegionError::AddressNotAligned)
    }
  }
}

impl Debug for FrameRegion {
  fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
    f.write_fmt(format_args!("{} FrameRegion at {:?}", self.size, self.start_frame))
  }
}
