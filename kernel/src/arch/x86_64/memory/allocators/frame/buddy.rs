use alloc::vec::Vec;
use core::{convert::TryInto, time::Duration};

use log::{debug, trace, warn};
use x86_64::{
  structures::paging::{
    frame::PhysFrameRangeInclusive, FrameAllocator, FrameDeallocator, PageSize, PhysFrame, Size1GiB as Size1GB,
    Size2MiB as Size2MB, Size4KiB as Size4KB,
  },
  PhysAddr, VirtAddr,
};

use super::{FrameMarker, FrameRegion};
use crate::{
  arch::memory::{bitmap::FrameBitmap, FRAME_ALLOCATOR},
  concurrency::Locked,
  memory::{region::RegionSize, BitmapError},
  multitasking::sleep,
};

#[derive(Debug)]
pub struct BuddyFrameAllocator {
  kernel_start: PhysFrame<Size4KB>,
  kernel_end: PhysFrame<Size4KB>,
  multiboot_start: PhysFrame<Size4KB>,
  multiboot_end: PhysFrame<Size4KB>,
  frames_4kb: Vec<FrameBitmap>,
  frames_8kb: Vec<FrameBitmap>,
  frames_16kb: Vec<FrameBitmap>,
  frames_32kb: Vec<FrameBitmap>,
  frames_2mb: Vec<FrameBitmap>,
  frames_1gb: Vec<FrameBitmap>,
  next_4kb: Option<PhysFrame<Size4KB>>,
  next_8kb: Option<PhysFrame<Size4KB>>,
  next_16kb: Option<PhysFrame<Size4KB>>,
  next_32kb: Option<PhysFrame<Size4KB>>,
  next_2mb: Option<PhysFrame<Size2MB>>,
  next_1gb: Option<PhysFrame<Size1GB>>,
}

impl BuddyFrameAllocator {
  pub const fn new() -> Self {
    Self {
      kernel_start: unsafe { PhysFrame::from_start_address_unchecked(PhysAddr::zero()) },
      kernel_end: unsafe { PhysFrame::from_start_address_unchecked(PhysAddr::zero()) },
      multiboot_start: unsafe { PhysFrame::from_start_address_unchecked(PhysAddr::zero()) },
      multiboot_end: unsafe { PhysFrame::from_start_address_unchecked(PhysAddr::zero()) },
      frames_4kb: Vec::new(),
      frames_8kb: Vec::new(),
      frames_16kb: Vec::new(),
      frames_32kb: Vec::new(),
      frames_2mb: Vec::new(),
      frames_1gb: Vec::new(),
      next_4kb: None,
      next_8kb: None,
      next_16kb: None,
      next_32kb: None,
      next_2mb: None,
      next_1gb: None,
    }
  }

  pub unsafe fn init(
    &mut self,
    kernel_phys_start: PhysAddr,
    multiboot_addr: VirtAddr,
    old_alloc: &impl super::FrameAllocator,
  ) {
    self.kernel_start = PhysFrame::<Size4KB>::containing_address(kernel_phys_start);
    let bootinfo = multiboot2::load(
      multiboot_addr.as_u64().try_into().expect("Running on a platform where usize is less than 64 bits"),
    )
    .expect("Error loading multiboot info struct");
    let memory_map = bootinfo.memory_map_tag().expect("Failed to load memory map tag");
    let (used_4kb, used_2mb, used_1gb) = old_alloc.get_used_frames();

    // Init frame structures
    let max_address = memory_map.memory_areas().max_by_key(|x| x.end_address()).unwrap().end_address();
    let num_1gb_areas = divide_and_round_up(max_address, RegionSize::Size1GB as u64);
    debug!("There are {num_1gb_areas} 1 GB areas of memory");
    let num_2mb_areas = divide_and_round_up(max_address, RegionSize::Size2MB as u64);
    debug!("There are {num_2mb_areas} 2 MB areas of memory");
    let num_32kb_areas = divide_and_round_up(max_address, RegionSize::Size32KB as u64);
    debug!("There are {num_32kb_areas} 32 KB areas of memory");
    let num_16kb_areas = divide_and_round_up(max_address, RegionSize::Size16KB as u64);
    debug!("There are {num_16kb_areas} 16 KB areas of memory");
    let num_8kb_areas = divide_and_round_up(max_address, RegionSize::Size8KB as u64);
    debug!("There are {num_8kb_areas} 8 KB areas of memory");
    let num_4kb_areas = divide_and_round_up(max_address, RegionSize::Size4KB as u64);
    debug!("There are {num_4kb_areas} 4 KB areas of memory");

    populate_bitmaps(&mut self.frames_1gb, RegionSize::Size1GB, num_1gb_areas);
    populate_bitmaps(&mut self.frames_2mb, RegionSize::Size2MB, num_2mb_areas);
    populate_bitmaps(&mut self.frames_32kb, RegionSize::Size32KB, num_32kb_areas);
    populate_bitmaps(&mut self.frames_16kb, RegionSize::Size16KB, num_16kb_areas);
    populate_bitmaps(&mut self.frames_8kb, RegionSize::Size8KB, num_8kb_areas);
    populate_bitmaps(&mut self.frames_4kb, RegionSize::Size4KB, num_4kb_areas);

    // For any areas which are not available, mark off the frames as unused
    let first_frame = PhysFrame::<Size4KB>::from_start_address(PhysAddr::zero()).unwrap();
    let last_frame = PhysFrame::<Size4KB>::containing_address(PhysAddr::new(max_address));

    for frame in PhysFrame::range(first_frame, last_frame) {
      let start_address = frame.start_address().as_u64();
      let end_address = start_address + frame.size();
      if !memory_map
        .memory_areas()
        .any(|area| start_address >= area.start_address() && end_address < area.end_address())
      {
        match self.mark_frame_used(frame, None) {
          Err(e) => match e {
            BitmapError::FrameOutsideBitmap => {
              warn!("Frame {frame:?} was indicated as unusable but is outside any frame bitmap");
            }
            _ => panic!("Error while marking unusable frame {frame:?} as used: {e:?}"),
          },
          Ok(_) => trace!("Frame {frame:?} appears to be out of the usable range, marked as already used"),
        }
      }
    }

    // Mark areas off based on kernel bounds
    self.kernel_end = old_alloc.get_kernel_bounds().end;
    for frame in old_alloc.get_kernel_bounds() {
      match self.mark_frame_used(frame, None) {
        Err(e) => panic!("Got error while trying to mark frame {frame:?} used: {e:?}"),
        Ok(_) => trace!("Frame {frame:?} marked as used"),
      }
    }

    // Mark areas off based on multiboot bounds
    self.multiboot_start = old_alloc.get_multiboot_bounds().start;
    self.multiboot_end = old_alloc.get_multiboot_bounds().end;
    for frame in old_alloc.get_multiboot_bounds() {
      match self.mark_frame_used(frame, None) {
        Err(e) => panic!("Got error while trying to mark frame {frame:?} used: {e:?}"),
        Ok(_) => trace!("Frame {frame:?} marked as used"),
      }
    }

    // Mark areas off based on 1GB frames
    if let Some(frames) = used_1gb {
      for frame in frames {
        match self.mark_frame_used(*frame, None) {
          Err(e) => panic!("Got error while trying to mark frame {frame:?} used: {e:?}"),
          Ok(_) => trace!("Frame {frame:?} marked as used"),
        }
      }
    }

    // Mark areas off based on 2MB frames
    if let Some(frames) = used_2mb {
      for frame in frames {
        match self.mark_frame_used(*frame, None) {
          Err(e) => panic!("Got error while trying to mark frame {frame:?} used: {e:?}"),
          Ok(_) => trace!("Frame {frame:?} marked as used"),
        }
      }
    }

    // Mark areas off based on 4KB frames
    if let Some(frames) = used_4kb {
      for frame in frames {
        match self.mark_frame_used(*frame, None) {
          Err(e) => panic!("Got error while trying to mark frame {frame:?} used: {e:?}"),
          Ok(_) => trace!("Frame {frame:?} marked as used"),
        }
      }
    }
  }

  fn get_bitmap(&mut self, address: PhysAddr, region_size: RegionSize) -> &mut FrameBitmap {
    let index: usize = (address.as_u64() / (64_u64 * region_size))
      .try_into()
      .expect("Running on a platform with pointer sizes less than 64 bits");
    match region_size {
      RegionSize::Size1GB => &mut self.frames_1gb[index],
      RegionSize::Size2MB => &mut self.frames_2mb[index],
      RegionSize::Size32KB => &mut self.frames_32kb[index],
      RegionSize::Size16KB => &mut self.frames_16kb[index],
      RegionSize::Size8KB => &mut self.frames_8kb[index],
      RegionSize::Size4KB => &mut self.frames_4kb[index],
    }
  }

  fn free_4kb_frame(&mut self, frame: PhysFrame<Size4KB>, region_size: RegionSize) -> Result<bool, BitmapError> {
    // If the region size is greater than 32 KB, we work with 2 MB or 1 GB PhysFrame objects. That isn't something we
    // can recurse on as it's a different type, so panic if that happens.
    assert!(region_size <= RegionSize::Size32KB);

    // First, recursively check if all the frames in the next smallest region size are free
    let smaller_region = region_size.smaller();
    let smaller_frames_free = if smaller_region == region_size {
      // This is the base case - if we are at the smallest region size, don't recurse
      true
    } else {
      self.free_4kb_frame(frame, smaller_region)?
    };

    if smaller_frames_free {
      // If all the frames in the next smallest region size are free (or if we're at the base case), then free the frame in
      // the current region size (if we aren't in the base case, the smaller frames will have been freed by recursion)
      unsafe {
        self.get_bitmap(frame.start_address(), region_size).mark_frame_free(frame, None)?;
      }

      let larger_region = region_size.larger();

      assert!(larger_region != RegionSize::Size1GB, "Somehow got a 1 GB region as the next larger region size, but this function should only ever see at most a 2 MB region");

      // Now we need to check all the frames of the CURRENT region size that are a child of the PARENT region size to see if they are free
      let larger_region_start = frame.start_address().align_down(larger_region);
      let larger_region_end = larger_region_start + larger_region as u64;
      let start_frame = PhysFrame::<Size4KB>::from_start_address(larger_region_start).unwrap();
      let end_frame = PhysFrame::<Size4KB>::from_start_address(larger_region_end).unwrap();

      let mut larger_region_child_frames =
        PhysFrame::range(start_frame, end_frame).filter(|x| x.start_address().is_aligned(region_size));

      let all_frames_free = larger_region_child_frames.all(|child_frame| {
        self.get_bitmap(child_frame.start_address(), region_size).is_frame_free(child_frame).unwrap_or(false)
      });

      // Either all frames in THIS region size that are a child of the NEXT LARGER region size are free, or they aren't. Either way return it.
      Ok(all_frames_free)
    } else {
      Ok(false)
    }
  }

  fn prefetch_frame(&mut self, region_size: RegionSize) -> bool {
    if region_size == RegionSize::Size1GB {
      if self.next_1gb.is_none() {
        let mut next_frame: Option<PhysFrame<Size1GB>> = None;

        for bitmap in &mut self.frames_1gb {
          let next_free_frame: Option<PhysFrame<Size1GB>> = bitmap.next_free_frame();
          if let Some(next_free_frame) = next_free_frame {
            self.mark_frame_used(next_free_frame, Some(region_size)).expect("Error marking free frame used");
            next_frame = Some(next_free_frame);
            break;
          }
        }

        if let Some(frame) = next_frame {
          debug!("Prefetched {frame:?}");
          self.next_1gb = next_frame;
          true
        } else {
          false
        }
      } else {
        false
      }
    } else if region_size == RegionSize::Size2MB {
      if self.next_2mb.is_none() {
        let mut next_frame: Option<PhysFrame<Size2MB>> = None;

        for bitmap in &mut self.frames_2mb {
          let next_free_frame: Option<PhysFrame<Size2MB>> = bitmap.next_free_frame();
          if let Some(next_free_frame) = next_free_frame {
            self.mark_frame_used(next_free_frame, Some(region_size)).expect("Error marking free frame used");
            next_frame = Some(next_free_frame);
            break;
          }
        }

        if let Some(frame) = next_frame {
          debug!("Prefetched {frame:?}");
          self.next_2mb = next_frame;
          true
        } else {
          false
        }
      } else {
        false
      }
    } else {
      let (next_frame, bitmap_vec) = match region_size {
        RegionSize::Size32KB => (self.next_32kb, &mut self.frames_32kb),
        RegionSize::Size16KB => (self.next_16kb, &mut self.frames_16kb),
        RegionSize::Size8KB => (self.next_8kb, &mut self.frames_8kb),
        RegionSize::Size4KB => (self.next_4kb, &mut self.frames_4kb),
        _ => panic!("Unsupported region size passed to prefetch_frame: {:?}", region_size),
      };

      if next_frame.is_none() {
        let mut next_frame: Option<PhysFrame> = None;

        for bitmap in bitmap_vec {
          let next_free_frame: Option<PhysFrame> = bitmap.next_free_frame();
          if let Some(next_free_frame) = next_free_frame {
            self.mark_frame_used(next_free_frame, Some(region_size)).expect("Error marking free frame used");
            next_frame = Some(next_free_frame);
            break;
          }
        }

        let frame_ref = match region_size {
          RegionSize::Size32KB => &mut self.next_32kb,
          RegionSize::Size16KB => &mut self.next_16kb,
          RegionSize::Size8KB => &mut self.next_8kb,
          RegionSize::Size4KB => &mut self.next_4kb,
          _ => panic!("Unsupported region size passed to prefetch_frame: {:?}", region_size),
        };

        if let Some(frame) = next_frame {
          debug!("Prefetched {frame:?} for {region_size:?}");
          *frame_ref = next_frame;
          true
        } else {
          false
        }
      } else {
        false
      }
    }
  }
}

unsafe impl<S: PageSize> FrameMarker<S> for BuddyFrameAllocator {
  unsafe fn mark_frame_free(
    &mut self,
    frame: PhysFrame<S>,
    _region_size: Option<RegionSize>,
  ) -> Result<(), BitmapError> {
    if S::SIZE == Size1GB::SIZE {
      // In the case of a 1 GB frame being freed, all lower frames should be freed.
      for region_size in RegionSize::all_sizes() {
        let bitmap = self.get_bitmap(frame.start_address(), region_size);
        if region_size == RegionSize::Size1GB {
          bitmap.mark_frame_free(frame, None)?;
        } else if region_size == RegionSize::Size2MB {
          let sized_frame = PhysFrame::<Size2MB>::from_start_address(frame.start_address()).unwrap();
          bitmap.mark_frame_free(sized_frame, None)?;
        } else {
          let sized_frame = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();
          bitmap.mark_frame_free(sized_frame, None)?;
        }
      }
    } else if S::SIZE == Size2MB::SIZE {
      // We know it's a 2MB PhysFrame - so we can safely make a 2MB object and use that from now on
      let frame = PhysFrame::<Size2MB>::from_start_address(frame.start_address()).unwrap();

      // First, free this 2 MB frame and its children
      self.get_bitmap(frame.start_address(), RegionSize::Size2MB).mark_frame_free(frame, None)?;
      let frame_4kb = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();
      for region_size in RegionSize::all_sizes().iter().copied().filter(|x| *x < RegionSize::Size2MB) {
        self.get_bitmap(frame_4kb.start_address(), region_size).mark_frame_free(frame_4kb, None)?;
      }

      // Lastly, check the sibling 2 GB frames to see if we should free the parent 1 GB frame
      let parent_1gb_frame = PhysFrame::<Size1GB>::containing_address(frame.start_address());
      let first_sibling_frame = PhysFrame::<Size2MB>::from_start_address(parent_1gb_frame.start_address()).unwrap();
      let last_sibling_frame =
        PhysFrame::<Size2MB>::containing_address(first_sibling_frame.start_address() + RegionSize::Size1GB as u64);
      let mut sibling_frame_range = PhysFrame::range(first_sibling_frame, last_sibling_frame);

      let all_2mb_frames_free = sibling_frame_range.all(|sibling_frame| {
        self
          .get_bitmap(sibling_frame.start_address(), RegionSize::Size2MB)
          .is_frame_free(sibling_frame)
          .unwrap_or(false)
      });

      if all_2mb_frames_free {
        self
          .get_bitmap(parent_1gb_frame.start_address(), RegionSize::Size1GB)
          .mark_frame_free(parent_1gb_frame, None)?;
      }
    } else {
      // We know it's a 4KB PhysFrame - so we can safely make a 4KB object and use that from now on
      let frame = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();

      // Use the recursive function to free the frame from the smallest size, then work its way up. The result will be whether
      // or not all the frames are free at the region size we pass in. If that is true, then we will handle the 2 MB and 1 GB cases.
      let all_4kb_frames_free = self.free_4kb_frame(frame, RegionSize::Size32KB)?;
      if all_4kb_frames_free {
        // All the 4 KB frame regions have said this frame is free there, so start with the 2 MB frames
        let frame_2mb = PhysFrame::<Size2MB>::containing_address(frame.start_address());
        self.get_bitmap(frame_2mb.start_address(), RegionSize::Size2MB).mark_frame_free(frame_2mb, None)?;

        let parent_1gb_frame = PhysFrame::<Size1GB>::containing_address(frame.start_address());
        let first_sibling_frame = PhysFrame::<Size2MB>::from_start_address(parent_1gb_frame.start_address()).unwrap();
        let last_sibling_frame =
          PhysFrame::<Size2MB>::containing_address(first_sibling_frame.start_address() + RegionSize::Size1GB as u64);
        let mut sibling_frame_range = PhysFrame::range(first_sibling_frame, last_sibling_frame);

        let all_2mb_frames_free = sibling_frame_range.all(|sibling_frame| {
          self
            .get_bitmap(sibling_frame.start_address(), RegionSize::Size2MB)
            .is_frame_free(sibling_frame)
            .unwrap_or(false)
        });

        if all_2mb_frames_free {
          self
            .get_bitmap(parent_1gb_frame.start_address(), RegionSize::Size1GB)
            .mark_frame_free(parent_1gb_frame, None)?;
        }
      }
    }
    Ok(())
  }

  fn mark_frame_used(&mut self, frame: PhysFrame<S>, region_size: Option<RegionSize>) -> Result<(), BitmapError> {
    match S::SIZE {
      Size1GB::SIZE => {
        self.get_bitmap(frame.start_address(), RegionSize::Size1GB).mark_frame_used(frame, None)?;
        let start_2mb = PhysFrame::<Size2MB>::from_start_address(frame.start_address()).unwrap();
        let end_2mb = PhysFrame::<Size2MB>::containing_address(start_2mb.start_address() + RegionSize::Size1GB as u64);
        for frame_2mb in PhysFrame::range(start_2mb, end_2mb) {
          self.get_bitmap(frame_2mb.start_address(), RegionSize::Size2MB).mark_frame_used(frame_2mb, None)?;
        }

        let start_4kb = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();
        let end_4kb = PhysFrame::<Size4KB>::containing_address(start_4kb.start_address() + RegionSize::Size1GB as u64);
        for region_size in RegionSize::all_sizes().iter().copied().filter(|x| *x < RegionSize::Size2MB) {
          let frame_range = PhysFrame::range(start_4kb, end_4kb).filter(|x| x.start_address().is_aligned(region_size));
          for inner_frame in frame_range {
            self.get_bitmap(inner_frame.start_address(), region_size).mark_frame_used(inner_frame, None)?;
          }
        }
      }
      Size2MB::SIZE => {
        let frame_1gb = PhysFrame::<Size1GB>::containing_address(frame.start_address());
        self.get_bitmap(frame_1gb.start_address(), RegionSize::Size1GB).mark_frame_used(frame_1gb, None)?;
        self.get_bitmap(frame.start_address(), RegionSize::Size2MB).mark_frame_used(frame, None)?;

        let start_4kb = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();
        let end_4kb = PhysFrame::<Size4KB>::containing_address(start_4kb.start_address() + RegionSize::Size2MB as u64);
        trace!("Start of 4KB range is {start_4kb:?}, end is {end_4kb:?}");
        for region_size in RegionSize::all_sizes().iter().copied().filter(|x| *x < RegionSize::Size2MB) {
          trace!("Now attempting to mark frames aligned with {region_size:?} used");
          let frame_range = PhysFrame::range(start_4kb, end_4kb).filter(|x| x.start_address().is_aligned(region_size));
          for inner_frame in frame_range {
            let bitmap = self.get_bitmap(inner_frame.start_address(), region_size);
            match bitmap.mark_frame_used(inner_frame, None) {
              Ok(_) => (),
              Err(e) => {
                debug!("Got error while marking frame used: {e:?}");
                debug!("Frame being marked is {inner_frame:?}");
                debug!("Bitmap is {bitmap:?}");
                panic!("Error while attempting to mark frame {frame:?} used: {e:?}");
              }
            }
          }
        }
      }
      Size4KB::SIZE => {
        let frame_1gb = PhysFrame::<Size1GB>::containing_address(frame.start_address());
        self.get_bitmap(frame_1gb.start_address(), RegionSize::Size1GB).mark_frame_used(frame_1gb, None)?;

        let frame_2mb = PhysFrame::<Size2MB>::containing_address(frame.start_address());
        self.get_bitmap(frame_2mb.start_address(), RegionSize::Size2MB).mark_frame_used(frame_2mb, None)?;

        let region_size = region_size.unwrap_or(RegionSize::Size4KB);
        assert!(frame.start_address().is_aligned(region_size));
        for bigger_region in
          RegionSize::all_sizes().iter().copied().filter(|x| *x > region_size && *x < RegionSize::Size2MB)
        {
          self.get_bitmap(frame.start_address(), bigger_region).mark_frame_used(frame, None)?;
        }
        self.get_bitmap(frame.start_address(), region_size).mark_frame_used(frame, None)?;

        // This is ok because we know that frame is Size4KB in this match branch
        let start_frame = PhysFrame::<Size4KB>::from_start_address(frame.start_address()).unwrap();
        let end_frame = PhysFrame::<Size4KB>::containing_address(frame.start_address() + region_size as u64);

        for smaller_region in RegionSize::all_sizes().iter().copied().filter(|x| *x < region_size) {
          let frame_range =
            PhysFrame::range(start_frame, end_frame).filter(|x| x.start_address().is_aligned(smaller_region));
          for child_frame in frame_range {
            self.get_bitmap(child_frame.start_address(), smaller_region).mark_frame_used(child_frame, None)?;
          }
        }
      }
      _ => panic!("Unsupported frame size: {:#x}", S::SIZE),
    }

    Ok(())
  }

  fn next_free_frame(&mut self) -> Option<PhysFrame<S>> {
    unimplemented!()
  }

  fn is_frame_free(&self, _frame: PhysFrame<S>) -> Result<bool, BitmapError> {
    unimplemented!()
  }
}

unsafe impl<S: PageSize> FrameAllocator<S> for BuddyFrameAllocator {
  fn allocate_frame(&mut self) -> Option<PhysFrame<S>> {
    let next_frame = match S::SIZE {
      Size1GB::SIZE => {
        self.next_1gb.take().map(|frame| PhysFrame::<S>::from_start_address(frame.start_address()).unwrap())
      }
      Size2MB::SIZE => {
        self.next_2mb.take().map(|frame| PhysFrame::<S>::from_start_address(frame.start_address()).unwrap())
      }
      Size4KB::SIZE => {
        self.next_4kb.take().map(|frame| PhysFrame::<S>::from_start_address(frame.start_address()).unwrap())
      }
      _ => panic!("Unexpected PageSize given to allocator: {:#x}", S::SIZE),
    };

    // This clippy lint is a false positive
    #[allow(clippy::needless_match)]
    if let Some(frame) = next_frame {
      Some(frame)
    } else {
      let bitmap_vec = match S::SIZE {
        Size1GB::SIZE => &mut self.frames_1gb,
        Size2MB::SIZE => &mut self.frames_2mb,
        Size4KB::SIZE => &mut self.frames_4kb,
        _ => panic!("Unexpected PageSize given to allocator: {:#x}", S::SIZE),
      };

      for bitmap in bitmap_vec {
        if let Some(frame) = bitmap.next_free_frame() {
          self.mark_frame_used(frame, None).unwrap();
          return Some(frame);
        }
      }

      None
    }
  }
}

unsafe impl<S: PageSize> FrameAllocator<S> for Locked<BuddyFrameAllocator> {
  fn allocate_frame(&mut self) -> Option<PhysFrame<S>> {
    self.lock().allocate_frame()
  }
}

unsafe impl super::FrameAllocator for BuddyFrameAllocator {
  #[allow(clippy::type_complexity)]
  fn get_used_frames(
    &self,
  ) -> (Option<&[PhysFrame<Size4KB>]>, Option<&[PhysFrame<Size2MB>]>, Option<&[PhysFrame<Size1GB>]>) {
    todo!()
  }

  fn get_kernel_bounds(&self) -> PhysFrameRangeInclusive<Size4KB> {
    PhysFrame::range_inclusive(self.kernel_start, self.kernel_end)
  }

  fn get_multiboot_bounds(&self) -> PhysFrameRangeInclusive<Size4KB> {
    PhysFrame::range_inclusive(self.multiboot_start, self.multiboot_end)
  }

  fn allocate_region(&mut self, region_size: RegionSize) -> Option<FrameRegion> {
    let (next_frame, bitmap_vec) = match region_size {
      RegionSize::Size32KB => (self.next_32kb.take(), &mut self.frames_32kb),
      RegionSize::Size16KB => (self.next_16kb.take(), &mut self.frames_16kb),
      RegionSize::Size8KB => (self.next_8kb.take(), &mut self.frames_8kb),
      _ => panic!("Tried to allocate invalid 4KB region: {region_size:?}"),
    };

    if let Some(frame) = next_frame {
      Some(FrameRegion::from_start_frame(frame, region_size).unwrap())
    } else {
      for bitmap in bitmap_vec.iter_mut() {
        if let Some(frame) = bitmap.next_free_frame() {
          self.mark_frame_used(frame, Some(region_size)).unwrap();
          return Some(FrameRegion::from_start_frame(frame, region_size).unwrap());
        }
      }
      None
    }
  }
}

impl<S: PageSize> FrameDeallocator<S> for BuddyFrameAllocator {
  unsafe fn deallocate_frame(&mut self, frame: PhysFrame<S>) {
    self.mark_frame_free(frame, None).unwrap();
  }
}

fn divide_and_round_up(numerator: u64, denominator: u64) -> u64 {
  let clean_divide = numerator / denominator;
  if numerator % denominator == 0 {
    clean_divide
  } else {
    clean_divide + 1
  }
}

fn populate_bitmaps(bitmap_vec: &mut Vec<FrameBitmap>, region_size: RegionSize, num_areas: u64) {
  let num_full_bitmaps = num_areas / 64;
  for i in 0..num_full_bitmaps {
    let mut bitmap = FrameBitmap::new();
    let base_addr = PhysAddr::new(i * 64 * region_size);
    let max_addr = base_addr + region_size * 64;
    match bitmap.init(base_addr, max_addr, region_size) {
      Err(e) => {
        debug!("Error while initializing bitmap:\nregion_size: {region_size}\nnum_areas: {num_areas}\niteration: {i}\nbase_addr: {base_addr:?}\nmax_addr: {max_addr:?}");
        panic!("Error while initializing memory bitmap: {e:?}");
      }
      Ok(_) => bitmap_vec.push(bitmap),
    }
  }
  let partial_bitmap_length = num_areas % 64;
  if partial_bitmap_length > 0 {
    let mut bitmap = FrameBitmap::new();
    // num_full_bitmaps is the upper bound (exclusive) for the previous loop, so it is also the base address
    // if we need a partial bitmap
    let base_addr = PhysAddr::new(num_full_bitmaps * region_size);
    let max_addr = base_addr + region_size * partial_bitmap_length;
    match bitmap.init(base_addr, max_addr, region_size) {
      Err(e) => {
        debug!("Error while initializing bitmap:\nregion_size: {region_size}\nnum_areas: {num_areas}\npartial_bitmap_length: {partial_bitmap_length}\nbase_addr: {base_addr:?}\nmax_addr: {max_addr:?}");
        panic!("Error while initializing memory bitmap: {e:?}");
      }
      Ok(_) => bitmap_vec.push(bitmap),
    }
  }
}

pub extern "C" fn allocator_janitor() -> ! {
  let mut times_prefetched = [0u64; RegionSize::all_sizes().len()];
  let mut times_run = 0u64;
  loop {
    if let Some(mut allocator) = FRAME_ALLOCATOR.try_lock() {
      for (index, region_size) in RegionSize::all_sizes().iter().enumerate() {
        let prefetched = allocator.prefetch_frame(*region_size);
        if prefetched {
          times_prefetched[index] += 1;
        }
      }

      drop(allocator);
    }

    times_run += 1;
    if times_run % 1000 == 0 {
      debug!("After {times_run} runs, allocator prefetched:");
      for (index, region_size) in RegionSize::all_sizes().iter().enumerate() {
        debug!("{region_size:?}: {}", times_prefetched[index]);
      }
    }

    sleep(Duration::from_millis(500)).unwrap();
  }
}
