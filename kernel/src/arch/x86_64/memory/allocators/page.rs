use alloc::{collections::BTreeMap, vec::Vec};

use log::debug;
use x86_64::{
  structures::paging::{page::PageRange, Page, PageSize, Size1GiB as Size1GB, Size4KiB as Size4KB},
  VirtAddr,
};

use crate::util::power_of_two;

/// Trait for allocating a range of pages from some virtual memory pool
///
/// # Safety
/// The implementation of this trait must ensure that only unique free pages are returned
pub unsafe trait PageAllocator<S: PageSize> {
  fn allocate_pages(
    &mut self,
    addr: Option<VirtAddr>,
    max_addr: Option<VirtAddr>,
    length: Option<u64>,
  ) -> Option<PageRange<S>>;
}

pub trait PageDeallocator<S: PageSize> {
  unsafe fn deallocate_page(&mut self, page: Page<S>);
}

#[derive(Clone, Copy, Debug)]
pub struct PageBlock {
  start_addr: VirtAddr,
  length: u64,
}

#[derive(Clone, Copy, Debug)]
pub enum PageBlockError {
  AddressNotAligned,
  BadBlockSize,
}

impl PageBlock {
  pub const BLOCK_SIZE: u64 = Size4KB::SIZE;
  pub const MINIMUM_LENGTH: u64 = 1;
  pub const MAXIMUM_LENGTH: u64 = 16384 * Size1GB::SIZE / Self::BLOCK_SIZE;

  pub fn new(start_addr: VirtAddr, length: u64) -> Result<Self, PageBlockError> {
    // First check that the start address is aligned to page
    if !start_addr.is_aligned(Self::BLOCK_SIZE * length) {
      return Err(PageBlockError::AddressNotAligned);
    }

    let too_big = length > Self::MAXIMUM_LENGTH;
    let too_small = length < Self::MINIMUM_LENGTH;

    if !power_of_two(length) || too_big || too_small {
      return Err(PageBlockError::BadBlockSize);
    }

    Ok(Self { start_addr, length })
  }

  pub fn start_addr(&self) -> VirtAddr {
    self.start_addr
  }

  pub fn length(&self) -> u64 {
    self.length
  }

  pub fn split(&self) -> (Self, Option<Self>) {
    if self.length == Self::MINIMUM_LENGTH {
      // This is the smallest possible block, can't split it
      (*self, None)
    } else {
      let new_length = self.length / 2;
      let first_block = Self { start_addr: self.start_addr, length: new_length };
      let next_addr = self.start_addr + (new_length * Self::BLOCK_SIZE);
      let second_block = Self { start_addr: next_addr, length: new_length };
      (first_block, Some(second_block))
    }
  }

  pub fn merge(first: &Self, second: &Self) -> Result<Self, &'static str> {
    let next_addr = first.start_addr + (first.length * Self::BLOCK_SIZE);
    if next_addr == second.start_addr && first.length == second.length {
      let merged = Self {
        start_addr: first.start_addr,
        length: first.length * 2,
      };
      Ok(merged)
    } else {
      debug!("Unable to merge two page blocks {first:?} and {second:?}");
      Err("Unable to merge page blocks. Check that the regions of memory are adjacent and the same size.")
    }
  }

  pub fn overlaps(&self, other: &Self) -> bool {
    (other.start_addr >= self.start_addr && other.start_addr < self.end())
      || (other.end() > self.start_addr && other.end() < self.end())
      || (other.start_addr <= self.start_addr && other.end() >= self.end())
  }

  pub fn contains(&self, other: &Self) -> bool {
    other.start_addr >= self.start_addr && other.end() < self.end()
  }

  pub fn end(&self) -> VirtAddr {
    if u64::MAX - self.start_addr.as_u64() >= self.length * Self::BLOCK_SIZE {
      self.start_addr + self.length * Self::BLOCK_SIZE
    } else {
      VirtAddr::new(u64::MAX)
    }
  }
}

pub struct BTreePageAllocator {
  blocks: BTreeMap<VirtAddr, PageBlock>,
}

impl BTreePageAllocator {
  pub const fn new() -> Self {
    Self { blocks: BTreeMap::new() }
  }

  #[allow(clippy::too_many_lines)]
  pub fn init(&mut self, start_addr: VirtAddr, end_addr: VirtAddr, used_blocks: Option<&[PageBlock]>) {
    // We require that the end address is at least 1 GB past the start address
    assert!(end_addr >= start_addr);
    assert!(end_addr - start_addr >= Size1GB::SIZE);

    // We also require that the start address is aligned to 1 GB, and the end address to 4 KB
    assert!(start_addr.is_aligned(Size1GB::SIZE));
    assert!(
      end_addr.is_aligned(Size4KB::SIZE) || end_addr.as_u64() == u64::MAX || end_addr == super::super::LOWER_HALF_END
    );

    let mut block_length = PageBlock::MAXIMUM_LENGTH;
    let mut address_shift = block_length * PageBlock::BLOCK_SIZE;

    if start_addr == VirtAddr::zero() {
      // We don't actually want to give out the page starting at 0x0, so we subdivide the first block to insert smaller blocks until only that one remains
      let mut current_length = block_length;
      let mut current_block = PageBlock::new(start_addr, current_length)
        .expect("Somehow got an error constructing PageBlock in allocator init");
      while current_length > PageBlock::MINIMUM_LENGTH {
        let (first_block, second_block) = current_block.split();
        let second_block = second_block.expect("Failed to unwrap split block");
        self.blocks.insert(second_block.start_addr, second_block);
        current_block = first_block;
        current_length = first_block.length;
      }
    } else {
      let current_block =
        PageBlock::new(start_addr, block_length).expect("Unable to construct PageBlock which should be foolproof");
      self.blocks.insert(start_addr, current_block);
    }

    // Insert all remaining blocks for the address space
    let mut current_addr = start_addr + address_shift;
    while current_addr < end_addr {
      // Check that we have an entire 1 GB of address space left before the end
      if end_addr - current_addr >= address_shift {
        let current_block = PageBlock::new(current_addr, block_length);
        if current_block.is_err() {
          debug!("Tried to construct page block and failed. Address {current_addr:?}, length {block_length:#x}");
          debug!("Error {:?}", current_block.err().unwrap());
          panic!("Failed to construct page block");
        }
        let current_block = current_block.unwrap();
        self.blocks.insert(current_addr, current_block);
      } else if end_addr - current_addr == address_shift - 1 {
        // When working at the top of the address range it can be possible that we have an address_shift worth of space left
        // despite the above calculation.
        let current_block = PageBlock::new(current_addr, block_length);
        if current_block.is_err() {
          debug!("Tried to construct page block and failed. Address {current_addr:?}, length {block_length:#x}");
          debug!("Error {:?}", current_block.err().unwrap());
          panic!("Failed to construct page block");
        }
        let current_block = current_block.unwrap();
        self.blocks.insert(current_addr, current_block);
        break;
      } else if address_shift >= 2 * Size4KB::SIZE {
        // address_shift starts out pretty big, so there's still potentially a lot of address space left between current_addr
        // and end_addr. So to handle that case, we will keep making address_shift and block_length smaller to fit smaller and
        // smaller page blocks in before the end
        address_shift /= 2;
        block_length /= 2;
        continue;
      } else {
        let mut current_block_length = (end_addr - current_addr) / PageBlock::BLOCK_SIZE;
        let mut current_addr_inner = current_addr;
        while !power_of_two(current_block_length) {
          let smaller_block_length = {
            // It takes too long to iterate through numbers to find the largest power of two if the block length is really big.
            // So we take it in 16 GB chunks if we can.
            if current_block_length > 16 * Size1GB::SIZE / PageBlock::BLOCK_SIZE {
              16 * Size1GB::SIZE / PageBlock::BLOCK_SIZE
            } else {
              let mut result = 0;
              let mut i = current_block_length;
              while i >= 1 {
                if i & (i - 1) == 0 {
                  result = i;
                  break;
                }
                i -= 1;
              }
              result
            }
          };
          let split_block =
            PageBlock::new(current_addr_inner, smaller_block_length).expect("Unable to construct PageBlock");
          self.blocks.insert(current_addr, split_block);
          current_addr_inner += PageBlock::BLOCK_SIZE * smaller_block_length;
          current_block_length -= smaller_block_length;
        }

        let current_block = PageBlock::new(current_addr, current_block_length)
          .expect("Unable to construct PageBlock which should be foolproof");
        self.blocks.insert(current_addr, current_block);

        break; // We have to break the while loop here because we're at the end of the address range, and that can overflow the VirtAddr max
      }

      current_addr += address_shift;
    }

    if let Some(used_blocks) = used_blocks {
      // Remove any used blocks from the tree
      for used_block in used_blocks {
        let overlapping_blocks = self
          .blocks
          .iter()
          .filter(|(_, block)| used_block.overlaps(block))
          .map(|(addr, _)| *addr)
          .collect::<Vec<VirtAddr>>();

        for overlapping_addr in overlapping_blocks {
          // It is safe to unwrap this because we know this key exists
          let overlapping_block = self.blocks.remove(&overlapping_addr).unwrap();

          let mut current_block = overlapping_block;
          while current_block.length >= 2 * used_block.length {
            let (first_block, second_block) = current_block.split();
            // We can safely unwrap the second block because we know it must exist. The current block has length at least 2x the used block
            let second_block = second_block.unwrap();

            // Because current_block is 2x the size of used_block, splitting it is guaranteed to give a block that overlaps and one that doesn't.
            // Insert the non-overlapping block back to the tree, and keep iterating on the other one
            let (parent_block, other_block) = if used_block.overlaps(&first_block) {
              (first_block, second_block)
            } else {
              (second_block, first_block)
            };
            self.blocks.insert(other_block.start_addr, other_block);
            current_block = parent_block;
          }

          // At this point, current_block cannot be split further and overlaps. However, it could still be sized bigger than necessary.
          // To whittle that down, we need to iterate over the block until we have split it as much as possible, checking each split to
          // see if it has pages that aren't part of the used block
          while current_block.length > PageBlock::MINIMUM_LENGTH {
            let (first_block, second_block) = current_block.split();
            // Because current_block is greater than the minimum length it is safe to unwrap this
            let second_block = second_block.unwrap();

            // The fully contained block needs to all go, so we can ignore it. The other block is the one of interest, which may
            // or may not overlap the used block and need to keep getting split down
            let (_, not_contained) = if used_block.contains(&first_block) {
              (first_block, second_block)
            } else {
              (second_block, first_block)
            };
            if used_block.overlaps(&not_contained) {
              current_block = not_contained;
            } else {
              // If it turns out that not_contained doesn't overlap used_block at all, push it back to the block tree and exit the loop
              self.blocks.insert(not_contained.start_addr, not_contained);
              break;
            }
          }
        }
      }
    }
  }
}

unsafe impl<S: PageSize> PageAllocator<S> for BTreePageAllocator {
  #[allow(clippy::too_many_lines)]
  fn allocate_pages(
    &mut self,
    addr: Option<VirtAddr>,
    max_addr: Option<VirtAddr>,
    length: Option<u64>,
  ) -> Option<PageRange<S>> {
    let addr = match addr {
      Some(addr) => addr,
      None => VirtAddr::zero(),
    };

    let length = match length {
      Some(length) => {
        if length > 0 {
          let modifier = S::SIZE / PageBlock::BLOCK_SIZE;
          let mut adjusted_length = length * modifier;
          while !power_of_two(adjusted_length) {
            adjusted_length += 1;
          }
          adjusted_length
        } else {
          1
        }
      }
      None => 1,
    };

    let mut potential_parents = self
      .blocks
      .iter()
      .filter(|(start_addr, block)| {
        let start_addr_higher = **start_addr >= addr && start_addr.is_aligned(S::SIZE);
        let start_addr_lower_but_has_capacity = block.end() > addr;
        start_addr_higher || start_addr_lower_but_has_capacity
      })
      .filter(|(start_addr, _block)| {
        if let Some(max_addr) = max_addr {
          let addr_shift = length * PageBlock::BLOCK_SIZE;
          if u64::MAX - addr_shift < start_addr.as_u64() {
            false
          } else {
            **start_addr + addr_shift < max_addr
          }
        } else {
          true
        }
      });

    // Pick out contiguous blocks from the allocator until we have enough to cover the requested block length.
    // The last block we pick out will be split off so that we take only the amount needed from that region of memory
    let mut parent_blocks = Vec::<(VirtAddr, PageBlock)>::new();
    if let Some((start_addr, block)) = potential_parents.next() {
      let mut total_length = block.length();
      let mut prev_block = block;
      parent_blocks.push((*start_addr, *block));

      while total_length < length {
        if let Some((start_addr, block)) = potential_parents.next() {
          if prev_block.start_addr() + (prev_block.length() * PageBlock::BLOCK_SIZE) == block.start_addr() {
            // This block is contiguous with the previous block, so we can add it to the running list of blocks
            total_length += block.length();
            prev_block = block;
            parent_blocks.push((*start_addr, *block));
          } else {
            // This block is NOT contiguous with the previous block. Since BTreeMap stores the blocks sorted by key, that means
            // that the contiguous blocks have run out and we need to start over with looking for a contiguous series of blocks
            // that have the required size.
            total_length = block.length();
            prev_block = block;
            parent_blocks.clear();
            parent_blocks.push((*start_addr, *block));
          }
        } else {
          // If the iterator was exhausted and we haven't found enough blocks, that means that we can't satisfy this allocation request
          return None;
        }
      }
    } else {
      // If there were no results at all in the iterator, we can't satisfy the allocation request
      return None;
    }

    // At this point (since we haven't returned) we have a Vec full of blocks which can be joined together to fit the requested length.
    // All we need to do now is potentially split the last one off, working out the size of the block that we will return
    let block_start = parent_blocks.first().unwrap().0;

    // To split the appropriate amount off the last block, first we need to know how much blocks 0..n-1 have in terms of length
    let mut block_length = 0;
    for (start_addr, block) in &parent_blocks {
      self.blocks.remove(start_addr);
      if *start_addr != parent_blocks.last().unwrap().0 {
        block_length += block.length();
      }
    }

    // Then the remaining length to split off the last block is easy to calculate.
    let mut remaining_length = length - block_length;

    // Now start splitting blocks
    let mut current_block = parent_blocks.last().unwrap().1;
    while remaining_length > 0 {
      if current_block.length() >= remaining_length * 2 {
        // Block is at least 2x as big, split the block and put the second half back into the map
        let (first_block, second_block) = current_block.split();
        let second_block = second_block.unwrap();

        self.blocks.insert(second_block.start_addr(), second_block);
        current_block = first_block; // Next iteration will go on the first block that was split
      } else if current_block.length() > remaining_length {
        // Split the current block (if possible) and add the first half onto the block length
        let (first_block, second_block) = current_block.split();
        remaining_length -= first_block.length();
        block_length += first_block.length();

        // If there is a second block from the split, we may or may not need to keep iterating
        if let Some(second_block) = second_block {
          if remaining_length > 0 {
            current_block = second_block;
          } else {
            // No more length to split off, return second_block to the map
            self.blocks.insert(second_block.start_addr(), second_block);
          }
        }
      } else {
        // The current block is exactly as long as it needs to be
        remaining_length -= current_block.length();
        block_length += current_block.length();
      }
    }

    // At this point all unused blocks of memory should be returned to the map. All that we need to do is construct the PageRange and return it.
    // This unwrap should never fail because we requested aligned addresses from the map at first
    let first_page = Page::<S>::from_start_address(block_start).unwrap();

    let block_length = {
      // We store blocks internally as multiples of 4KB pages, so if this function call is requesting a larger page size we need to account for that.
      let modifier = S::SIZE / PageBlock::BLOCK_SIZE;
      block_length / modifier
    };

    let second_page = first_page + block_length;
    let page_range = Page::<S>::range(first_page, second_page);
    Some(page_range)
  }
}

impl<S: PageSize> PageDeallocator<S> for BTreePageAllocator {
  unsafe fn deallocate_page(&mut self, page: Page<S>) {
    let length = S::SIZE / PageBlock::BLOCK_SIZE;
    // This call cannot fail because page has a start address that is aligned to its size
    let page_block = PageBlock::new(page.start_address(), length).unwrap();
    self.blocks.insert(page.start_address(), page_block);
  }
}
