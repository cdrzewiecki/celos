use alloc::{collections::BTreeMap, vec::Vec};
use core::{alloc::Layout, convert::TryFrom, mem::align_of, ptr::write_bytes};

use contracts::*;
use lazy_static::lazy_static;
use log::{debug, trace, warn};
use multiboot2::{BootInformation, ElfSection, ElfSectionFlags, FramebufferType, MemoryArea};
use spin::Mutex;
use x86_64::{
  instructions::tlb,
  registers::control::Cr3,
  structures::paging::{
    mapper::{MapToError, MapperFlush},
    page_table::PageTableEntry,
    FrameAllocator, FrameDeallocator, Mapper, OffsetPageTable, Page, PageSize, PageTable, PageTableFlags,
    PageTableIndex, PhysFrame, RecursivePageTable, Size1GiB as Size1GB, Size2MiB as Size2MB, Size4KiB as Size4KB,
  },
  PhysAddr, VirtAddr,
};

mod allocators;
mod bitmap;
mod level4_entries;
#[allow(non_upper_case_globals)]
pub mod paging;

use allocators::{
  frame::{area::AreaFrameAllocator, buddy::BuddyFrameAllocator},
  heap::ALLOCATOR,
  page::{BTreePageAllocator, PageBlock},
};
use paging::{create_mapping, MappingFlags, ProtectionFlags, SharedMapping};

use crate::multitasking::{spawn_thread, ThreadCreationError, KERNEL_PID};

pub const PHYSICAL_MEMORY_OFFSET: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_a000_0000_0000) };
pub const KERNEL_STACK_ADDR: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_ffff_ffff_fff0) };
pub const KERNEL_INITIAL_STACK_SIZE: u32 = 0x20000;
pub const KERNEL_HEAP_START: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_9000_0000_0000) };
pub const KERNEL_HEAP_SIZE: usize = 2 * 1024 * 1024; // 2 MB

pub const KERNEL_SPACE_START: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_8000_0000_0000) };
pub const KERNEL_VMA: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_ffff_8000_0000) };
pub const KERNEL_STACK_AREA: VirtAddr = unsafe { VirtAddr::new_unsafe(0xffff_fff0_0000_0000) };
pub const LOWER_HALF_END: VirtAddr = unsafe { VirtAddr::new_unsafe(0x7fff_ffff_ffff) };

lazy_static! {
  static ref PAGE_TABLE: Mutex<OffsetPageTable<'static>> =
    unsafe { Mutex::new(OffsetPageTable::new(offset_p4_table(), PHYSICAL_MEMORY_OFFSET)) };
}

pub static KERNEL_MEMORY: Mutex<KernelMemoryInfo> = Mutex::new(KernelMemoryInfo::new());
static FRAME_ALLOCATOR: Mutex<BuddyFrameAllocator> = Mutex::new(BuddyFrameAllocator::new());
static PAGE_ALLOCATORS: Mutex<BTreeMap<PhysAddr, BTreePageAllocator>> = Mutex::new(BTreeMap::new());
pub static SHARED_MAPPINGS_4KB: Mutex<Vec<SharedMapping<Size4KB>>> = Mutex::new(Vec::new());
pub static SHARED_MAPPINGS_2MB: Mutex<Vec<SharedMapping<Size2MB>>> = Mutex::new(Vec::new());
pub static SHARED_MAPPINGS_1GB: Mutex<Vec<SharedMapping<Size1GB>>> = Mutex::new(Vec::new());

pub fn check_heap() -> u64 {
  ALLOCATOR.lock().total_allocated()
}

/// Perform memory initialization. This function:
/// * sets up a newer, cleaner page table
/// * zeroes out the boot trampoline and frees that memory
/// * maps the complete physical memory to the offset at `PHYSICAL_MEMORY_OFFSET`
/// * initializes a heap, as well as frame and page allocators for use
/// * unmaps the identity paging set up by the boot trampoline
///
/// # Safety
/// The caller must ensure that `bootinfo` refers to a valid multiboot2 info struct, and that
/// the bounds for the kernel given in the other parameters are accurate. Also, duh of course it's unsafe.
#[requires(kernel_virt_start >= KERNEL_VMA)]
#[requires(KERNEL_STACK_ADDR.as_u64() >= u64::from(KERNEL_INITIAL_STACK_SIZE))]
#[requires(usize::try_from(KERNEL_HEAP_START.as_u64()).unwrap() + KERNEL_HEAP_SIZE < usize::MAX)]
pub unsafe fn init(
  bootinfo: &BootInformation,
  kernel_virt_start: VirtAddr,
  kernel_virt_end: VirtAddr,
  kernel_phys_start: PhysAddr,
  kernel_phys_end: PhysAddr,
) {
  let kernel_stack_bottom = KERNEL_STACK_ADDR - u64::from(KERNEL_INITIAL_STACK_SIZE);

  x86_64::instructions::interrupts::without_interrupts(|| {
    KERNEL_MEMORY.lock().kernel_start = kernel_virt_start;
    KERNEL_MEMORY.lock().kernel_end = kernel_virt_end;
    KERNEL_MEMORY.lock().stack_bottom = kernel_stack_bottom;
    KERNEL_MEMORY.lock().stack_top = KERNEL_STACK_ADDR;
  });

  // Create a frame allocator based on the multiboot2 info struct
  let multiboot_start = VirtAddr::new(bootinfo.start_address() as u64);
  let multiboot_end = VirtAddr::new(bootinfo.end_address() as u64);
  let memory_map_tag = bootinfo.memory_map_tag().expect("Memory map tag required");
  let mut frame_allocator =
    AreaFrameAllocator::new(kernel_phys_start, kernel_phys_end, multiboot_start, multiboot_end, memory_map_tag);

  // To get better (fine grained and with the right protection) mappings, switch to a new page table
  // now that we're out of assembly and it's easier to do
  migrate_page_table(bootinfo, &mut frame_allocator);

  // We don't need the contents that were in .boot any more, so mark the kernel as starting at the LMA of the higher half
  // Zero those frames out, and then re-assign the start address variable
  zero_boot(kernel_phys_start, kernel_virt_start, multiboot_start, multiboot_end);
  let kernel_phys_start = PhysAddr::new(kernel_virt_start.as_u64() - KERNEL_VMA.as_u64());

  let mut p4_entries = level4_entries::UsedLevel4Entries::new(kernel_virt_start, kernel_virt_end);

  let recursive_index = PageTableIndex::new(u16::from(p4_entries.get_free_entry()));
  debug!("Chose {recursive_index:?} as the recursive page table index");
  let mut rec_page_table = create_recursive_table(recursive_index);

  map_physical_memory(bootinfo, PHYSICAL_MEMORY_OFFSET, &mut rec_page_table, &mut frame_allocator);

  init_heap(&mut frame_allocator).expect("Heap initialization failed!");

  // Unmap the recursive page
  let rec_page_table_addr =
    Page::from_page_table_indices(recursive_index, recursive_index, recursive_index, recursive_index).start_address();
  rec_page_table
    .unmap(Page::<Size4KB>::containing_address(rec_page_table_addr))
    .expect("Error deallocating recursive page table entry")
    .1
    .flush();

  debug!("Finished initializing heap and unmapping recursive page");

  let multiboot_addr = VirtAddr::new(bootinfo.start_address() as u64);
  x86_64::instructions::interrupts::without_interrupts(|| {
    FRAME_ALLOCATOR.lock().init(kernel_phys_start, multiboot_addr, &frame_allocator);
  });

  debug!("Finished initializing frame allocator");

  // Remove the identity mapping for the first GB of memory
  unmap_identity_pages();
  debug!("Finished unmapping identity pages");

  init_page_allocator();
  debug!("Finished initializing page allocator");
  debug!("Finished memory initialization");
  debug!("Total heap allocated: {:#x} bytes", ALLOCATOR.lock().total_allocated());

  let flags = MappingFlags::ExactMapping | MappingFlags::HugePage2MB;
  let protection = ProtectionFlags::Present | ProtectionFlags::Writable | ProtectionFlags::Global;
  let heap_end = KERNEL_HEAP_START + KERNEL_HEAP_SIZE as u64;
  let extra_heap_size = 1024 * 1024 * 100; // 100 MB
  debug!("Attempting to expand heap by {extra_heap_size:#x} bytes");
  let (p4_frame, _) = Cr3::read();
  match create_mapping::<Size2MB>(p4_frame.start_address(), Some(heap_end), None, extra_heap_size, flags, protection) {
    Ok(page_range) => {
      debug!("Got page range from {:?} to {:?}, extending heap", page_range.start, page_range.end);
      let size = usize::try_from(page_range.end.start_address().as_u64() - page_range.start.start_address().as_u64())
        .expect("Error converting size of heap page range to usize");
      ALLOCATOR.lock().extend(size);
    }
    Err(e) => {
      debug!("Unable to expand heap. Error: {e:?}");
    }
  }
}

/// Migrates the system to a new page table based on the mappings from ELF sections given by
/// the bootloader. Also sets up identity mapping for the first GB of physical memory.
///
/// Safety:
/// The caller must provide a valid `bootinfo` struct.
unsafe fn migrate_page_table(bootinfo: &BootInformation, frame_allocator: &mut impl FrameAllocator<Size4KB>) {
  let elf_section_tag = bootinfo.elf_sections_tag().expect("Unable to get ELF sections tag");

  // Get a new P4 table allocated
  let p4_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for new P4");
  assert!(
    p4_frame.start_address().as_u64() < Size1GB::SIZE,
    "Received allocation for new P4 table which is out of the identity mapped range: {:?}",
    p4_frame.start_address()
  );

  // Zero out allocated memory and create reference to the new page table
  #[allow(clippy::cast_possible_truncation)]
  write_bytes(p4_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);
  let p4 = &mut *(p4_frame.start_address().as_u64() as *mut PageTable);
  debug!("New page table will be at {:?}", p4_frame.start_address());

  // Iterate over every ELF section of the kernel to map it into the new page table
  for section in elf_section_tag.sections().filter(ElfSection::is_allocated) {
    if section.name() == ".boot" {
      continue; // We don't need the boot section mapped, or rather we won't after we migrate to the new page table
    }

    map_elf_section(&section, p4, frame_allocator);
  }

  // We also need to add identity-mapped entries for the first gigabyte, so that the rest of the init() function
  // can work with the identity-mapped memory
  let start_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::new(0x0));
  let end_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::new(Size1GB::SIZE));
  for frame in PhysFrame::range(start_frame, end_frame) {
    let virt_frame = VirtAddr::new(frame.start_address().as_u64());
    let p4_index = virt_frame.p4_index();
    let p3_index = virt_frame.p3_index();
    let p2_index = virt_frame.p2_index();

    if p4[p4_index].is_unused() {
      // Allocate a new P3 table, and zero it out
      let p3_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for P3 table");
      assert!(
        p3_frame.start_address().as_u64() < Size1GB::SIZE,
        "Received allocation for new P3 table which is out of the identity mapped range: {:?}",
        p3_frame.start_address()
      );

      #[allow(clippy::cast_possible_truncation)]
      write_bytes(p3_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);

      // Write the address for the new P3 table into the P4 table
      let mut p4_entry = PageTableEntry::new();
      p4_entry.set_frame(p3_frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE);
      p4[p4_index] = p4_entry;
    }

    // P4 entry is written, now check the P3 table
    let p4_entry = &p4[p4_index];
    let p3 = &mut *(p4_entry.addr().as_u64() as *mut PageTable);

    if p3[p3_index].is_unused() {
      // Allocate a new P2 table, and zero it out
      let p2_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for P2 table");
      assert!(
        p2_frame.start_address().as_u64() < Size1GB::SIZE,
        "Received allocation for new P2 table which is out of the identity mapped range: {:?}",
        p2_frame.start_address()
      );

      #[allow(clippy::cast_possible_truncation)]
      write_bytes(p2_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);

      // Write the address for the new P2 table into the P3 table
      let mut p3_entry = PageTableEntry::new();
      p3_entry.set_frame(p2_frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE);
      p3[p3_index] = p3_entry;
    }

    // P3 entry is written, now check the P2 table
    let p3_entry = &p3[p3_index];
    let p2 = &mut *(p3_entry.addr().as_u64() as *mut PageTable);

    let mut p2_entry = PageTableEntry::new();
    p2_entry
      .set_addr(frame.start_address(), PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::HUGE_PAGE);
    p2[p2_index] = p2_entry;
  }

  // Finally, write the new P4 address to CR3
  let (_, cr3_flags) = Cr3::read();
  debug!("About to write CR3 {p4_frame:?} with flags {cr3_flags:?}");

  Cr3::write(p4_frame, cr3_flags);
  debug!("Wrote CR3");
}

/// Map an ELF section into the page table given in `p4`. P4 must be the root of its page table hierarchy.
#[requires(section.start_address() >= KERNEL_VMA.as_u64())]
fn map_elf_section(section: &ElfSection, p4: &mut PageTable, frame_allocator: &mut impl FrameAllocator<Size4KB>) {
  // Calculate all the addresses (virtual and physical) we need to work with
  let start_addr_phys = PhysAddr::new(section.start_address() - KERNEL_VMA.as_u64());
  let end_addr_phys = PhysAddr::new(section.end_address() - KERNEL_VMA.as_u64());
  let end_addr_phys = if end_addr_phys.is_aligned(Size4KB::SIZE) {
    end_addr_phys - 1u64
  } else {
    end_addr_phys
  };
  let start_frame = PhysFrame::<Size4KB>::containing_address(start_addr_phys);
  let end_frame = PhysFrame::<Size4KB>::containing_address(end_addr_phys);
  let section_addr_virt = if section.name() == ".bss.kernel_stack" {
    let num_frames = end_frame - start_frame;
    let last_frame_virt = u64::MAX - 0xfff;
    trace!(
      "num_frames {}, last_frame_virt {:#x}, section_addr_virt {:?}",
      num_frames,
      last_frame_virt,
      VirtAddr::new(last_frame_virt - num_frames * Size4KB::SIZE)
    );
    VirtAddr::new(last_frame_virt - num_frames * Size4KB::SIZE)
  } else {
    VirtAddr::new(section.start_address())
  };

  // Now iterate over the frames in the section, creating a mapping for each
  for frame in PhysFrame::range_inclusive(start_frame, end_frame) {
    // This is the virtual address corresponding to the start of this frame
    let frame_addr_virt = {
      if section.name() == ".bss.kernel_stack" {
        let delta = frame.start_address() - start_addr_phys;
        section_addr_virt + delta
      } else {
        KERNEL_VMA + frame.start_address().as_u64()
      }
    };

    trace!("Section {} - mapping {:?} to {:?}", section.name(), frame_addr_virt, frame.start_address());

    let p4_index = frame_addr_virt.p4_index();
    let p3_index = frame_addr_virt.p3_index();
    let p2_index = frame_addr_virt.p2_index();
    let p1_index = frame_addr_virt.p1_index();

    if p4[p4_index].is_unused() {
      // Allocate a new P3 table, and zero it out
      let p3_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for P3 table");
      assert!(
        p3_frame.start_address().as_u64() < Size1GB::SIZE,
        "Received allocation for new P3 table which is out of the identity mapped range: {:?}",
        p3_frame.start_address()
      );

      unsafe {
        #[allow(clippy::cast_possible_truncation)]
        write_bytes(p3_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);
      }

      // Write the address for the new P3 table into the P4 table
      let mut p4_entry = PageTableEntry::new();
      p4_entry.set_frame(p3_frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE);
      p4[p4_index] = p4_entry;
    }

    // P4 entry is written, now check the P3 table
    let p4_entry = &p4[p4_index];
    let p3 = unsafe { &mut *(p4_entry.addr().as_u64() as *mut PageTable) };

    if p3[p3_index].is_unused() {
      // Allocate a new P2 table, and zero it out
      let p2_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for P2 table");
      assert!(
        p2_frame.start_address().as_u64() < Size1GB::SIZE,
        "Received allocation for new P2 table which is out of the identity mapped range: {:?}",
        p2_frame.start_address()
      );

      unsafe {
        #[allow(clippy::cast_possible_truncation)]
        write_bytes(p2_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);
      }

      // Write the address for the new P2 table into the P3 table
      let mut p3_entry = PageTableEntry::new();
      p3_entry.set_frame(p2_frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::GLOBAL);
      p3[p3_index] = p3_entry;
    }

    // P3 entry is written, now check the P2 table
    let p3_entry = &p3[p3_index];
    let p2 = unsafe { &mut *(p3_entry.addr().as_u64() as *mut PageTable) };

    if p2[p2_index].is_unused() {
      // Allocate a new P1 table, and zero it out
      let p1_frame = frame_allocator.allocate_frame().expect("Unable to allocate frame for P1 table");
      assert!(
        p1_frame.start_address().as_u64() < Size1GB::SIZE,
        "Received allocation for new P1 table which is out of the identity mapped range: {:?}",
        p1_frame.start_address()
      );

      unsafe {
        #[allow(clippy::cast_possible_truncation)]
        write_bytes(p1_frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);
      }

      //Write the address for the new P1 table into the P2 table
      let mut p2_entry = PageTableEntry::new();
      p2_entry.set_frame(p1_frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::GLOBAL);
      p2[p2_index] = p2_entry;
    }

    // P2 entry is written, now we can write to the appropriate entry of P1
    let p2_entry = &p2[p2_index];
    let p1 = unsafe { &mut *(p2_entry.addr().as_u64() as *mut PageTable) };

    if p1[p1_index].is_unused() {
      // Calculate P1 flags based on the flags of the ELF section, and write P1 entry
      let mut p1_entry = PageTableEntry::new();
      let mut p1_flags = PageTableFlags::PRESENT | PageTableFlags::GLOBAL;
      // For whatever reason the section for the kernel stack isn't labeled as writable in the ELF file
      // So set that flag for the .bss.kernel_stack section, regardless of what ELF says
      if section.flags().contains(ElfSectionFlags::WRITABLE) || section.name() == ".bss.kernel_stack" {
        p1_flags |= PageTableFlags::WRITABLE;
      }
      if !section.flags().contains(ElfSectionFlags::EXECUTABLE) {
        p1_flags |= PageTableFlags::NO_EXECUTE;
      }
      p1_entry.set_frame(frame, p1_flags);
      p1[p1_index] = p1_entry;
    } else {
      warn!("Somehow there was a non-zeroed entry in the page table we are building. Section {:?}", section);
      // Calculate P1 flags based on the flags of the ELF section, and write P1 entry
      let p1_entry = &mut p1[p1_index];
      let mut p1_flags = p1_entry.flags();
      // For whatever reason the section for the kernel stack isn't labeled as writable in the ELF file
      // So set that flag for the .bss.kernel_stack section, regardless of what ELF says
      if section.flags().contains(ElfSectionFlags::WRITABLE) || section.name() == ".bss.kernel_stack" {
        p1_flags |= PageTableFlags::WRITABLE;
      }
      if !section.flags().contains(ElfSectionFlags::EXECUTABLE) {
        p1_flags |= PageTableFlags::NO_EXECUTE;
      }
      p1_entry.set_flags(p1_flags);
    }
  }
}

/// Zeroes out the boot area that exists before the start of the kernel in higher-half virtual memory.
/// Also frees those frames so that they can be used.
///
/// Safety:
/// The caller must ensure that all the parameters are accurate as to the bounds of the kernel and
/// multiboot info structure. If they are not correct it can cause the system to fail.
unsafe fn zero_boot(
  kernel_phys_start: PhysAddr,
  kernel_virt_start: VirtAddr,
  multiboot_start: VirtAddr,
  multiboot_end: VirtAddr,
) {
  debug!("Zeroing boot section");
  let start_frame = PhysFrame::<Size4KB>::containing_address(kernel_phys_start);
  let end_frame = if kernel_virt_start.is_aligned(Size4KB::SIZE) {
    PhysFrame::<Size4KB>::containing_address(PhysAddr::new(kernel_virt_start.as_u64() - KERNEL_VMA.as_u64() - 1))
  } else {
    let virt_frame_start = kernel_virt_start.align_down(Size4KB::SIZE);
    PhysFrame::<Size4KB>::containing_address(PhysAddr::new(virt_frame_start.as_u64() - KERNEL_VMA.as_u64() - 1))
  };
  debug!("Boot section is from {start_frame:?} to {end_frame:?}");
  let multiboot_range = PhysFrame::<Size4KB>::range_inclusive(
    PhysFrame::containing_address(PhysAddr::new(multiboot_start.as_u64())),
    PhysFrame::containing_address(PhysAddr::new(multiboot_end.as_u64())),
  );
  debug!("Multiboot is from {:?} to {:?} (inclusive)", multiboot_range.start, multiboot_range.end);
  for frame in PhysFrame::range_inclusive(start_frame, end_frame) {
    if frame == multiboot_range.start {
      let max_addr = multiboot_start;
      let count =
        usize::try_from(max_addr.as_u64() - frame.start_address().as_u64()).expect("Failed to convert count to usize");
      debug!("Zeroing only {} bytes from {:?} as multiboot exists in this frame", count, frame.start_address());
      write_bytes(frame.start_address().as_u64() as *mut u8, 0, count);
    } else if frame == multiboot_range.end {
      let count = usize::try_from((frame.start_address().as_u64() + frame.size()) - multiboot_end.as_u64())
        .expect("Failed to convert count to usize");
      debug!("Zeroing only {count} bytes from {multiboot_end:?} as multiboot exists in this frame");
      write_bytes(multiboot_end.as_mut_ptr::<u8>(), 0, count);
    } else {
      #[allow(clippy::cast_possible_truncation)]
      write_bytes(frame.start_address().as_u64() as *mut u8, 0, Size4KB::SIZE as usize);
    }
  }
}

/// Function to create the recursive page table we need just until the offset page table is setup
/// Requires a `PageTableIndex` to use as the recursive entry of the page table
fn create_recursive_table(idx: PageTableIndex) -> RecursivePageTable<'static> {
  // First, get the current P4 address from CR3
  let (p4_frame, _) = Cr3::read();
  let p4_physical = p4_frame.start_address();

  // Then create an entry in the P4 table which is recursive
  let mut entry = PageTableEntry::new();
  entry.set_addr(p4_physical, PageTableFlags::PRESENT | PageTableFlags::WRITABLE);

  // Write the recursive entry into the page table
  let page_table = unsafe { &mut *(p4_physical.as_u64() as *mut PageTable) };
  page_table[idx] = entry;
  tlb::flush_all();

  let recursive_page_table_addr = Page::from_page_table_indices(idx, idx, idx, idx).start_address();
  let inner_page_table = unsafe { &mut *(recursive_page_table_addr.as_mut_ptr::<PageTable>()) };
  RecursivePageTable::new(inner_page_table).expect("Recursive page table creation failed")
}

/// Maps the complete physical memory to virtual memory at the offset provided by `physical_memory_offset`
fn map_physical_memory(
  bootinfo: &BootInformation,
  physical_memory_offset: VirtAddr,
  rec_page_table: &mut RecursivePageTable,
  frame_allocator: &mut impl FrameAllocator<Size4KB>,
) {
  // This will eventually need to read the EFI memory tag as well. For now, not worrying about that.
  let memmap = bootinfo.memory_map_tag().expect("No memory map in multiboot info!");
  let max_phys_addr = memmap
    .memory_areas()
    .map(MemoryArea::end_address)
    .max()
    .expect("No available memory regions in multiboot info!");

  // We take a free entry from the P4 table and map memory there. This works if the total
  // physical memory fits into a single P4 entry, asserted here
  assert!(max_phys_addr < (1 << 48) / 512);

  let virt_for_phys = |phys: PhysAddr| -> VirtAddr { VirtAddr::new(phys.as_u64() + physical_memory_offset.as_u64()) };

  let start_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::zero());
  let end_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::new(max_phys_addr));

  for frame in PhysFrame::range_inclusive(start_frame, end_frame) {
    let page = Page::containing_address(virt_for_phys(frame.start_address()));
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    unsafe { init_map_page(page, frame, flags, rec_page_table, frame_allocator) }
      .expect("Offset mapping of page failed")
      .flush();
  }

  let framebuffer_info = bootinfo.framebuffer_tag().expect("No framebuffer tag found");

  // Map video memory
  let video_start_addr = framebuffer_info.address;

  #[allow(clippy::match_wildcard_for_single_variants)]
  let video_end_addr = match framebuffer_info.buffer_type {
    FramebufferType::Text => {
      // In text mode the height and width are expressed by GRUB in characters, not pixels. bpp is 16 (16 bits per char)
      // So we use width instead of pitch in this case
      let buffer_size =
        u64::from(framebuffer_info.height) * u64::from(framebuffer_info.width) * u64::from(framebuffer_info.bpp);
      video_start_addr + buffer_size
    }
    FramebufferType::RGB { .. } => {
      // In graphics mode the length of the buffer is pixels per line * num of lines * bits per pixel
      let buffer_size =
        u64::from(framebuffer_info.height) * u64::from(framebuffer_info.pitch) * u64::from(framebuffer_info.bpp);
      video_start_addr + buffer_size
    }
    _ => panic!("Unsupported framebuffer type: {:?}", framebuffer_info.buffer_type),
  };

  let video_start_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::new(video_start_addr));
  let video_end_frame = PhysFrame::<Size2MB>::containing_address(PhysAddr::new(video_end_addr));
  for frame in PhysFrame::range_inclusive(video_start_frame, video_end_frame) {
    let page = Page::containing_address(virt_for_phys(frame.start_address()));
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    let map_result = unsafe { init_map_page(page, frame, flags, rec_page_table, frame_allocator) };
    match map_result {
      Ok(mapper_flush) => mapper_flush.flush(),
      Err(err) => match err {
        MapToError::PageAlreadyMapped(..) => {}
        _ => panic!("Unable to map video page: {err:?}"),
      },
    }
  }
}

/// Helper function to map a page to a physical frame of memory. Should only be used during the `init` process.
///
/// Safety:
/// The caller must guarantee that `phys_frame` points to unique free physical memory.
unsafe fn init_map_page<'a, S>(
  page: Page<S>,
  phys_frame: PhysFrame<S>,
  flags: PageTableFlags,
  page_table: &mut RecursivePageTable<'a>,
  frame_allocator: &mut impl FrameAllocator<Size4KB>,
) -> Result<MapperFlush<S>, MapToError<S>>
where
  S: PageSize,
  RecursivePageTable<'a>: Mapper<S>,
{
  page_table.map_to(page, phys_frame, flags, frame_allocator)
}

/// Init the heap at the memory location and size specified by `KERNEL_HEAP_START` and `KERNEL_HEAP_SIZE`.
fn init_heap(frame_allocator: &mut impl FrameAllocator<Size4KB>) -> Result<(), MapToError<Size4KB>> {
  let page_range = {
    let heap_end = KERNEL_HEAP_START + KERNEL_HEAP_SIZE - 1_u64;
    let heap_start_page = Page::containing_address(KERNEL_HEAP_START);
    let heap_end_page = Page::containing_address(heap_end);
    Page::range_inclusive(heap_start_page, heap_end_page)
  };

  for page in page_range {
    let frame = frame_allocator.allocate_frame().ok_or(MapToError::FrameAllocationFailed)?;
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    unsafe {
      PAGE_TABLE.lock().map_to(page, frame, flags, frame_allocator)?.flush();
    }
  }

  unsafe {
    allocators::heap::ALLOCATOR
      .lock()
      .init(usize::try_from(KERNEL_HEAP_START.as_u64()).unwrap(), KERNEL_HEAP_SIZE);
  }

  Ok(())
}

/// Unmaps the identity paging for the first GB of memory. Also frees the page tables that held
/// the identity mappings if they are now empty.
fn unmap_identity_pages() {
  let start_page = unsafe { Page::<Size2MB>::from_start_address_unchecked(VirtAddr::zero()) };
  let end_page = unsafe { Page::<Size2MB>::from_start_address_unchecked(VirtAddr::new(Size1GB::SIZE)) };
  let (p4_frame, _) = Cr3::read();
  let p4 = unsafe { &*(p4_frame.start_address().as_u64() as *const PageTable) };
  let p4_index = start_page.start_address().p4_index();
  let p3_index = start_page.start_address().p3_index();
  let p3_frame = p4[p4_index].frame().expect("Error getting frame for P3 table");
  let p3 = unsafe { &*(p3_frame.start_address().as_u64() as *const PageTable) };
  let p2_frame = p3[p3_index].frame().expect("Error getting frame for P2 table");

  // Remove and invalidate all of the identity mapped entries
  for page in Page::range(start_page, end_page) {
    PAGE_TABLE.lock().unmap(page).expect("Error deallocating identity paging entry").1.flush();
  }

  // Deallocate the frame that was holding the identity mapped P2 table
  unsafe {
    FRAME_ALLOCATOR.lock().deallocate_frame(p2_frame);
  }

  // We need to free the P3 table if everything is unused (which it should be). But because we removed the identity
  // mapping, we have to make use of the offset physical memory mapping to do that.
  let virt_p3 = VirtAddr::new(p3_frame.start_address().as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64());
  let virt_p3_table = unsafe { &*virt_p3.as_ptr::<PageTable>() };
  if virt_p3_table.iter().all(PageTableEntry::is_unused) {
    let virt_p4 = VirtAddr::new(p4_frame.start_address().as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64());
    let virt_p4_table = unsafe { &mut *virt_p4.as_mut_ptr::<PageTable>() };
    virt_p4_table[p4_index].set_unused();
    unsafe {
      FRAME_ALLOCATOR.lock().deallocate_frame(p3_frame);
    }
  }
}

/// Initializes the page allocator, marking as used the areas already in use by the kernel. Those areas are:
/// * the kernel itself (the first GB of memory past `KERNEL_VMA`)
/// * the heap (`KERNEL_HEAP_SIZE` bytes past `KERNEL_HEAP_START`)
/// * the physical memory area (the entire 16 TB past `PHYSICAL_MEMORY_OFFSET`)
/// * the kernel stack area (`KERNEL_STACK_AREA` through the upper limit of memory)
fn init_page_allocator() {
  let mut paging_entries = Vec::<PageBlock>::new();
  let kernel_start_page = Page::<Size1GB>::from_start_address(KERNEL_VMA).unwrap();
  let kernel_end_page = kernel_start_page + 1;
  let length = (kernel_end_page.start_address() - kernel_start_page.start_address()) / Size4KB::SIZE;
  debug!("Kernel length {length:#x}");
  debug!("Page block maximum length {:#x}", PageBlock::MAXIMUM_LENGTH);

  let kernel_page_block = PageBlock::new(kernel_start_page.start_address(), length).unwrap();
  paging_entries.push(kernel_page_block);

  let heap_start_page = Page::<Size2MB>::from_start_address(KERNEL_HEAP_START).unwrap();
  let heap_end_page = heap_start_page + 1;
  let length = (heap_end_page.start_address() - heap_start_page.start_address()) / Size4KB::SIZE;

  let heap_page_block = PageBlock::new(heap_start_page.start_address(), length).unwrap();
  paging_entries.push(heap_page_block);

  let phys_mem_start_page = Page::<Size1GB>::from_start_address(PHYSICAL_MEMORY_OFFSET).unwrap();
  let phys_mem_end_page = Page::<Size1GB>::from_start_address(PHYSICAL_MEMORY_OFFSET + 0x1000_0000_0000u64).unwrap();
  let length = Size1GB::SIZE / Size4KB::SIZE;

  for page in Page::range(phys_mem_start_page, phys_mem_end_page) {
    let page_block = PageBlock::new(page.start_address(), length).unwrap();
    paging_entries.push(page_block);
  }

  let stack_start_addr = VirtAddr::new(u64::MAX - (u64::from(KERNEL_INITIAL_STACK_SIZE) - 1));
  let stack_end_addr = VirtAddr::new(u64::MAX);

  let stack_start_page = Page::<Size4KB>::containing_address(stack_start_addr);
  let stack_end_page = Page::<Size4KB>::containing_address(stack_end_addr);

  for page in Page::range(stack_start_page, stack_end_page) {
    let page_block = PageBlock::new(page.start_address(), 1).unwrap();
    paging_entries.push(page_block);
  }

  // We need to push the last one manually due to a bug in the current version of x86_64, see https://github.com/rust-osdev/x86_64/issues/346.
  let page_block = PageBlock::new(stack_end_page.start_address(), 1).unwrap();
  paging_entries.push(page_block);

  debug!("Finished allocating paging entries");
  debug!("Total heap allocated: {:#x} bytes", ALLOCATOR.lock().total_allocated());

  let mut kernel_page_allocator = BTreePageAllocator::new();
  kernel_page_allocator.init(KERNEL_SPACE_START, VirtAddr::new(u64::MAX), Some(paging_entries.as_slice()));

  let (p4_frame, _) = Cr3::read();
  PAGE_ALLOCATORS.lock().insert(p4_frame.start_address(), kernel_page_allocator);
}

/// Creates a `PageTable` struct that points to the virtual address (physical + `PHYSICAL_MEMORY_OFFSET`)
/// for the P4 table in CR3.
fn offset_p4_table() -> &'static mut PageTable {
  let (p4_frame, _) = Cr3::read();
  let phys = p4_frame.start_address();
  let virt = VirtAddr::new(phys.as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64());
  let page_table_ptr = virt.as_mut_ptr::<PageTable>();
  unsafe { &mut *page_table_ptr }
}

#[allow(dead_code)]
/// Translates a virtual address to the physical address it represents.
pub fn translate_addr(addr: VirtAddr) -> Option<PhysAddr> {
  use x86_64::structures::paging::Translate;

  let (p4_frame, _) = Cr3::read();
  let p4_virtual = VirtAddr::new(PHYSICAL_MEMORY_OFFSET.as_u64() + p4_frame.start_address().as_u64());
  let p4_pointer: *mut PageTable = p4_virtual.as_mut_ptr();
  let page_table = unsafe { &mut *p4_pointer };
  let mapper = unsafe { OffsetPageTable::new(page_table, PHYSICAL_MEMORY_OFFSET) };

  mapper.translate_addr(addr)
}

pub struct KernelMemoryInfo {
  pub kernel_start: VirtAddr,
  pub kernel_end: VirtAddr,
  pub stack_top: VirtAddr,
  pub stack_bottom: VirtAddr,
}

impl KernelMemoryInfo {
  pub const fn new() -> Self {
    KernelMemoryInfo {
      kernel_start: VirtAddr::zero(),
      kernel_end: VirtAddr::zero(),
      stack_top: VirtAddr::zero(),
      stack_bottom: VirtAddr::zero(),
    }
  }
}

pub fn kernel_alloc(size: usize) -> Option<*mut u8> {
  let align = align_of::<usize>();
  kernel_alloc_aligned(size, align)
}

pub fn kernel_alloc_aligned(size: usize, align: usize) -> Option<*mut u8> {
  use core::alloc::GlobalAlloc;

  match Layout::from_size_align(size, align) {
    Ok(layout) => {
      let allocation = unsafe { ALLOCATOR.alloc(layout) };

      if allocation.is_null() {
        None
      } else {
        Some(allocation)
      }
    }
    Err(err) => {
      debug!("Layout error when allocating memory: {err:?}");
      None
    }
  }
}

/// Returns a frame to the frame allocator.
///
/// # Safety
/// The caller must guarantee that the frame is not in use any more
pub unsafe fn free_frame<S: PageSize>(frame: PhysFrame<S>) {
  FRAME_ALLOCATOR.lock().deallocate_frame(frame);
}

pub fn allocate_frame<S: PageSize>() -> Option<PhysFrame<S>> {
  FRAME_ALLOCATOR.lock().allocate_frame()
}

pub fn init_process_page_allocator(p4_address: PhysAddr) {
  let mut allocator = BTreePageAllocator::new();
  allocator.init(VirtAddr::zero(), LOWER_HALF_END, None);
  PAGE_ALLOCATORS.lock().insert(p4_address, allocator);
}

pub fn remove_process_page_allocator(p4_address: PhysAddr) {
  PAGE_ALLOCATORS.lock().remove(&p4_address);
}

pub(crate) fn init_background_threads() -> Result<(), ThreadCreationError> {
  spawn_thread(KERNEL_PID, allocators::frame::buddy::allocator_janitor)
}
