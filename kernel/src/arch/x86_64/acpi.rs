use core::{
  ptr::NonNull,
  sync::atomic::{AtomicU8, AtomicUsize, Ordering},
};

use acpi::{AcpiHandler, PhysicalMapping};
use contracts::*;
use log::{debug, trace};
use multiboot2::BootInformation;
use x86_64::{
  align_down,
  registers::control::Cr3,
  structures::paging::{Page, PageSize, Size4KiB as Size4KB},
  PhysAddr, VirtAddr,
};

use super::memory::paging::{self, MappingFlags, ProtectionFlags};
use crate::arch::memory::KERNEL_VMA;

pub(in crate::arch::x86_64) static RDST_ADDR: AtomicUsize = AtomicUsize::new(0);
pub(in crate::arch::x86_64) static RDST_REV: AtomicU8 = AtomicU8::new(0);

#[derive(Clone, Copy)]
pub(in crate::arch::x86_64) struct Handler;

impl AcpiHandler for Handler {
  unsafe fn map_physical_region<T>(&self, physical_address: usize, size: usize) -> PhysicalMapping<Self, T> {
    trace!("Mapping ACPI region at {physical_address:#x} of size {size}");
    let (p4_frame, _) = Cr3::read();
    let phys = PhysAddr::new(physical_address as u64);
    let protection = ProtectionFlags::Present | ProtectionFlags::Writable | ProtectionFlags::Global;

    match paging::create_mapping::<Size4KB>(
      p4_frame.start_address(),
      Some(KERNEL_VMA),
      Some(phys),
      size,
      MappingFlags::empty(),
      protection,
    ) {
      Ok(page_range) => {
        let physical_start = physical_address;
        let region_length = size;
        let mapped_length = usize::try_from((page_range.end - page_range.start) * Size4KB::SIZE)
          .expect("Unable to convert length to usize");
        #[allow(clippy::cast_possible_truncation)]
        let ptr_offset = physical_address - align_down(physical_address as u64, Size4KB::SIZE) as usize;
        let ptr = (page_range.start.start_address() + ptr_offset as u64).as_mut_ptr::<T>();
        let virtual_start = NonNull::<T>::new(ptr).expect("Pointer to virtual start is null");
        let mapping: PhysicalMapping<Self, T> =
          PhysicalMapping::new(physical_start, virtual_start, region_length, mapped_length, *self);

        mapping
      }
      Err(e) => panic!("Got error {e:?} while mapping ACPI page"),
    }
  }

  #[requires(u64::MAX - region.virtual_start().as_ptr() as u64 >= Size4KB::SIZE)]
  fn unmap_physical_region<T>(region: &PhysicalMapping<Self, T>) {
    trace!("Unmapping ACPI region at {:#x} of size {}", region.physical_start(), region.region_length());
    let ptr = region.virtual_start().as_ptr();
    let ptr_addr = ptr as u64;
    let start_page: Page<Size4KB> = Page::containing_address(VirtAddr::new(ptr_addr));
    let end_page: Page<Size4KB> = start_page + (region.mapped_length() as u64 / Size4KB::SIZE);

    let (p4_frame, _) = Cr3::read();
    let p4_address = p4_frame.start_address();

    for page in Page::range(start_page, end_page) {
      match unsafe { paging::remove_mapping(p4_address, page) } {
        Ok(_) => (),
        Err(e) => panic!("Error while unmapping ACPI page {page:?}: {e:?}"),
      }
    }
  }
}

pub fn init(bootinfo: &BootInformation) {
  if let Some(tag) = bootinfo.rsdp_v2_tag() {
    RDST_ADDR.store(tag.xsdt_address(), Ordering::Release);
    RDST_REV.store(tag.revision(), Ordering::Release);
  } else {
    let tag = bootinfo.rsdp_v1_tag().expect("No RSDP tag present");
    RDST_ADDR.store(tag.rsdt_address(), Ordering::Release);
    RDST_REV.store(tag.revision(), Ordering::Release);
  };

  debug!("RSDT/XSDT is at {:#x} with revision {}", RDST_ADDR.load(Ordering::Acquire), RDST_REV.load(Ordering::Acquire));
}
