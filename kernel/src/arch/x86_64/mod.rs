pub mod acpi;
pub mod cpu;
mod gdt;
pub mod interrupts;
pub mod memory;
pub mod multitasking;
pub mod serial;

pub use x86_64::{PhysAddr, VirtAddr};

use crate::multitasking::ThreadCreationError;

/// Spawns arch-specific background threads (e.g. on x86_64 spawns a thread which prefetches frames from the allocator)
///
/// # Errors
/// Returns `ThreadCreationError` if there was an issue spawning any background thread.
pub fn init_background_threads() -> Result<(), ThreadCreationError> {
  memory::init_background_threads()
}
