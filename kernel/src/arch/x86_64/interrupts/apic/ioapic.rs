use core::convert::TryFrom;

use bit_field::BitField;
use contracts::*;
use x86_64::{registers::control::Cr3, structures::paging::Size4KiB as Size4KB, PhysAddr, VirtAddr};

use super::{DeliveryMode, DeliveryStatus, Polarity, TriggerMode};
use crate::arch::x86_64::memory::{
  self,
  paging::{self, MappingFlags, ProtectionFlags},
};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum RegisterWidth {
  Bits32,
  Bits64,
}

struct _IoApicId {
  addr: VirtAddr,
  offset: u8,
  width: RegisterWidth,
}

impl _IoApicId {
  const fn new() -> Self {
    Self {
      addr: VirtAddr::zero(),
      offset: 0x0,
      width: RegisterWidth::Bits32,
    }
  }

  unsafe fn init(&mut self, addr: VirtAddr) {
    self.addr = addr;
  }

  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn read(&self) -> IoApicId {
    let reg_value = u32::try_from(read_io_apic_register(self.addr, self.offset, self.width))
      .expect("Value recieved from IOAPICID register too big");

    IoApicId::from(reg_value)
  }

  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn write(&mut self, id: IoApicId) {
    let id_bits = u64::from(u32::from(id));

    // We don't want to overwrite any reserved bits, so get the current value from the register and set only the bits we can
    // manipulate to 0
    let mut reg_value = read_io_apic_register(self.addr, self.offset, self.width);
    reg_value.set_bits(24..28, 0);

    let new_reg_value = reg_value | id_bits;

    write_io_apic_register(self.addr, self.offset, self.width, new_reg_value);
  }
}

#[derive(Clone, Copy, Debug)]
pub struct IoApicId {
  id: u8,
}

impl IoApicId {
  #[requires(id < 16, "I/O APIC IDs must be in the range 0-15")]
  pub const fn new(id: u8) -> Self {
    Self { id }
  }

  pub fn id(self) -> u8 {
    self.id
  }
}

impl From<u32> for IoApicId {
  fn from(value: u32) -> Self {
    Self { id: u8::try_from(value.get_bits(24..28)).unwrap() }
  }
}

impl From<IoApicId> for u32 {
  fn from(value: IoApicId) -> Self {
    let mut output = 0u32;
    output.set_bits(24..28, u32::from(value.id));

    output
  }
}

struct IoApicVer {
  addr: VirtAddr,
  offset: u8,
  width: RegisterWidth,
}

impl IoApicVer {
  const fn new() -> Self {
    Self {
      addr: VirtAddr::zero(),
      offset: 0x1,
      width: RegisterWidth::Bits32,
    }
  }

  unsafe fn init(&mut self, addr: VirtAddr) {
    self.addr = addr;
  }

  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn read(&self) -> IoApicVersion {
    let reg_value = u32::try_from(read_io_apic_register(self.addr, self.offset, self.width))
      .expect("Value received from IOAPICVER register too big");

    IoApicVersion::from(reg_value)
  }
}

#[derive(Clone, Copy, Debug)]
pub struct IoApicVersion {
  max_redirect_entry: u8,
  apic_version: u8,
}

impl IoApicVersion {
  pub fn max_redirect_entry(self) -> u8 {
    self.max_redirect_entry
  }

  pub fn apic_version(self) -> u8 {
    self.apic_version
  }
}

impl From<u32> for IoApicVersion {
  fn from(value: u32) -> Self {
    Self {
      max_redirect_entry: u8::try_from(value.get_bits(16..24)).unwrap(),
      apic_version: u8::try_from(value.get_bits(0..8)).unwrap(),
    }
  }
}

struct IoApicArb {
  addr: VirtAddr,
  offset: u8,
  width: RegisterWidth,
}

impl IoApicArb {
  const fn new() -> Self {
    Self {
      addr: VirtAddr::zero(),
      offset: 0x2,
      width: RegisterWidth::Bits32,
    }
  }

  unsafe fn init(&mut self, addr: VirtAddr) {
    self.addr = addr;
  }

  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn read(&self) -> IoApicArbitration {
    let reg_value = u32::try_from(read_io_apic_register(self.addr, self.offset, self.width))
      .expect("Value received from IOAPICARB register too big");

    IoApicArbitration::from(reg_value)
  }
}

#[derive(Clone, Copy, Debug)]
pub struct IoApicArbitration {
  arbitration_id: u8,
}

impl IoApicArbitration {
  pub fn arbitration_id(self) -> u8 {
    self.arbitration_id
  }
}

impl From<u32> for IoApicArbitration {
  fn from(value: u32) -> Self {
    Self {
      arbitration_id: u8::try_from(value.get_bits(24..28)).unwrap(),
    }
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum DestinationMode {
  Physical = 0,
  Logical = 1,
}

impl From<bool> for DestinationMode {
  fn from(value: bool) -> Self {
    if value {
      Self::Logical
    } else {
      Self::Physical
    }
  }
}

impl From<DestinationMode> for bool {
  fn from(value: DestinationMode) -> Self {
    match value {
      DestinationMode::Logical => true,
      DestinationMode::Physical => false,
    }
  }
}

struct IoApicRedTbl {
  addr: VirtAddr,
  offset: u8,
  width: RegisterWidth,
}

impl IoApicRedTbl {
  const fn new() -> Self {
    Self {
      addr: VirtAddr::zero(),
      offset: 0x10,
      width: RegisterWidth::Bits64,
    }
  }

  unsafe fn init(&mut self, addr: VirtAddr) {
    self.addr = addr;
  }

  #[requires(index < 24)]
  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn read(&self, index: u8) -> IoApicRedirectEntry {
    let adjusted_offset = self.offset + (2 * index);
    let reg_value = read_io_apic_register(self.addr, adjusted_offset, self.width);

    IoApicRedirectEntry::from(reg_value)
  }

  #[requires(index < 24)]
  #[requires(self.addr != VirtAddr::zero(), "IoApic must be initialized before use")]
  fn write(&mut self, index: u8, entry: IoApicRedirectEntry) {
    let adjusted_offset = self.offset + (2 * index);
    let value_to_write = u64::from(entry);

    // We don't want to write reserved register bits, so get the current register values first
    let mut reg_value = read_io_apic_register(self.addr, adjusted_offset, self.width);

    // Then set any bits we are going to manipulate to 0, to prepare for the OR operation
    reg_value.set_bits(0..8, 0);
    reg_value.set_bits(8..11, 0);
    reg_value.set_bit(11, false);
    reg_value.set_bit(13, false);
    reg_value.set_bit(15, false);
    reg_value.set_bit(16, false);
    reg_value.set_bits(56..64, 0);

    let new_reg_value = reg_value | value_to_write;

    write_io_apic_register(self.addr, adjusted_offset, self.width, new_reg_value);
  }
}

#[derive(Debug)]
pub struct IoApicRedirectEntry {
  interrupt_vector: u8,
  delivery_mode: DeliveryMode,
  destination_mode: DestinationMode,
  delivery_status: Option<DeliveryStatus>,
  interrupt_pin_polarity: Polarity,
  trigger_mode: TriggerMode,
  interrupt_masked: bool,
  destination: u8,
}

impl IoApicRedirectEntry {
  pub const fn new(
    interrupt_vector: u8,
    delivery_mode: DeliveryMode,
    destination_mode: DestinationMode,
    interrupt_pin_polarity: Polarity,
    trigger_mode: TriggerMode,
    interrupt_masked: bool,
    destination: u8,
  ) -> Self {
    Self {
      interrupt_vector,
      delivery_mode,
      destination_mode,
      delivery_status: None,
      interrupt_pin_polarity,
      trigger_mode,
      interrupt_masked,
      destination,
    }
  }

  pub fn interrupt_vector(&self) -> u8 {
    self.interrupt_vector
  }

  pub fn delivery_mode(&self) -> DeliveryMode {
    self.delivery_mode
  }

  pub fn destination_mode(&self) -> DestinationMode {
    self.destination_mode
  }

  pub fn delivery_status(&self) -> Option<DeliveryStatus> {
    self.delivery_status
  }

  pub fn interrupt_pin_polarity(&self) -> Polarity {
    self.interrupt_pin_polarity
  }

  pub fn trigger_mode(&self) -> TriggerMode {
    self.trigger_mode
  }

  pub fn interrupt_masked(&self) -> bool {
    self.interrupt_masked
  }

  pub fn destination(&self) -> u8 {
    self.destination
  }
}

impl From<u64> for IoApicRedirectEntry {
  fn from(value: u64) -> Self {
    let vector = u8::try_from(value.get_bits(0..9)).unwrap();
    let delivery_mode = DeliveryMode::from(u8::try_from(value.get_bits(8..11)).unwrap());
    let destination_mode = DestinationMode::from(value.get_bit(11));
    let delivery_status = DeliveryStatus::from(value.get_bit(12));
    let polarity = Polarity::from(value.get_bit(13));
    let trigger_mode = TriggerMode::from(value.get_bit(15));
    let masked = value.get_bit(16);
    let destination = u8::try_from(value.get_bits(56..64)).unwrap();

    Self {
      interrupt_vector: vector,
      delivery_mode,
      destination_mode,
      delivery_status: Some(delivery_status),
      interrupt_pin_polarity: polarity,
      trigger_mode,
      interrupt_masked: masked,
      destination,
    }
  }
}

impl From<IoApicRedirectEntry> for u64 {
  fn from(value: IoApicRedirectEntry) -> Self {
    let mut output = 0u64;

    output.set_bits(0..9, u64::from(value.interrupt_vector));
    output.set_bits(8..11, u64::from(value.delivery_mode as u8));
    output.set_bit(11, bool::from(value.destination_mode));
    output.set_bit(13, bool::from(value.interrupt_pin_polarity));
    output.set_bit(15, bool::from(value.trigger_mode));
    output.set_bit(16, value.interrupt_masked);
    output.set_bits(56..64, u64::from(value.destination));

    output
  }
}

pub struct IoApic {
  initialized: bool,
  io_apic_id: _IoApicId,
  io_apic_ver: IoApicVer,
  io_apic_arb: IoApicArb,
  io_apic_red_tbl: IoApicRedTbl,
}

impl IoApic {
  pub const fn new() -> Self {
    Self {
      initialized: false,
      io_apic_id: _IoApicId::new(),
      io_apic_ver: IoApicVer::new(),
      io_apic_arb: IoApicArb::new(),
      io_apic_red_tbl: IoApicRedTbl::new(),
    }
  }

  pub unsafe fn init(&mut self, addr: PhysAddr) {
    let (p4_frame, _) = Cr3::read();
    let flags = MappingFlags::empty();
    let protection = ProtectionFlags::Global | ProtectionFlags::Present | ProtectionFlags::Writable;
    let page_range = match paging::create_mapping::<Size4KB>(
      p4_frame.start_address(),
      Some(memory::KERNEL_VMA),
      Some(addr),
      8,
      flags,
      protection,
    ) {
      Ok(range) => range,
      Err(e) => panic!("Error while mapping I/O APIC into virtual memory: {e:?}"),
    };

    let frame_start_phys = memory::translate_addr(page_range.start.start_address())
      .expect("Unable to translate start of I/O APIC page range");

    let offset = addr.as_u64() - frame_start_phys.as_u64();
    let virt_addr = page_range.start.start_address() + offset;

    self.io_apic_id.init(virt_addr);
    self.io_apic_ver.init(virt_addr);
    self.io_apic_arb.init(virt_addr);
    self.io_apic_red_tbl.init(virt_addr);

    self.initialized = true;
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  pub fn io_apic_id(&self) -> IoApicId {
    self.io_apic_id.read()
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  pub fn io_apic_ver(&self) -> IoApicVersion {
    self.io_apic_ver.read()
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  pub fn io_apic_arb(&self) -> IoApicArbitration {
    self.io_apic_arb.read()
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  #[requires(index < 24, "IoApicRedTbl has only 24 entries")]
  pub fn io_apic_red_tbl(&self, index: u8) -> IoApicRedirectEntry {
    self.io_apic_red_tbl.read(index)
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  pub fn write_id(&mut self, id: IoApicId) {
    self.io_apic_id.write(id);
  }

  #[requires(self.initialized, "IoApic must be initialized before using")]
  #[requires(index < 24, "IoApicRedTbl has only 24 entries")]
  pub fn write_redirect_entry(&mut self, index: u8, entry: IoApicRedirectEntry) {
    self.io_apic_red_tbl.write(index, entry);
  }
}

fn read_io_apic_register(addr: VirtAddr, offset: u8, width: RegisterWidth) -> u64 {
  let io_reg_sel = addr.as_mut_ptr::<u32>();
  let io_reg_win = (addr + 0x10_u64).as_ptr::<u32>();

  unsafe {
    io_reg_sel.write_volatile(offset.into());
  }

  let low_bits = unsafe { u64::from(io_reg_win.read_volatile()) };

  let high_bits = match width {
    RegisterWidth::Bits32 => 0u64,
    RegisterWidth::Bits64 => unsafe {
      io_reg_sel.write_volatile((offset + 1).into());
      u64::from(io_reg_win.read_volatile()) << 32
    },
  };

  high_bits + low_bits
}

#[allow(clippy::cast_possible_truncation)]
fn write_io_apic_register(addr: VirtAddr, offset: u8, width: RegisterWidth, value: u64) {
  let io_reg_sel = addr.as_mut_ptr::<u32>();
  let io_reg_win = (addr + 0x10_u64).as_mut_ptr::<u32>();

  let low_bits = value as u32;
  unsafe {
    io_reg_sel.write_volatile(offset.into());
    io_reg_win.write_volatile(low_bits);
  }

  if width == RegisterWidth::Bits64 {
    let high_bits =
      u32::try_from((value & 0xffff_ffff_0000_0000) >> 32).expect("Error converting high bits of register to u32");

    unsafe {
      io_reg_sel.write_volatile((offset + 1).into());
      io_reg_win.write_volatile(high_bits);
    }
  }
}
