use core::{
  arch::asm,
  sync::atomic::{AtomicBool, Ordering},
  time::Duration,
};

use bit_field::BitField;
use bitflags::bitflags;
use contracts::*;
use log::debug;
use x86_64::{
  registers::{control::Cr3, model_specific::Msr},
  structures::{idt::InterruptStackFrame, paging::Size4KiB as Size4KB},
  PhysAddr, VirtAddr,
};

use super::{DeliveryMode, DeliveryStatus, Polarity, TriggerMode};
use crate::{
  arch::interrupts::{
    timing::{pit::PIT_FREQUENCY, Timer},
    InterruptMap,
  },
  memory::{
    self,
    paging::{self, MappingFlags, ProtectionFlags},
  },
};

const HOUR_NS: u128 = 3_600_000_000_000;

pub static INIT_PIT_FIRED: AtomicBool = AtomicBool::new(false);

extern "C" {
  fn xapic_start_compare_pit(count: u16, reg_ptr: *mut u32);
  fn x2apic_start_compare_pit(count: u16);
}

pub struct LocalApic {
  addr: VirtAddr,
  initialized: bool,
  mode: LocalApicMode,
  supports_tsc: bool,
  /// Timer frequency, in Hz
  timer_frequency: u128,
}

impl LocalApic {
  pub const fn new() -> Self {
    Self {
      addr: VirtAddr::zero(),
      initialized: false,
      mode: LocalApicMode::XApic,
      supports_tsc: false,
      timer_frequency: 0,
    }
  }

  pub unsafe fn init(&mut self, mode: LocalApicMode, phys_addr: Option<PhysAddr>) {
    debug!("Initializing local APIC at {phys_addr:?} with mode {mode:?}");
    if mode == LocalApicMode::XApic {
      let (p4_frame, _) = Cr3::read();
      let phys_addr = phys_addr.expect("phys_addr parameter is required when mode is xAPIC");
      let flags = MappingFlags::empty();
      let protection = ProtectionFlags::Global | ProtectionFlags::Present | ProtectionFlags::Writable;
      let page_range = match paging::create_mapping::<Size4KB>(
        p4_frame.start_address(),
        Some(memory::KERNEL_VMA),
        Some(phys_addr),
        8,
        flags,
        protection,
      ) {
        Ok(range) => range,
        Err(e) => panic!("Error while mapping I/O APIC into virtual memory: {e:?}"),
      };

      let frame_start_phys = memory::translate_addr(page_range.start.start_address())
        .expect("Unable to translate start of I/O APIC page range");

      let offset = phys_addr.as_u64() - frame_start_phys.as_u64();
      let virt_addr = page_range.start.start_address() + offset;

      self.addr = virt_addr;
    }
    self.mode = mode;

    // Determine if the local APIC supports TSC deadline mode for the timer
    let cpuid_value: u32;
    asm!(
      "push rbx",
      "cpuid",
      "pop rbx",
      in("eax") 0x1,
      out("ecx") cpuid_value,
      out("edx") _,
    );
    self.supports_tsc = cpuid_value.get_bit(24);
    debug!("Local APIC supports TSC deadline timer: {}", self.supports_tsc);

    self.initialized = true;

    // Set local APIC destination registers
    if mode == LocalApicMode::XApic {
      self.write_register(LOCAL_APIC_REGISTERS.destination_format, DestinationFormat::Flat.into());
      self.write_register(LOCAL_APIC_REGISTERS.logical_destination, LogicalDestination(1).into());
    }

    self.write_register(LOCAL_APIC_REGISTERS.spurious_interrupt_vector, 0x1ff);

    self.detect_timer_frequency();
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  unsafe fn read_register(&self, register: LocalApicRegister) -> u32 {
    match self.mode {
      LocalApicMode::XApic => self._read_mmio_register(register.mmio_offset),
      LocalApicMode::X2Apic => self._read_msr(register.msr_id),
    }
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  unsafe fn write_register(&mut self, register: LocalApicRegister, value: u32) {
    match self.mode {
      LocalApicMode::XApic => self._write_mmio_register(register.mmio_offset, value),
      LocalApicMode::X2Apic => self._write_msr(register.msr_id, value),
    }
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  unsafe fn _read_mmio_register(&self, offset: u16) -> u32 {
    let offset_addr = self.addr + u64::from(offset);
    let reg_ptr = offset_addr.as_ptr::<u32>();

    reg_ptr.read_volatile()
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  unsafe fn _write_mmio_register(&self, offset: u16, value: u32) {
    let offset_addr = self.addr + u64::from(offset);
    let reg_ptr = offset_addr.as_mut_ptr::<u32>();

    reg_ptr.write_volatile(value);
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  #[requires(self.mode == LocalApicMode::X2Apic, "APIC must be in x2APIC mode to manipulate with MSRs")]
  unsafe fn _read_msr(&self, id: u32) -> u32 {
    let msr = Msr::new(id);
    u32::try_from(msr.read()).expect("Local APIC register value was wider than 32 bits")
  }

  #[requires(self.initialized, "LocalApic must be initialized before use")]
  #[requires(self.mode == LocalApicMode::X2Apic, "APIC must be in x2APIC mode to manipulate with MSRs")]
  unsafe fn _write_msr(&mut self, id: u32, value: u32) {
    let mut msr = Msr::new(id);
    msr.write(u64::from(value));
  }

  pub fn id(&self) -> u32 {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_apic_id) };
    match self.mode {
      LocalApicMode::XApic => reg_value.get_bits(24..32),
      LocalApicMode::X2Apic => reg_value,
    }
  }

  pub fn version(&self) -> LocalApicVersion {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_apic_version) };
    LocalApicVersion::from(reg_value)
  }

  #[ensures(ret.vector_type == LocalApicVectorType::Cmci)]
  pub fn cmci_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_cmci) };
    LocalApicVector::from_u32(LocalApicVectorType::Cmci, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::Cmci && vector.delivery_mode.unwrap() != DeliveryMode::LowestPriority)]
  pub fn write_cmci_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_cmci) };
    old_reg_value.set_bits(0..11, 0);
    old_reg_value.set_bit(16, false);

    let new_reg_value = old_reg_value | value_to_write;

    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_cmci, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::Timer)]
  pub fn timer_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_timer) };
    LocalApicVector::from_u32(LocalApicVectorType::Timer, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::Timer)]
  pub fn write_timer_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_timer) };
    old_reg_value.set_bits(0..8, 0);
    old_reg_value.set_bit(16, false);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_timer, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::ThermalSensor)]
  pub fn thermal_monitor_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_thermal_sensor) };
    LocalApicVector::from_u32(LocalApicVectorType::ThermalSensor, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::ThermalSensor && vector.delivery_mode.unwrap() != DeliveryMode::LowestPriority)]
  pub fn write_thermal_monitor_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_thermal_sensor) };
    old_reg_value.set_bits(0..11, 0);
    old_reg_value.set_bit(16, false);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_thermal_sensor, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::PerformanceCounter)]
  pub fn performance_counter_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_performance_counter) };
    LocalApicVector::from_u32(LocalApicVectorType::PerformanceCounter, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::PerformanceCounter)]
  pub fn write_performance_counter_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_performance_counter) };
    old_reg_value.set_bits(0..11, 0);
    old_reg_value.set_bit(16, false);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_performance_counter, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::LocalInterrupt)]
  pub fn local_int0_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_lint0) };
    LocalApicVector::from_u32(LocalApicVectorType::LocalInterrupt, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::LocalInterrupt)]
  pub fn write_local_int0_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_lint0) };
    old_reg_value.set_bits(0..11, 0);
    old_reg_value.set_bit(13, false);
    old_reg_value.set_bits(15..17, 0);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_lint0, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::LocalInterrupt)]
  pub fn local_int1_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_lint1) };
    LocalApicVector::from_u32(LocalApicVectorType::LocalInterrupt, reg_value)
  }

  #[requires(vector.vector_type == LocalApicVectorType::LocalInterrupt)]
  pub fn write_local_int1_vector(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_lint1) };
    old_reg_value.set_bits(0..11, 0);
    old_reg_value.set_bit(13, false);
    old_reg_value.set_bits(15..17, 0);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_lint1, new_reg_value);
    }
  }

  #[ensures(ret.vector_type == LocalApicVectorType::Error)]
  #[requires(self.mode == LocalApicMode::XApic, "Error vector is only supported in xAPIC mode")]
  pub fn error_vector(&self) -> LocalApicVector {
    let reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_error) };
    LocalApicVector::from_u32(LocalApicVectorType::Error, reg_value)
  }

  #[requires(self.mode == LocalApicMode::XApic, "Error vector is only supported in xAPIC mode")]
  #[requires(vector.vector_type == LocalApicVectorType::Error)]
  pub fn write_error(&mut self, vector: LocalApicVector) {
    let value_to_write = u32::from(vector);

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_error) };
    old_reg_value.set_bits(0..8, 0);
    old_reg_value.set_bit(16, false);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_error, new_reg_value);
    }
  }

  pub fn error_status(&self) -> LocalApicError {
    let reg_value = unsafe { u8::try_from(self.read_register(LOCAL_APIC_REGISTERS.error_status)).unwrap() };
    LocalApicError::from_bits(reg_value).unwrap()
  }

  #[requires(self.mode == LocalApicMode::X2Apic, "Clearing the error statis register requires x2APIC mode")]
  pub fn clear_error_status(&mut self) {
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.error_status, 0);
    }
  }

  fn set_timer_divide(&mut self, divide: FrequencyDivide) {
    let divide = divide as u8;
    let mut value_to_write = 0u32;
    value_to_write.set_bits(0..2, u32::from(divide.get_bits(0..2)));
    value_to_write.set_bit(3, divide.get_bit(2));

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.timer_divide) };
    old_reg_value.set_bits(0..2, 0);
    old_reg_value.set_bit(3, false);

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.timer_divide, new_reg_value);
    }
  }

  #[requires(self.supports_tsc || mode != TimerMode::TscDeadline)]
  fn set_timer_mode(&mut self, mode: TimerMode) {
    let mut value_to_write = 0u32;
    if self.supports_tsc {
      value_to_write.set_bits(17..19, u32::from(mode as u8));
    } else {
      value_to_write.set_bit(17, (mode as u8).get_bit(0));
    }

    let mut old_reg_value = unsafe { self.read_register(LOCAL_APIC_REGISTERS.local_vector_timer) };
    if self.supports_tsc {
      old_reg_value.set_bits(17..19, 0);
    } else {
      old_reg_value.set_bit(17, false);
    }

    let new_reg_value = old_reg_value | value_to_write;
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_timer, new_reg_value);
    }
  }

  fn detect_timer_frequency(&mut self) {
    x86_64::instructions::interrupts::without_interrupts(|| {
      crate::arch::x86_64::interrupts::IDT.write()[super::InterruptMap::Pit as usize]
        .set_handler_fn(apic_calibration_pit_handler);
    });
    self.set_timer_divide(FrequencyDivide::DivideBy1);
    self.set_timer_mode(TimerMode::OneShot);
    let timer_vector =
      LocalApicVector::new(LocalApicVectorType::Timer, super::InterruptMap::ApicTimer as u8, None, None, None, false);
    let timer_vector_stop =
      LocalApicVector::new(LocalApicVectorType::Timer, super::InterruptMap::ApicTimer as u8, None, None, None, true);
    self.write_timer_vector(timer_vector);

    let time_ms = 10u8; // How long we want to let the PIT run to measure the APIC timer frequency
    let num_ticks = u16::try_from(PIT_FREQUENCY * u32::from(time_ms) / 1000).unwrap();
    let timer_stop_value = u32::from(timer_vector_stop);

    if self.mode == LocalApicMode::XApic {
      let offset_addr = self.addr + u64::from(LOCAL_APIC_REGISTERS.timer_initial_count.mmio_offset);
      unsafe {
        xapic_start_compare_pit(num_ticks, offset_addr.as_mut_ptr::<u32>());
      }
    } else {
      unsafe {
        x2apic_start_compare_pit(num_ticks);
      }
    }

    while !INIT_PIT_FIRED.load(Ordering::Acquire) {
      core::hint::spin_loop();
    }

    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.local_vector_timer, timer_stop_value);
    }

    self.send_eoi(); // EOI is delayed until this point so that we can stop the timer sooner (and thus get a more accurate count)

    let reg_val = unsafe { self.read_register(LOCAL_APIC_REGISTERS.timer_current_count) };

    let elapsed_ticks = u128::from(u32::MAX - reg_val);
    debug!("{elapsed_ticks} ticks of the APIC timer elapsed in 10ms");
    let ticks_per_second = elapsed_ticks * 100;
    debug!("Local APIC appears to oscillate at {ticks_per_second} Hz");
    self.timer_frequency = ticks_per_second;
  }

  pub(super) fn send_eoi(&mut self) {
    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.eoi, 0);
    }
  }

  #[requires(duration.as_nanos() <= HOUR_NS, "Timer does not support durations longer than an hour")]
  fn set_timer_inner(&mut self, mode: TimerMode, duration: Duration) {
    self.set_timer_mode(mode);

    let ns = duration.as_nanos();
    let ticks = (self.timer_frequency * ns) / Duration::from_secs(1).as_nanos();
    let (ticks, frequency_divide) = if ticks > u128::from(u32::MAX) {
      let mut min_divide = ticks / u128::from(u32::MAX);
      if ticks % u128::from(u32::MAX) != 0 {
        // Since there was a remainder, we need the divider to be one higher
        min_divide += 1;
      }

      let (frequency_divide, divisor) = if min_divide <= 2 {
        (FrequencyDivide::DivideBy2, 2)
      } else if min_divide <= 4 {
        (FrequencyDivide::DivideBy4, 4)
      } else if min_divide <= 8 {
        (FrequencyDivide::DivideBy8, 8)
      } else if min_divide <= 16 {
        (FrequencyDivide::DivideBy16, 16)
      } else if min_divide <= 32 {
        (FrequencyDivide::DivideBy32, 32)
      } else if min_divide <= 64 {
        (FrequencyDivide::DivideBy64, 64)
      } else if min_divide <= 128 {
        (FrequencyDivide::DivideBy128, 128)
      } else {
        panic!("APIC timer appears to have an unreasonably large frequency: {}", self.timer_frequency);
      };

      let mut adjusted_ticks = ticks / divisor;
      if ticks % divisor != 0 {
        adjusted_ticks += 1;
      }

      (adjusted_ticks, frequency_divide)
    } else {
      (ticks, FrequencyDivide::DivideBy1)
    };

    self.set_timer_divide(frequency_divide);
    let vector =
      LocalApicVector::new(LocalApicVectorType::Timer, InterruptMap::ApicTimer as u8, None, None, None, false);
    self.write_timer_vector(vector);

    unsafe {
      self.write_register(LOCAL_APIC_REGISTERS.timer_initial_count, u32::try_from(ticks).unwrap());
    }
  }
}

impl Timer for LocalApic {
  fn start_periodic(&mut self, duration: Duration) {
    self.set_timer_inner(TimerMode::Periodic, duration);
  }

  fn start_oneshot(&mut self, duration: Duration) {
    self.set_timer_inner(TimerMode::OneShot, duration);
  }

  fn stop(&mut self) {
    let mut vector = unsafe {
      LocalApicVector::from_u32(LocalApicVectorType::Timer, self.read_register(LOCAL_APIC_REGISTERS.local_vector_timer))
    };
    vector.masked = true;
    self.write_timer_vector(vector);
  }
}

#[derive(Clone, Copy, Debug)]
pub struct LogicalDestination(u8);

impl From<LogicalDestination> for u32 {
  fn from(value: LogicalDestination) -> Self {
    let mut output = 0;
    output.set_bits(24..32, u32::from(value.0));
    output
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum DestinationFormat {
  Flat = 0b1111,
  Cluster = 0,
}

impl From<u8> for DestinationFormat {
  fn from(value: u8) -> Self {
    match value {
      0 => DestinationFormat::Cluster,
      0b1111 => DestinationFormat::Flat,
      _ => panic!("Unsupported destination format value: {value}"),
    }
  }
}

impl From<DestinationFormat> for u32 {
  fn from(value: DestinationFormat) -> Self {
    let mut output = 0xfff_ffff;
    output.set_bits(28..32, u32::from(value as u8));
    output
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum FrequencyDivide {
  DivideBy2 = 0b000,
  DivideBy4 = 0b001,
  DivideBy8 = 0b010,
  DivideBy16 = 0b011,
  DivideBy32 = 0b100,
  DivideBy64 = 0b101,
  DivideBy128 = 0b110,
  DivideBy1 = 0b111,
}

impl From<u8> for FrequencyDivide {
  fn from(value: u8) -> Self {
    match value {
      0b000 => Self::DivideBy2,
      0b001 => Self::DivideBy4,
      0b010 => Self::DivideBy8,
      0b011 => Self::DivideBy16,
      0b100 => Self::DivideBy32,
      0b101 => Self::DivideBy64,
      0b110 => Self::DivideBy128,
      0b111 => Self::DivideBy1,
      _ => panic!("Invalid value for FrequencyDivide: {value}"),
    }
  }
}

bitflags! {
  pub struct LocalApicError: u8 {
    const SEND_CHECKSUM_ERROR = 0b1;
    const RECEIVE_CHECKSUM_ERROR = 0b10;
    const SEND_ACCEPT_ERROR = 0b100;
    const RECEIVE_ACCEPT_ERROR = 0b1000;
    const REDIRECTABLE_IPI = 0b10000;
    const SEND_ILLEGAL_VECTOR = 0b10_0000;
    const RECEIVED_ILLEGAL_VECTOR = 0b100_0000;
    const ILLEGAL_REGISTER_ADDRESS = 0b1000_0000;
  }
}

#[derive(Clone, Copy, Debug)]
pub struct LocalApicVector {
  vector_type: LocalApicVectorType,
  pub vector: u8,
  delivery_mode: Option<DeliveryMode>,
  delivery_status: Option<DeliveryStatus>,
  polarity: Option<Polarity>,
  trigger: Option<TriggerMode>,
  pub masked: bool,
}

impl LocalApicVector {
  #[requires(vector_type != LocalApicVectorType::LocalInterrupt || (polarity.is_some() && trigger.is_some()), "Polarity and trigger must be set for local interrupt vectors")]
  #[requires(vector_type == LocalApicVectorType::LocalInterrupt || (polarity.is_none() && trigger.is_none()), "Polarity and trigger are only valid for local interrupt vectors")]
  #[requires((vector_type == LocalApicVectorType::Timer || vector_type == LocalApicVectorType::Error) || delivery_mode.is_some(), "Delivery mode must be set for non-timer, non-error vectors")]
  #[requires((vector_type != LocalApicVectorType::Timer && vector_type != LocalApicVectorType::Error) || delivery_mode.is_none(), "Delivery is only valid for non-timer, non-error vectors")]
  pub fn new(
    vector_type: LocalApicVectorType,
    vector: u8,
    delivery_mode: Option<DeliveryMode>,
    polarity: Option<Polarity>,
    trigger: Option<TriggerMode>,
    masked: bool,
  ) -> Self {
    Self {
      vector_type,
      vector,
      delivery_mode,
      delivery_status: None,
      polarity,
      trigger,
      masked,
    }
  }

  pub fn vector_type(self) -> LocalApicVectorType {
    self.vector_type
  }

  pub fn delivery_mode(self) -> Option<DeliveryMode> {
    self.delivery_mode
  }

  pub fn delivery_status(self) -> Option<DeliveryStatus> {
    self.delivery_status
  }

  pub fn polarity(self) -> Option<Polarity> {
    self.polarity
  }

  pub fn trigger(self) -> Option<TriggerMode> {
    self.trigger
  }

  pub fn from_u32(vector_type: LocalApicVectorType, value: u32) -> Self {
    let vector = u8::try_from(value.get_bits(0..8)).unwrap();
    let delivery_status = Some(DeliveryStatus::from(value.get_bit(12)));
    let masked = value.get_bit(16);

    let delivery_mode = if vector_type != LocalApicVectorType::Timer && vector_type != LocalApicVectorType::Error {
      let bits = u8::try_from(value.get_bits(8..11)).unwrap();
      let delivery_mode = DeliveryMode::from(bits);
      Some(delivery_mode)
    } else {
      None
    };

    let (polarity, trigger) = if vector_type == LocalApicVectorType::LocalInterrupt {
      let polarity = Polarity::from(value.get_bit(13));
      let trigger = TriggerMode::from(value.get_bit(15));
      (Some(polarity), Some(trigger))
    } else {
      (None, None)
    };

    Self {
      vector_type,
      vector,
      delivery_mode,
      delivery_status,
      polarity,
      trigger,
      masked,
    }
  }
}

impl From<LocalApicVector> for u32 {
  fn from(value: LocalApicVector) -> Self {
    let mut output = 0u32;

    output.set_bits(0..8, u32::from(value.vector));
    output.set_bit(16, value.masked);

    if value.vector_type != LocalApicVectorType::Timer && value.vector_type != LocalApicVectorType::Error {
      let bits = u32::from(value.delivery_mode.unwrap() as u8);
      output.set_bits(8..11, bits);
    }

    if value.vector_type == LocalApicVectorType::LocalInterrupt {
      output.set_bit(13, bool::from(value.polarity.unwrap()));
      output.set_bit(15, bool::from(value.trigger.unwrap()));
    }

    output
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum TimerMode {
  OneShot = 0b00,
  Periodic = 0b01,
  TscDeadline = 0b10,
}

impl From<u8> for TimerMode {
  fn from(value: u8) -> Self {
    match value {
      0b00 => Self::OneShot,
      0b01 => Self::Periodic,
      0b10 => Self::TscDeadline,
      _ => panic!("Invalid timer mode: {value}"),
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LocalApicVectorType {
  Cmci,
  Error,
  LocalInterrupt,
  PerformanceCounter,
  ThermalSensor,
  Timer,
}

#[derive(Clone, Copy, Debug)]
pub struct LocalApicVersion {
  pub version: u8,
  pub apic_type: LocalApicType,
  pub max_lvt_entry: u8,
  pub supports_eoi_broadcast_suppression: bool,
}

impl From<u32> for LocalApicVersion {
  fn from(value: u32) -> Self {
    let version = u8::try_from(value.get_bits(0..8)).unwrap();
    let apic_type = if version < 0x10 {
      LocalApicType::Discrete
    } else if (0x10..0x16).contains(&version) {
      LocalApicType::Integrated
    } else {
      panic!("Unsupported local APIC version: {version}");
    };
    let max_lvt_entry = u8::try_from(value.get_bits(16..24)).unwrap();
    let supports_eoi_broadcast_suppression = value.get_bit(24);

    Self {
      version,
      apic_type,
      max_lvt_entry,
      supports_eoi_broadcast_suppression,
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LocalApicMode {
  XApic,
  X2Apic,
}

#[derive(Clone, Copy, Debug)]
pub enum LocalApicType {
  Discrete,
  Integrated,
}

#[derive(Clone, Copy, Debug)]
struct LocalApicRegister {
  mmio_offset: u16,
  msr_id: u32,
}

struct LocalApicRegisters {
  local_apic_id: LocalApicRegister,
  local_apic_version: LocalApicRegister,
  task_priority: LocalApicRegister,
  arbitration_priority: LocalApicRegister,
  processor_priority: LocalApicRegister,
  eoi: LocalApicRegister,
  remote_read: LocalApicRegister,
  logical_destination: LocalApicRegister,
  destination_format: LocalApicRegister,
  spurious_interrupt_vector: LocalApicRegister,
  isr0: LocalApicRegister,
  isr32: LocalApicRegister,
  isr64: LocalApicRegister,
  isr96: LocalApicRegister,
  isr128: LocalApicRegister,
  isr160: LocalApicRegister,
  isr192: LocalApicRegister,
  isr224: LocalApicRegister,
  tmr0: LocalApicRegister,
  tmr32: LocalApicRegister,
  tmr64: LocalApicRegister,
  tmr96: LocalApicRegister,
  tmr128: LocalApicRegister,
  tmr160: LocalApicRegister,
  tmr192: LocalApicRegister,
  tmr224: LocalApicRegister,
  irr0: LocalApicRegister,
  irr32: LocalApicRegister,
  irr64: LocalApicRegister,
  irr96: LocalApicRegister,
  irr128: LocalApicRegister,
  irr160: LocalApicRegister,
  irr192: LocalApicRegister,
  irr224: LocalApicRegister,
  error_status: LocalApicRegister,
  local_vector_cmci: LocalApicRegister,
  icr0: LocalApicRegister,
  icr32: LocalApicRegister,
  local_vector_timer: LocalApicRegister,
  local_vector_thermal_sensor: LocalApicRegister,
  local_vector_performance_counter: LocalApicRegister,
  local_vector_lint0: LocalApicRegister,
  local_vector_lint1: LocalApicRegister,
  local_vector_error: LocalApicRegister,
  timer_initial_count: LocalApicRegister,
  timer_current_count: LocalApicRegister,
  timer_divide: LocalApicRegister,
  self_ipi: LocalApicRegister,
}

static LOCAL_APIC_REGISTERS: LocalApicRegisters = LocalApicRegisters {
  local_apic_id: LocalApicRegister { mmio_offset: 0x20, msr_id: 0x802 },
  local_apic_version: LocalApicRegister { mmio_offset: 0x30, msr_id: 0x803 },
  task_priority: LocalApicRegister { mmio_offset: 0x80, msr_id: 0x808 },
  arbitration_priority: LocalApicRegister { mmio_offset: 0x90, msr_id: 0 },
  processor_priority: LocalApicRegister { mmio_offset: 0xa0, msr_id: 0x80a },
  eoi: LocalApicRegister { mmio_offset: 0xb0, msr_id: 0x80b },
  remote_read: LocalApicRegister { mmio_offset: 0xc0, msr_id: 0 },
  logical_destination: LocalApicRegister { mmio_offset: 0xd0, msr_id: 0x80d },
  destination_format: LocalApicRegister { mmio_offset: 0xe0, msr_id: 0 },
  spurious_interrupt_vector: LocalApicRegister { mmio_offset: 0xf0, msr_id: 0x80f },
  isr0: LocalApicRegister { mmio_offset: 0x100, msr_id: 0x810 },
  isr32: LocalApicRegister { mmio_offset: 0x110, msr_id: 0x811 },
  isr64: LocalApicRegister { mmio_offset: 0x120, msr_id: 0x812 },
  isr96: LocalApicRegister { mmio_offset: 0x130, msr_id: 0x813 },
  isr128: LocalApicRegister { mmio_offset: 0x140, msr_id: 0x814 },
  isr160: LocalApicRegister { mmio_offset: 0x150, msr_id: 0x815 },
  isr192: LocalApicRegister { mmio_offset: 0x160, msr_id: 0x816 },
  isr224: LocalApicRegister { mmio_offset: 0x170, msr_id: 0x817 },
  tmr0: LocalApicRegister { mmio_offset: 0x180, msr_id: 0x818 },
  tmr32: LocalApicRegister { mmio_offset: 0x190, msr_id: 0x819 },
  tmr64: LocalApicRegister { mmio_offset: 0x1a0, msr_id: 0x81a },
  tmr96: LocalApicRegister { mmio_offset: 0x1b0, msr_id: 0x81b },
  tmr128: LocalApicRegister { mmio_offset: 0x1c0, msr_id: 0x81c },
  tmr160: LocalApicRegister { mmio_offset: 0x1d0, msr_id: 0x81d },
  tmr192: LocalApicRegister { mmio_offset: 0x1e0, msr_id: 0x81e },
  tmr224: LocalApicRegister { mmio_offset: 0x1f0, msr_id: 0x81f },
  irr0: LocalApicRegister { mmio_offset: 0x200, msr_id: 0x820 },
  irr32: LocalApicRegister { mmio_offset: 0x210, msr_id: 0x821 },
  irr64: LocalApicRegister { mmio_offset: 0x220, msr_id: 0x822 },
  irr96: LocalApicRegister { mmio_offset: 0x230, msr_id: 0x823 },
  irr128: LocalApicRegister { mmio_offset: 0x240, msr_id: 0x824 },
  irr160: LocalApicRegister { mmio_offset: 0x250, msr_id: 0x825 },
  irr192: LocalApicRegister { mmio_offset: 0x260, msr_id: 0x826 },
  irr224: LocalApicRegister { mmio_offset: 0x270, msr_id: 0x827 },
  error_status: LocalApicRegister { mmio_offset: 0x280, msr_id: 0x828 },
  local_vector_cmci: LocalApicRegister { mmio_offset: 0x2f0, msr_id: 0x82f },
  icr0: LocalApicRegister { mmio_offset: 0x300, msr_id: 0x830 },
  icr32: LocalApicRegister { mmio_offset: 0x310, msr_id: 0 },
  local_vector_timer: LocalApicRegister { mmio_offset: 0x320, msr_id: 0x832 },
  local_vector_thermal_sensor: LocalApicRegister { mmio_offset: 0x330, msr_id: 0x833 },
  local_vector_performance_counter: LocalApicRegister { mmio_offset: 0x340, msr_id: 0x834 },
  local_vector_lint0: LocalApicRegister { mmio_offset: 0x350, msr_id: 0x835 },
  local_vector_lint1: LocalApicRegister { mmio_offset: 0x360, msr_id: 0x836 },
  local_vector_error: LocalApicRegister { mmio_offset: 0x370, msr_id: 0 },
  timer_initial_count: LocalApicRegister { mmio_offset: 0x380, msr_id: 0x838 },
  timer_current_count: LocalApicRegister { mmio_offset: 0x390, msr_id: 0x839 },
  timer_divide: LocalApicRegister { mmio_offset: 0x3e0, msr_id: 0x83e },
  self_ipi: LocalApicRegister { mmio_offset: 0, msr_id: 0x83f },
};

extern "x86-interrupt" fn apic_calibration_pit_handler(_: InterruptStackFrame) {
  INIT_PIT_FIRED.store(true, Ordering::Release);
}
