use core::{arch::asm, sync::atomic::Ordering};

use acpi::{
  madt::Madt,
  platform::{interrupt::Apic, Processor, ProcessorState},
  sdt::Signature,
  AcpiTables, InterruptModel,
};
use bit_field::BitField;
use ioapic::{DestinationMode, IoApic, IoApicRedirectEntry};
use local_apic::{LocalApic, LocalApicMode};
use log::{debug, warn};
use spin::Mutex;
use x86_64::{instructions::interrupts::without_interrupts, registers::model_specific::Msr, PhysAddr};

use super::{
  super::acpi::{Handler, RDST_ADDR, RDST_REV},
  InterruptMap,
};
use crate::arch::cpu::MsrId;

pub mod ioapic;
pub mod local_apic;

pub static IO_APIC: Mutex<IoApic> = Mutex::new(IoApic::new());
pub static LOCAL_APIC: Mutex<LocalApic> = Mutex::new(LocalApic::new());

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum DeliveryMode {
  Fixed = 0b000,
  LowestPriority = 0b001,
  Smi = 0b010,
  Nmi = 0b100,
  Init = 0b101,
  ExInt = 0b111,
}

impl From<u8> for DeliveryMode {
  fn from(value: u8) -> Self {
    match value {
      0b000 => Self::Fixed,
      0b001 => Self::LowestPriority,
      0b010 => Self::Smi,
      0b100 => Self::Nmi,
      0b101 => Self::Init,
      0b111 => Self::ExInt,
      _ => panic!("Unsupported bit configuration found when parsing DeliveryMode: {value:b}"),
    }
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum DeliveryStatus {
  Idle = 0,
  SendPending = 1,
}

impl From<bool> for DeliveryStatus {
  fn from(value: bool) -> Self {
    if value {
      Self::SendPending
    } else {
      Self::Idle
    }
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Polarity {
  ActiveHigh = 0,
  ActiveLow = 1,
}

impl From<bool> for Polarity {
  fn from(value: bool) -> Self {
    if value {
      Polarity::ActiveLow
    } else {
      Polarity::ActiveHigh
    }
  }
}

impl From<Polarity> for bool {
  fn from(value: Polarity) -> Self {
    match value {
      Polarity::ActiveLow => true,
      Polarity::ActiveHigh => false,
    }
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum TriggerMode {
  Edge = 0,
  Level = 1,
}

impl From<bool> for TriggerMode {
  fn from(value: bool) -> Self {
    if value {
      TriggerMode::Level
    } else {
      TriggerMode::Edge
    }
  }
}

impl From<TriggerMode> for bool {
  fn from(value: TriggerMode) -> Self {
    match value {
      TriggerMode::Level => true,
      TriggerMode::Edge => false,
    }
  }
}

pub(super) fn init() {
  let tables = unsafe {
    AcpiTables::from_rsdt(Handler, RDST_REV.load(Ordering::Acquire), RDST_ADDR.load(Ordering::Acquire))
      .expect("Unable to parse ACPI tables")
  };

  let madt = match unsafe { tables.get_sdt::<Madt>(Signature::MADT) } {
    Ok(maybe_table) => match maybe_table {
      Some(table) => table,
      None => panic!("No MADT exists in ACPI"),
    },
    Err(e) => panic!("Error while trying to retrieve MADT: {e:?}"),
  };

  let (interrupt_model, processor_info) =
    madt.parse_interrupt_model().expect("Unable to parse interrupt model from MADT");
  match interrupt_model {
    InterruptModel::Apic(model) => {
      debug!("APIC interrupt model in use");
      debug!("Local APIC address: {:#x}", model.local_apic_address);
      debug!("I/O APIC address: {:#x}", model.io_apics[0].address);

      init_ioapic(&model);
      init_local_apic(&model);
    }
    InterruptModel::Unknown => debug!("Legacy interrupt model in use"),
    _ => panic!("Unsupported value for ACPI interrupt model"),
  }

  if let Some(info) = processor_info {
    fn descr_proc(proc: Processor) {
      let state = match proc.state {
        ProcessorState::Disabled => "disabled",
        ProcessorState::Running => "running",
        ProcessorState::WaitingForSipi => "waiting for SIPI",
      };
      debug!(
        "Processor - is AP {}, local APIC ID {}, UID {}, state {}",
        proc.is_ap, proc.local_apic_id, proc.processor_uid, state
      );
    }

    descr_proc(info.boot_processor);
    for proc in info.application_processors {
      descr_proc(proc);
    }
  }
}

fn init_ioapic(model: &Apic) {
  assert!(model.io_apics.len() == 1, "Found {} I/O APICs, only one is supported", model.io_apics.len());

  let addr = u64::from(model.io_apics[0].address);
  let phys_addr = PhysAddr::new(addr);
  unsafe {
    IO_APIC.lock().init(phys_addr);
  }

  debug!("I/O APIC reports ID {:?}", IO_APIC.lock().io_apic_id());
  debug!("I/O APIC reports version {:?}", IO_APIC.lock().io_apic_ver());

  for iso in &model.interrupt_source_overrides {
    // Map interrupt source override entries to the I/O APIC
    let index = u8::try_from(iso.global_system_interrupt)
      .unwrap_or_else(|_| panic!("Received invalid GSI value {} from ACPI", iso.global_system_interrupt));
    let vector = if let Some(remap) = super::ISA_INTERRUPT_MAP.into_iter().find(|x| x.original == iso.isa_source) {
      remap.new
    } else {
      // If the interrupt doesn't exist in the map, set it to the spurious interrupt vector
      warn!("No mapping found in ISA interrupt map for ISA interrupt {}", iso.isa_source);
      InterruptMap::Spurious as u8
    };
    let delivery_mode = DeliveryMode::Fixed;
    let destination_mode = DestinationMode::Logical;

    // per https://uefi.org/specs/ACPI/6.4/05_ACPI_Software_Programming_Model/ACPI_Software_Programming_Model.html#interrupt-source-override-structure
    // the only interrupts supported by the ISO structure are ISA interrupts. And it also says that ISA interrupts are active low, edge triggered.
    // So the right thing to do in case of SameAsBus trigger mode should be edge, and SameAsBus polarity should be active low.
    let trigger_mode = match iso.trigger_mode {
      acpi::platform::interrupt::TriggerMode::Edge | acpi::platform::interrupt::TriggerMode::SameAsBus => {
        TriggerMode::Edge
      }
      acpi::platform::interrupt::TriggerMode::Level => TriggerMode::Level,
    };
    let polarity = match iso.polarity {
      acpi::platform::interrupt::Polarity::ActiveHigh => Polarity::ActiveHigh,
      acpi::platform::interrupt::Polarity::ActiveLow | acpi::platform::interrupt::Polarity::SameAsBus => {
        Polarity::ActiveLow
      }
    };

    let masked = false;
    let destination = 0xff; // Send ISA interrupts to every CPU for the moment
    let redirect_entry =
      IoApicRedirectEntry::new(vector, delivery_mode, destination_mode, polarity, trigger_mode, masked, destination);

    debug!("Setting {redirect_entry:?} to I/O APIC index {index}");
    IO_APIC.lock().write_redirect_entry(index, redirect_entry);
  }
}

fn init_local_apic(model: &Apic) {
  // First, detect if we can use x2APIC mode
  let use_x2apic = {
    unsafe {
      let mut cpuid_value: u32;

      asm!(
        "push rbx",
        "cpuid",
        "pop rbx",
        in("eax") 1,
        out("ecx") cpuid_value,
        out("edx") _,
      );

      cpuid_value.get_bit(21)
    }
  };

  let (mode, addr) = if use_x2apic {
    // We need to also enable x2APIC mode in the IA32_APIC_BASE MSR to enable it
    let mut ia32_apic_base = Msr::new(MsrId::Ia32ApicBase as u32);
    let mut msr_value = unsafe { ia32_apic_base.read() };
    msr_value.set_bit(10, true);
    unsafe {
      ia32_apic_base.write(msr_value);
    }

    (LocalApicMode::X2Apic, None)
  } else {
    (LocalApicMode::XApic, Some(PhysAddr::new(model.local_apic_address)))
  };

  unsafe {
    LOCAL_APIC.lock().init(mode, addr);
  }

  debug!("Local APIC says its ID is {}", LOCAL_APIC.lock().id());
  debug!("Local APIC reports version info {:?}", LOCAL_APIC.lock().version());
  debug!("Local APIC reports timer vector {:?}", LOCAL_APIC.lock().timer_vector());
}

pub(in crate::arch::x86_64) fn send_local_apic_eoi() {
  without_interrupts(|| LOCAL_APIC.lock().send_eoi());
}
