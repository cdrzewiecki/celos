use core::time::Duration;

use bit_field::BitField;
use contracts::*;
use log::debug;
use x86_64::instructions::port::Port;

use crate::arch::interrupts::apic::local_apic::TimerMode;

/// Frequency of the PIT, in Hz
pub const PIT_FREQUENCY: u32 = 1_193_182;

pub struct Pit {
  data0: Port<u8>,
  data1: Port<u8>,
  data2: Port<u8>,
  mode: Port<u8>,
}

impl Pit {
  pub const fn new() -> Self {
    Self {
      data0: Port::new(0x40),
      data1: Port::new(0x41),
      data2: Port::new(0x42),
      mode: Port::new(0x43),
    }
  }

  pub fn disable_timer0(&mut self) {
    let mode = PitMode {
      channel: Channel::Channel0,
      access_mode: AccessMode::Lobyte,
      operating_mode: OperatingMode::HardwareStrobe,
    };

    unsafe {
      let mut wait_port: Port<u8> = Port::new(0x80);
      self.mode.write(u8::from(mode));
      wait_port.read();
      self.data0.write(0);
    }
  }

  #[requires(duration <= Duration::from_millis(54), "PIT must be set to an interval of 54 ms or less")]
  #[requires(mode != TimerMode::TscDeadline, "TSC deadline timer is not supported for PIT")]
  pub fn set_timer0(&mut self, mode: TimerMode, duration: Duration) {
    let mode = PitMode {
      channel: Channel::Channel0,
      access_mode: AccessMode::LobyteHibyte,
      operating_mode: if mode == TimerMode::OneShot {
        OperatingMode::TerminalCount
      } else {
        OperatingMode::SquareWaveGenerator
      },
    };

    // Odd tick values act weird in this mode, so mask off the last bit to get an even value
    let ticks = u16::try_from(u128::from(PIT_FREQUENCY) * duration.as_nanos() / Duration::from_secs(1).as_nanos())
      .unwrap()
      & 0xfffe;
    debug!("ticks {ticks}");
    let ticks_lowbyte = u8::try_from(ticks & 0b1111_1111).unwrap();
    let ticks_highbyte = u8::try_from(ticks >> 8).unwrap();

    unsafe {
      let mut wait_port: Port<u8> = Port::new(0x80);
      self.mode.write(u8::from(mode));
      wait_port.read();
      self.data0.write(ticks_lowbyte);
      wait_port.read();
      self.data0.write(ticks_highbyte);
    }
  }
}

#[derive(Clone, Copy, Debug)]
pub struct PitMode {
  channel: Channel,
  access_mode: AccessMode,
  operating_mode: OperatingMode,
}

impl From<PitMode> for u8 {
  fn from(value: PitMode) -> Self {
    let mut output = 0u8;
    output.set_bits(6..8, value.channel as u8);
    output.set_bits(4..6, value.access_mode as u8);
    output.set_bits(1..4, value.operating_mode as u8);
    output
  }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Channel {
  Channel0 = 0b00,
  Channel1 = 0b01,
  Channel2 = 0b10,
  ReadBack = 0b11,
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum AccessMode {
  LatchCount = 0b00,
  Lobyte = 0b01,
  Hibyte = 0b10,
  LobyteHibyte = 0b11,
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum OperatingMode {
  TerminalCount = 0b000,
  OneShot = 0b001,
  RateGenerator = 0b010,
  SquareWaveGenerator = 0b011,
  SoftwareStrobe = 0b100,
  HardwareStrobe = 0b101,
  RateGenerator2 = 0b110,
  SquareWaveGenerator2 = 0b111,
}
