use core::{
  arch::asm,
  sync::atomic::{AtomicBool, AtomicU64, Ordering},
  time::Duration,
};

use bit_field::BitField;
use log::{debug, warn};
use spin::Mutex;
use x86_64::structures::idt::InterruptStackFrame;

pub mod pit;
use pit::Pit;

use super::{InterruptMap, IDT};

extern "C" {
  fn tsc_start_compare_pit(count: u16);
}

/// The detected frequency of the TSC, in Hz
pub static TSC_FREQUENCY: AtomicU64 = AtomicU64::new(0);
pub static PIT: Mutex<Pit> = Mutex::new(Pit::new());
pub static TSC_INIT_TIMER_FIRED: AtomicBool = AtomicBool::new(false);

pub trait Timer {
  fn start_periodic(&mut self, duration: Duration);
  fn start_oneshot(&mut self, duration: Duration);
  fn stop(&mut self);
}

pub fn init_tsc() {
  let cpuid_value: u32;
  unsafe {
    asm!(
      "mov eax, 0x80000007",
      "push rbx",
      "cpuid",
      "pop rbx",
      out("eax") _,
      out("ecx") _,
      out("edx") cpuid_value,
    );
  }
  if !cpuid_value.get_bit(8) {
    warn!("TSC does not support invariant TSC, using it for timing is not stable");
  }
  let time_ms = 10;
  let ticks = u16::try_from(pit::PIT_FREQUENCY * time_ms / 1000).unwrap();
  x86_64::instructions::interrupts::without_interrupts(|| {
    IDT.write()[InterruptMap::Pit as usize].set_handler_fn(init_tsc_callback);
  });

  let initial_tsc_value = read_tsc();

  unsafe {
    tsc_start_compare_pit(ticks);
  }

  while !TSC_INIT_TIMER_FIRED.load(Ordering::Acquire) {
    core::hint::spin_loop();
  }
  let new_tsc_value = read_tsc();
  let tsc_freq_hz = (new_tsc_value - initial_tsc_value) * 100;
  TSC_FREQUENCY.store(tsc_freq_hz, Ordering::Release);
  debug!("Detected TSC frequency is {} Hz", TSC_FREQUENCY.load(Ordering::Acquire));
}

extern "x86-interrupt" fn init_tsc_callback(_: InterruptStackFrame) {
  TSC_INIT_TIMER_FIRED.store(true, Ordering::Release);
  super::apic::send_local_apic_eoi();
}

pub fn read_tsc() -> u64 {
  let tsc_value: u64;

  unsafe {
    asm!(
      "rdtsc",
      "shl rdx, 32",
      "add rdx, rax",
      out("rdx") tsc_value,
      out("rax") _,
    );
  }

  tsc_value
}
