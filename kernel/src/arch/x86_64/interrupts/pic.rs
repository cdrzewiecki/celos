use bitflags::bitflags;
use x86_64::instructions::port::Port;

const PIC1: u16 = 0x20;
const PIC1_COMMAND: u16 = PIC1;
const PIC1_DATA: u16 = PIC1 + 1;
const PIC2: u16 = 0xA0;
const PIC2_COMMAND: u16 = PIC1;
const PIC2_DATA: u16 = PIC2 + 1;

const PIC1_OFFSET: u8 = 32;
const PIC2_OFFSET: u8 = PIC1_OFFSET + 8;

bitflags! {
  struct ICW1: u8 {
    const USE_ICW4 = 0b1;
    const SINGLE = 0b10;
    const INTERVAL_4 = 0b100;
    const LEVEL_TRIGGERED = 0b1000;
    const INIT = 0b1_0000;
  }
}

bitflags! {
  struct ICW4: u8 {
    const MODE_8086 = 0b1;
    const AUTO_EOI = 0b10;
    const MASTER = 0b100;
    const BUFFERED = 0b1000;
    const SPECIAL_FULLY_NESTED = 0b1_0000;
  }
}

bitflags! {
  struct OCW1: u8 {
    const DISABLE_IRQ0 = 0b1;
    const DISABLE_IRQ1 = 0b10;
    const DISABLE_IRQ2 = 0b100;
    const DISABLE_IRQ3 = 0b1000;
    const DISABLE_IRQ4 = 0b1_0000;
    const DISABLE_IRQ5 = 0b10_0000;
    const DISABLE_IRQ6 = 0b100_0000;
    const DISABLE_IRQ7 = 0b1000_0000;
  }
}

pub fn init() {
  let mut pic1_command: Port<u8> = Port::new(PIC1_COMMAND);
  let mut pic1_data: Port<u8> = Port::new(PIC1_DATA);
  let mut pic2_command: Port<u8> = Port::new(PIC2_COMMAND);
  let mut pic2_data: Port<u8> = Port::new(PIC2_DATA);

  unsafe {
    let mut wait_port: Port<u8> = Port::new(0x80);
    let mut wait = || wait_port.write(0);

    pic1_command.write((ICW1::INIT | ICW1::USE_ICW4).bits);
    wait();
    pic2_command.write((ICW1::INIT | ICW1::USE_ICW4).bits);
    wait();

    pic1_data.write(PIC1_OFFSET);
    wait();
    pic2_data.write(PIC2_OFFSET);
    wait();

    pic1_data.write(4); // Tell master PIC that it has a slave at IRQ2 (0b100)
    wait();
    pic2_data.write(2); // Tell slave PIC that its cascade identity is 0b10
    wait();

    pic1_data.write(ICW4::MODE_8086.bits);
    wait();
    pic2_data.write(ICW4::MODE_8086.bits);
    wait();
  }
}

pub fn disable_pic() {
  let mut pic1_data: Port<u8> = Port::new(PIC1_DATA);
  let mut pic2_data: Port<u8> = Port::new(PIC2_DATA);

  unsafe {
    let mut wait_port: Port<u8> = Port::new(0x80);
    let mut wait = || wait_port.write(0);

    pic1_data.write(OCW1::all().bits);
    wait();
    pic2_data.write(OCW1::all().bits);
    wait();
  }
}
