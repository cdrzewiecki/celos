use lazy_static::lazy_static;
use log::warn;
use spin::RwLock;
use x86_64::{
  registers::control::Cr2,
  structures::{
    idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode},
    paging::{Page, Size4KiB as Size4KB},
  },
};

use super::{gdt, memory};
use crate::serial_println;

pub(in crate::arch::x86_64) mod apic;
pub(in crate::arch::x86_64) mod pic;
pub(in crate::arch::x86_64) mod timing;

#[allow(unused_imports)]
pub(in crate::arch::x86_64) use apic::{IO_APIC, LOCAL_APIC};
pub(in crate::arch::x86_64) use timing::PIT;
pub use x86_64::instructions::interrupts::without_interrupts;

lazy_static! {
  static ref IDT: RwLock<InterruptDescriptorTable> = RwLock::new({
    let mut idt = InterruptDescriptorTable::new();
    unsafe {
      idt.double_fault.set_handler_fn(double_fault_handler).set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX);
      idt.page_fault.set_handler_fn(page_fault_handler).set_stack_index(gdt::PAGE_FAULT_IST_INDEX);
    }
    idt.segment_not_present.set_handler_fn(segment_not_present_handler);
    idt.general_protection_fault.set_handler_fn(general_protection_fault_handler);

    idt[InterruptMap::Pit as usize].set_handler_fn(pit_handler);
    idt[InterruptMap::ApicTimer as usize].set_handler_fn(apic_timer_handler);

    idt
  });
}

pub fn init() {
  gdt::init();
  init_idt();
  pic::init();
}

pub fn init_stage2() {
  pic::disable_pic();
  PIT.lock().disable_timer0();
  x86_64::instructions::interrupts::enable();
  apic::init();
  timing::init_tsc();
  x86_64::instructions::interrupts::without_interrupts(|| {
    IDT.write()[InterruptMap::Pit as usize].set_handler_fn(pit_handler);
  });
}

fn init_idt() {
  unsafe {
    IDT.write().load_unsafe();
  }
}

extern "x86-interrupt" fn double_fault_handler(stack_frame: InterruptStackFrame, _error_code: u64) -> ! {
  panic!("EXCEPTION: DOUBLE FAULT\n{stack_frame:#?}");
}

extern "x86-interrupt" fn page_fault_handler(stack_frame: InterruptStackFrame, error_code: PageFaultErrorCode) {
  let accessed_address = Cr2::read();
  if error_code & PageFaultErrorCode::PROTECTION_VIOLATION == PageFaultErrorCode::PROTECTION_VIOLATION {
    // This page fault was caused by a protection violation
    // We don't handle those right now
    panic!("Unrecoverable page fault caused by protection violation!\nAddress: {accessed_address:?}\nError code: {error_code:?}");
  } else {
    // This page fault was caused by a not-present page, which we DO handle
    // First, see if the problem was caused by kernel code
    serial_println!("Accessed address: {:#x}", accessed_address);
    let kernel_start_addr =
      memory::KERNEL_MEMORY.try_lock().expect("Unable to lock mutex for kernel memory info!").kernel_start;
    let kernel_end_addr =
      memory::KERNEL_MEMORY.try_lock().expect("Unable to lock mutex for kernel memory info!").kernel_end;
    let kernel_start_page = Page::containing_address(kernel_start_addr);
    let kernel_end_page = Page::containing_address(kernel_end_addr);
    let mut kernel_page_range = Page::range_inclusive(kernel_start_page, kernel_end_page);
    serial_println!("instruction pointer is {:#x}", stack_frame.instruction_pointer);
    let instruction_page = Page::containing_address(stack_frame.instruction_pointer);

    if kernel_page_range.any(|page: Page<Size4KB>| page == instruction_page) {
      // The instruction pointer was set to an address inside the kernel range
      // Check for a kernel stack overflow
      let kernel_stack_bottom =
        memory::KERNEL_MEMORY.try_lock().expect("Unable to lock mutex for kernel memory info!").stack_bottom;
      if accessed_address >= (kernel_stack_bottom - 0x1000_u64) && accessed_address <= kernel_stack_bottom {
        panic!("The page fault was in the kernel stack, the kernel stack most likely needs to be resized.\nAddress: {accessed_address:?}\nError code: {error_code:?}");
      } else {
        panic!("Unrecoverable kernel page fault, unable to determine cause!\nAddress: {accessed_address:?}\nError code: {error_code:?}");
      }
    } else {
      // The instruction pointer was set elsewhere, so this is userspace
      panic!(
        "Unrecoverable page fault!\nAddress {:?} appears to be in the userspace address range, which is not yet supported",
        stack_frame.instruction_pointer
      );
    }
  }
}

extern "x86-interrupt" fn segment_not_present_handler(stack_frame: InterruptStackFrame, error_code: u64) {
  panic!("EXCEPTION: SEGMENT NOT PRESENT\n{stack_frame:#?}\nsegment selector {error_code}");
}

extern "x86-interrupt" fn general_protection_fault_handler(stack_frame: InterruptStackFrame, error_code: u64) {
  panic!("EXCEPTION: GENERAL PROTECTION FAULT\n{stack_frame:#?}\nerror code {error_code}");
}

extern "x86-interrupt" fn pit_handler(_: InterruptStackFrame) {
  warn!("PIT interrupt occurred but handler is not set up");
  apic::send_local_apic_eoi();
}

extern "x86-interrupt" fn apic_timer_handler(_: InterruptStackFrame) {
  crate::multitasking::wake_tasks();
  apic::send_local_apic_eoi();
  crate::multitasking::update_timeslice();
  crate::multitasking::preempt_running_task();
}

pub(in crate::arch::x86_64) struct IsaInterruptMap {
  pub pit: InterruptRemap,
  pub ps2_keyboard: InterruptRemap,
  pub com2: InterruptRemap,
  pub com1: InterruptRemap,
  pub lpt2: InterruptRemap,
  pub floppy: InterruptRemap,
  pub lpt1: InterruptRemap,
  pub rtc: InterruptRemap,
  pub irq9: InterruptRemap,
  pub irq10: InterruptRemap,
  pub irq11: InterruptRemap,
  pub ps2_mouse: InterruptRemap,
  pub coprocessor: InterruptRemap,
  pub primary_ata: InterruptRemap,
  pub secondary_ata: InterruptRemap,
}

pub(in crate::arch::x86_64) const ISA_INTERRUPT_MAP: IsaInterruptMap = IsaInterruptMap {
  pit: InterruptRemap { original: 0, new: InterruptMap::Pit as u8 },
  ps2_keyboard: InterruptRemap { original: 1, new: InterruptMap::Ps2Keyboard as u8 },
  com2: InterruptRemap { original: 3, new: InterruptMap::Com2 as u8 },
  com1: InterruptRemap { original: 4, new: InterruptMap::Com1 as u8 },
  lpt2: InterruptRemap { original: 5, new: InterruptMap::Lpt2 as u8 },
  floppy: InterruptRemap { original: 6, new: InterruptMap::Floppy as u8 },
  lpt1: InterruptRemap { original: 7, new: InterruptMap::Lpt1 as u8 },
  rtc: InterruptRemap { original: 8, new: InterruptMap::Rtc as u8 },
  irq9: InterruptRemap { original: 9, new: InterruptMap::IsaIrq9 as u8 },
  irq10: InterruptRemap { original: 10, new: InterruptMap::IsaIrq10 as u8 },
  irq11: InterruptRemap { original: 11, new: InterruptMap::IsaIrq11 as u8 },
  ps2_mouse: InterruptRemap { original: 12, new: InterruptMap::Ps2Mouse as u8 },
  coprocessor: InterruptRemap { original: 13, new: InterruptMap::Coprocessor as u8 },
  primary_ata: InterruptRemap { original: 14, new: InterruptMap::PrimaryAta as u8 },
  secondary_ata: InterruptRemap {
    original: 15,
    new: InterruptMap::SecondaryAta as u8,
  },
};

impl IntoIterator for IsaInterruptMap {
  type Item = InterruptRemap;
  type IntoIter = core::array::IntoIter<InterruptRemap, 15>;

  fn into_iter(self) -> Self::IntoIter {
    [
      self.pit,
      self.ps2_keyboard,
      self.com2,
      self.com1,
      self.lpt2,
      self.floppy,
      self.lpt1,
      self.rtc,
      self.irq9,
      self.irq10,
      self.irq11,
      self.ps2_mouse,
      self.coprocessor,
      self.primary_ata,
      self.secondary_ata,
    ]
    .into_iter()
  }
}

#[derive(Clone, Copy, Debug)]
pub(in crate::arch::x86_64) struct InterruptRemap {
  pub original: u8,
  pub new: u8,
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub(in crate::arch::x86_64) enum InterruptMap {
  Pit = 0x20,
  ApicTimer = 0x21,
  Rtc = 0x22,
  Coprocessor = 0x23,
  Ps2Keyboard = 0x30,
  Ps2Mouse = 0x31,
  PrimaryAta = 0x40,
  SecondaryAta = 0x41,
  Com1 = 0xe0,
  Com2 = 0xe1,
  Lpt1 = 0xe2,
  Lpt2 = 0xe3,
  Floppy = 0xe4,
  IsaIrq9 = 0xe5,
  IsaIrq10 = 0xe6,
  IsaIrq11 = 0xe7,
  Spurious = 0xff,
}
