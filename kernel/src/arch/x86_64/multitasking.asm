section .text
bits 64

global task_switch_inner
; Rust declaration of function:
; extern "C" {
;   fn task_switch_inner(current_task: *mut Thread<X86Data>, next_task: *mut Thread<X86Data>);
; }
global task_init
; Rust declaration of function:
; extern "C" {
;   fn task_init();
; }

task_switch_inner:
  ; Save previous thread state
  push rbx
  push rbp
  push r12
  push r13
  push r14
  push r15

  ; the structure of the thread control block should be as such:
  ; [pointer] - FXSAVE area pointer, *mut u8
  ; [pointer + 8] - TSC value last time thread was scheduled (or otherwise updated its running time), u64
  ; [pointer + 16] - Address of P4 table to use for thread's paging needs, PhysAddr (which is a u64)
  ; [pointer + 24] - Stack top pointer, *mut u8

  ; Save floating point data
  mov rbx, [rdi]
  fxsave64 [rbx]

  mov [rdi + 24], rsp ; save stack pointer to thread control block
  mov rsp, [rsi + 24] ; next thread control block is at rsi

  ; compare cr3 to the P4 address of next thread to see if we need to reload it
  mov rax, cr3
  mov rbx, [rsi + 16]

  cmp rax, rbx
  je .same_address_space
  mov cr3, rbx

.same_address_space:

  mov rbx, [rsi]
  fxrstor64 [rbx]

  pop r15
  pop r14
  pop r13
  pop r12
  pop rbp
  pop rbx

  ; compare the address on the stack (what we will return to) to see if it is in the higher half. If it isn't, then there's an issue.
  mov rax, 0xffff800000000000
  cmp rax, [rsp]
  jb .ret_address_ok

  ; if we didn't jump then there's an issue - call the rust debug function
  mov rdi, rsi
  extern debug_mangled_task
  call debug_mangled_task

.ret_address_ok:
  ret

task_init:
  extern unlock_scheduler
  call unlock_scheduler
  sti
  ret
