use core::arch::asm;

use bit_field::BitField;
use log::debug;
use x86_64::{
  instructions::interrupts::without_interrupts,
  registers::model_specific::{Efer, EferFlags},
};

pub fn init() {
  let current_flags = Efer::read();
  let new_flags = current_flags | EferFlags::NO_EXECUTE_ENABLE | EferFlags::SYSTEM_CALL_EXTENSIONS;
  debug!("Setting EFER to {new_flags:?}");

  unsafe {
    Efer::write(new_flags);
  }
}

pub(in crate::arch::x86_64) fn initial_apic_id() -> u8 {
  let cpuid_value: u32;

  unsafe {
    asm!(
      "mov eax, 1",
      "push rbx",
      "cpuid",
      "mov eax, ebx",
      "pop rbx",
      out("eax") cpuid_value,
      out("ecx") _,
      out("edx") _,
    );
  }

  u8::try_from(cpuid_value.get_bits(24..32)).unwrap()
}

#[derive(Clone, Copy, Debug)]
#[repr(u32)]
pub(in crate::arch::x86_64) enum MsrId {
  Ia32ApicBase = 0x01b,
}

pub fn cpu_index() -> usize {
  without_interrupts(|| super::interrupts::LOCAL_APIC.lock().id() as usize)
}
