mod x86_data;

use alloc::vec::Vec;
use core::{arch::asm, convert::TryFrom, ops::DerefMut, sync::atomic::Ordering, time::Duration};

use log::debug;
use x86_64::{
  registers::control::Cr3,
  structures::paging::{
    Page, PageSize, PageTable, PageTableFlags, PhysFrame, Size1GiB as Size1GB, Size2MiB as Size2MB, Size4KiB as Size4KB,
  },
  PhysAddr, VirtAddr,
};
pub use x86_data::X86Data;

use super::memory::{
  paging::{CustomPageFlags, SharedMapping},
  KERNEL_STACK_ADDR, PHYSICAL_MEMORY_OFFSET,
};
use crate::{
  arch::x86_64::interrupts::timing::read_tsc,
  interrupts::timing::SECOND,
  memory::{
    paging::{create_mapping, MappingFlags, ProtectionFlags},
    KERNEL_STACK_AREA,
  },
  multitasking::{
    Process, ProcessCreationError, Stack, StackDirection, TaskState, Thread, ThreadCreationError, DEFAULT_STACK_SIZE,
    DEFAULT_TIMESLICE, IDLE_TASK_TID, KERNEL_PID, PROCESSES, THREAD_IDS,
  },
};

extern "C" {
  #[allow(improper_ctypes)]
  fn task_switch_inner(current_task: *mut Thread<X86Data>, next_task: *mut Thread<X86Data>);
  fn task_init();
}

pub type ArchData = X86Data;

pub fn create_initial_process() -> Process {
  let (p4_frame, _) = Cr3::read();
  let p4_address = p4_frame.start_address();

  Process::new(KERNEL_PID, p4_address)
}

pub fn create_initial_thread() -> Thread<X86Data> {
  let arch_data = X86Data::new();
  let (p4_frame, _) = Cr3::read();
  let p4_address = p4_frame.start_address();
  let stack_top: *mut u8;
  let stack_bottom = KERNEL_STACK_ADDR;

  unsafe {
    asm!(
      "mov {}, rsp",
      out(reg) stack_top,
    );
  }

  let stack = Stack::new(
    stack_top,
    stack_bottom.as_ptr::<u8>(),
    super::memory::KERNEL_INITIAL_STACK_SIZE as usize,
    StackDirection::GrowDown,
  );

  let mut thread = Thread::new(stack, p4_address, Duration::MAX, KERNEL_PID, IDLE_TASK_TID, arch_data);
  thread.set_state(TaskState::Running);
  thread.update_running_time();
  thread
}

pub fn start_tick_timer(interval: Duration) {
  use super::interrupts::timing::Timer;

  super::interrupts::LOCAL_APIC.lock().start_periodic(interval);
}

/// Creates a new process.
///
/// # Errors
/// Returns `ProcessCreationError::PageTableCreationFailed` if the frame allocator fails to return a frame
/// for the new process' page table (most likely the system is OOM in that case).
pub fn new_process(process_id: u32) -> Result<Process, ProcessCreationError> {
  // Get current L4 page table and create a new one for the new process
  let (current_p4_frame, _) = Cr3::read();
  let current_p4_virt = VirtAddr::new(current_p4_frame.start_address().as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64());
  let current_p4_table = unsafe { &(*current_p4_virt.as_mut_ptr::<PageTable>()) };

  let new_p4_frame = match super::memory::allocate_frame::<Size4KB>() {
    Some(frame) => frame,
    None => {
      debug!("No frame returned from frame allocator, can't create process");
      return Err(ProcessCreationError::PageTableCreationFailed);
    }
  };

  let new_p4_virt = VirtAddr::new(new_p4_frame.start_address().as_u64() + PHYSICAL_MEMORY_OFFSET.as_u64());
  let new_p4_table = unsafe { &mut (*new_p4_virt.as_mut_ptr::<PageTable>()) };

  // Copy entries 256-511 (i.e. everything in the higher half) to the new page table
  for i in 256..512 {
    let entry = &current_p4_table[i];
    if entry.flags().contains(PageTableFlags::PRESENT) {
      new_p4_table[i] = entry.clone();
    }
  }

  super::memory::init_process_page_allocator(new_p4_frame.start_address());

  Ok(Process::new(process_id, new_p4_frame.start_address()))
}

/// Creates a new thread.
///
/// # Errors
/// Returns `ThreadCreationError::NoStack` if there was an issue creating the stack for the new thread.
pub fn new_thread(
  process_id: u32,
  thread_id: u32,
  entry_point: extern "C" fn() -> !,
) -> Result<Thread<X86Data>, ThreadCreationError> {
  let stack_addr = if process_id == KERNEL_PID {
    Some(KERNEL_STACK_AREA)
  } else {
    None
  };

  let p4_address = PROCESSES.lock().get(&process_id).expect("No process with given process ID").paging_base_addr();

  let stack = match unsafe { new_thread_stack(p4_address, stack_addr, None, entry_point) } {
    Some(stack) => stack,
    None => return Err(ThreadCreationError::NoStack),
  };

  let arch_data = X86Data::new();
  let thread = Thread::new(stack, p4_address, DEFAULT_TIMESLICE, process_id, thread_id, arch_data);

  Ok(thread)
}

unsafe fn new_thread_stack(
  p4_address: PhysAddr,
  addr: Option<VirtAddr>,
  size: Option<usize>,
  entry_point: extern "C" fn() -> !,
) -> Option<Stack> {
  // Create an 8MB stack area for the new process
  // Remember - stack needs to be aligned to 16 bytes for FP instructions
  let stack_size = size.unwrap_or(DEFAULT_STACK_SIZE);
  let protection = ProtectionFlags::Present | ProtectionFlags::Writable;
  let flags = MappingFlags::GrowDown | MappingFlags::GuardPage;
  if let Ok(page_range) = create_mapping::<Size4KB>(p4_address, addr, None, stack_size, flags, protection) {
    // Push the pointer to the process entry point onto the stack we have allocated
    // Note: we need to do some alignment shenanigans before we push the stack pointer on. We take a SysV ABI function pointer.
    // The SysV ABI says that the stack must be 16-byte aligned *before* calling the function. That is, when we are in the function
    // the stack will be aligned at an odd multiple of 8 bytes (since a return address got pushed). Since we do not actually call
    // the function we take the pointer of (we just have switch_to_task return to that instruction), the stack will not be aligned
    // that way. Therefore, we deliberately align the stack just off by 8 bytes so that the stack will be aligned the way that the
    // compiler expects it to be once it is inside f().
    // TLDR: we need to have the stack at an 8-byte alignment, not a 16-byte alignment. We do that by subtracting 8 from the otherwise
    // 16-byte aligned stack top.
    //
    // ^ basically the Diagram
    let stack_bottom = page_range.end.start_address().as_mut_ptr::<u8>();
    let mut stack_top = stack_bottom.sub(0x8);

    asm!(
      "mov {tmp_rsp}, rsp",
      "mov {tmp_cr3}, cr3",
      "mov cr3, {p4_address}",
      "mov rsp, {stack_top}",
      "push {entry_point:r}",
      "push {task_init:r}",
      "sub rsp, 48", // Move the stack pointer down to account for the data we will pop off in the future
      "mov {stack_top}, rsp",
      "mov cr3, {tmp_cr3}",
      "mov rsp, {tmp_rsp}",
      stack_top = inout(reg) stack_top,
      entry_point = in(reg) entry_point,
      task_init = in(reg) task_init,
      p4_address = in(reg) p4_address.as_u64(),
      tmp_rsp = out(reg) _,
      tmp_cr3 = out(reg) _,
      options(nostack)
    );

    let stack = Stack::new(stack_top, stack_bottom, stack_size, StackDirection::GrowDown);
    Some(stack)
  } else {
    None
  }
}

pub fn time_since_boot() -> Duration {
  let elapsed_tsc = super::interrupts::timing::read_tsc();
  let tsc_freq = super::interrupts::timing::TSC_FREQUENCY.load(Ordering::Acquire);

  let elapsed_ns = (u128::from(elapsed_tsc) * SECOND.as_nanos()) / u128::from(tsc_freq);
  Duration::from_nanos(u64::try_from(elapsed_ns).expect("Nanoseconds since boot is too large to fit into a u64!"))
}

pub fn task_switch(current_task: &mut Thread<X86Data>, next_task: &mut Thread<X86Data>) {
  if current_task.state() == TaskState::Running {
    current_task.set_state(TaskState::ReadyToRun);
  }

  next_task.set_state(TaskState::Running);
  next_task.arch_data_mut().tsc_when_ran = read_tsc();
  unsafe {
    task_switch_inner(current_task as *mut Thread<X86Data>, next_task as *mut Thread<X86Data>);
  }
}

#[no_mangle]
unsafe extern "C" fn debug_mangled_task(thread: *mut Thread<X86Data>) {
  panic!("Bad task detected - address of bad task is {:p}, stack top is {:p}", thread, (*thread).stack_top());
}

#[allow(clippy::needless_pass_by_value)]
pub fn teardown_thread(thread: Thread<X86Data>) {
  let stack_bottom = VirtAddr::new(thread.stack_bottom() as u64);
  let stack_top = stack_bottom - thread.stack_size() as u64;
  let bottom_page = Page::<Size4KB>::containing_address(stack_bottom);
  let top_page = Page::<Size4KB>::containing_address(stack_top);
  let guard_page = top_page - 1;
  let p4_address = PROCESSES.lock().get(&thread.process_id()).expect("No process for thread").paging_base_addr();

  for page in Page::range(top_page, bottom_page) {
    unsafe {
      let frame = super::memory::paging::remove_mapping(p4_address, page).expect("Failed to remove page mapping");
      super::memory::free_frame(frame);
    }
  }

  unsafe {
    super::memory::paging::remove_mapping(p4_address, guard_page).expect("Failed to free guard page");
  }

  THREAD_IDS.lock().push(thread.thread_id());
}

#[allow(clippy::missing_panics_doc)]
pub fn teardown_process(process: Process) {
  let p4_virt = PHYSICAL_MEMORY_OFFSET + process.paging_base_addr().as_u64();
  let page_table = unsafe { &(*p4_virt.as_mut_ptr::<PageTable>()) };

  // We only want to tear down the lower half page table entries, thus why the partial iteration here
  for p4_index in 0..256 {
    let p4_entry = &page_table[p4_index];

    if p4_entry.flags().contains(PageTableFlags::PRESENT) {
      let p4_frame = p4_entry.frame().unwrap();
      let p3_virt = PHYSICAL_MEMORY_OFFSET + p4_frame.start_address().as_u64();
      let p3_table = unsafe { &(*p3_virt.as_mut_ptr::<PageTable>()) };

      for p3_entry in p3_table.iter() {
        if p3_entry.flags().contains(PageTableFlags::PRESENT) {
          if p3_entry.flags().contains(PageTableFlags::HUGE_PAGE) {
            let p3_frame = PhysFrame::<Size1GB>::from_start_address(p3_entry.addr()).unwrap();

            if CustomPageFlags::from(p3_entry.flags()).contains(CustomPageFlags::SharedFrame) {
              let mut shared_mappings = super::memory::SHARED_MAPPINGS_1GB.lock();
              handle_shared_mapping(&mut shared_mappings, process, p3_frame);
              drop(shared_mappings);
            } else {
              unsafe {
                super::memory::free_frame(p3_frame);
              }
            }
          } else {
            let p3_frame = p3_entry.frame().unwrap();
            let p2_virt = PHYSICAL_MEMORY_OFFSET + p3_frame.start_address().as_u64();
            let p2_table = unsafe { &(*p2_virt.as_mut_ptr::<PageTable>()) };

            for p2_entry in p2_table.iter() {
              if p2_entry.flags().contains(PageTableFlags::PRESENT) {
                if p2_entry.flags().contains(PageTableFlags::HUGE_PAGE) {
                  let p2_frame = PhysFrame::<Size2MB>::from_start_address(p2_entry.addr()).unwrap();

                  if CustomPageFlags::from(p2_entry.flags()).contains(CustomPageFlags::SharedFrame) {
                    let mut shared_mappings = super::memory::SHARED_MAPPINGS_2MB.lock();
                    handle_shared_mapping(&mut shared_mappings, process, p2_frame);
                    drop(shared_mappings);
                  } else {
                    unsafe {
                      super::memory::free_frame(p2_frame);
                    }
                  }
                } else {
                  let p2_frame = p2_entry.frame().unwrap();
                  let p1_virt = PHYSICAL_MEMORY_OFFSET + p2_frame.start_address().as_u64();
                  let p1_table = unsafe { &(*p1_virt.as_mut_ptr::<PageTable>()) };

                  for p1_entry in p1_table.iter() {
                    if p1_entry.flags().contains(PageTableFlags::PRESENT) {
                      let p1_frame = p1_entry.frame().unwrap();

                      if CustomPageFlags::from(p1_entry.flags()).contains(CustomPageFlags::SharedFrame) {
                        let mut shared_mappings = super::memory::SHARED_MAPPINGS_4KB.lock();
                        handle_shared_mapping(&mut shared_mappings, process, p1_frame);
                        drop(shared_mappings);
                      } else {
                        unsafe {
                          super::memory::free_frame(p1_frame);
                        }
                      }
                    }
                  }

                  unsafe {
                    super::memory::free_frame(p2_frame);
                  }
                }
              }
            }

            unsafe {
              super::memory::free_frame(p3_frame);
            }
          }
        }
      }

      unsafe {
        super::memory::free_frame(p4_frame);
      }
    }
  }
}

fn handle_shared_mapping<S: PageSize>(
  mappings: &mut impl DerefMut<Target = Vec<SharedMapping<S>>>,
  process: Process,
  frame: PhysFrame<S>,
) {
  let mut remove_mapping = false;
  let mut index = 0;

  for i in 0..mappings.len() {
    let mapping = &mut mappings[i];

    if mapping.frame() == frame {
      mapping.remove_process(process).expect("Frame was listed in shared mappings but process was not");

      if !mapping.in_use() {
        remove_mapping = true;
      }

      index = i;
      break;
    }
  }

  if remove_mapping {
    mappings.remove(index);

    unsafe {
      super::memory::free_frame(frame);
    }
  }
}
