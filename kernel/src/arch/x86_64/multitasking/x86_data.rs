use alloc::alloc::{alloc_zeroed, dealloc};
use core::{alloc::Layout, sync::atomic::Ordering, time::Duration};

use crate::{
  arch::interrupts::timing::{read_tsc, TSC_FREQUENCY},
  interrupts::timing::SECOND,
  multitasking::{ArchDataTrait, TaskState},
};

const FXSAVE_LAYOUT: Layout = unsafe { Layout::from_size_align_unchecked(512, 16) };

#[derive(Debug)]
pub struct X86Data {
  fxsave_area: *mut u8,
  pub(super) tsc_when_ran: u64,
}

unsafe impl Send for X86Data {}

impl X86Data {
  #[allow(clippy::missing_panics_doc)]
  pub fn new() -> Self {
    let fxsave_area = unsafe { alloc_zeroed(FXSAVE_LAYOUT) };
    assert!(!fxsave_area.is_null(), "Unable to allocate fxsave area for new task");

    X86Data { fxsave_area, tsc_when_ran: 0 }
  }
}

impl Default for X86Data {
  fn default() -> Self {
    Self::new()
  }
}

impl Drop for X86Data {
  fn drop(&mut self) {
    unsafe {
      dealloc(self.fxsave_area, FXSAVE_LAYOUT);
    }
  }
}

impl ArchDataTrait for X86Data {
  fn delta_runtime(&mut self, thread_state: &TaskState) -> Duration {
    let current_tsc = read_tsc();
    let elapsed_tsc = u128::from(current_tsc - self.tsc_when_ran);

    if *thread_state == TaskState::Running {
      self.tsc_when_ran = current_tsc;
    }

    let elapsed_ns = elapsed_tsc * SECOND.as_nanos() / u128::from(TSC_FREQUENCY.load(Ordering::Acquire));
    Duration::from_nanos(u64::try_from(elapsed_ns).unwrap())
  }
}
