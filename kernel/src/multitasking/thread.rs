use core::time::Duration;

use super::{is_idle_task, Stack, TaskState};
use crate::arch::PhysAddr;

pub trait ArchDataTrait {
  fn delta_runtime(&mut self, thread_state: &TaskState) -> Duration;
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Thread<T: ArchDataTrait> {
  arch_data: T,
  paging_base_addr: PhysAddr,
  stack: Stack,
  time_running: Duration,
  timeslice: Duration,
  process_id: u32,
  thread_id: u32,
  state: TaskState,
}

impl<T: ArchDataTrait> Thread<T> {
  pub fn new(
    stack: Stack,
    paging_base_addr: PhysAddr,
    timeslice: Duration,
    process_id: u32,
    thread_id: u32,
    arch_data: T,
  ) -> Self {
    Self {
      stack,
      paging_base_addr,
      time_running: Duration::ZERO,
      timeslice,
      process_id,
      thread_id,
      state: TaskState::ReadyToRun,
      arch_data,
    }
  }

  pub fn stack_top(&mut self) -> *mut u8 {
    self.stack.stack_top()
  }

  pub fn stack_bottom(&self) -> *const u8 {
    self.stack.stack_bottom()
  }

  pub fn stack_size(&self) -> usize {
    self.stack.size()
  }

  pub fn paging_base_addr(&self) -> PhysAddr {
    self.paging_base_addr
  }

  pub fn time_running(&self) -> Duration {
    self.time_running
  }

  pub fn timeslice(&self) -> Duration {
    self.timeslice
  }

  pub fn set_timeslice(&mut self, timeslice: Duration) {
    self.timeslice = timeslice;
  }

  pub fn process_id(&self) -> u32 {
    self.process_id
  }

  pub fn thread_id(&self) -> u32 {
    self.thread_id
  }

  pub fn state(&self) -> TaskState {
    self.state
  }

  pub fn set_state(&mut self, state: TaskState) {
    self.state = state;
  }

  pub fn arch_data(&self) -> &T {
    &self.arch_data
  }

  pub fn arch_data_mut(&mut self) -> &mut T {
    &mut self.arch_data
  }

  pub fn update_running_time(&mut self) {
    let delta_duration = self.arch_data.delta_runtime(&self.state);
    self.time_running = self.time_running.saturating_add(delta_duration);

    if !is_idle_task(self) {
      self.decrement_timeslice(delta_duration);
    }
  }

  pub fn decrement_timeslice(&mut self, interval: Duration) {
    self.timeslice = self.timeslice.saturating_sub(interval);
  }
}
