#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum StackDirection {
  GrowDown,
  GrowUp,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Stack {
  stack_top: *mut u8,
  stack_bottom: *const u8,
  stack_size: usize,
  stack_direction: StackDirection,
}

impl Stack {
  /// Creates the `Stack` object.
  ///
  /// # Panics
  /// Panics if `stack_top` and `stack_bottom` are in the wrong order for the given `stack_direction`. For example,
  /// when `stack_direction` is `GrowDown` then `stack_top` must not be greater than `stack_bottom`.
  pub fn new(stack_top: *mut u8, stack_bottom: *const u8, stack_size: usize, stack_direction: StackDirection) -> Self {
    // Assert that the stack top and stack bottom given are in accord with the stack direction given
    match stack_direction {
      StackDirection::GrowDown => assert!(stack_top as usize <= stack_bottom as usize),
      StackDirection::GrowUp => assert!(stack_top as usize >= stack_bottom as usize),
    }

    Self {
      stack_top,
      stack_bottom,
      stack_size,
      stack_direction,
    }
  }

  pub fn stack_top(&mut self) -> *mut u8 {
    self.stack_top
  }

  pub fn stack_bottom(&self) -> *const u8 {
    self.stack_bottom
  }

  pub fn size(&self) -> usize {
    self.stack_size
  }
}

unsafe impl Send for Stack {}
