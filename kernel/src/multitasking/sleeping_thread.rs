use core::cmp::Ordering;

use super::{ArchDataTrait, Thread};

#[derive(Debug)]
pub struct SleepingThread<T: ArchDataTrait> {
  wake_time: u64,
  thread: Thread<T>,
}

impl<T: ArchDataTrait> SleepingThread<T> {
  pub fn new(wake_time: u64, thread: Thread<T>) -> Self {
    Self { wake_time, thread }
  }

  pub fn wake_time(&self) -> u64 {
    self.wake_time
  }

  pub fn task(self) -> Thread<T> {
    self.thread
  }

  pub fn thread_ref(&self) -> &Thread<T> {
    &(self.thread)
  }

  pub fn thread_mut(&mut self) -> &mut Thread<T> {
    &mut self.thread
  }
}

impl<T: ArchDataTrait> PartialEq for SleepingThread<T> {
  fn eq(&self, other: &Self) -> bool {
    self.wake_time == other.wake_time
      && self.thread.process_id() == other.thread.process_id()
      && self.thread.thread_id() == other.thread.thread_id()
  }
}

impl<T: ArchDataTrait> Eq for SleepingThread<T> {}

impl<T: ArchDataTrait> PartialOrd for SleepingThread<T> {
  fn ge(&self, other: &Self) -> bool {
    self.wake_time >= other.wake_time
  }

  fn gt(&self, other: &Self) -> bool {
    self.wake_time > other.wake_time
  }

  fn le(&self, other: &Self) -> bool {
    self.wake_time <= other.wake_time
  }

  fn lt(&self, other: &Self) -> bool {
    self.wake_time < other.wake_time
  }

  #[allow(clippy::comparison_chain)]
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    if self < other {
      Some(Ordering::Less)
    } else if self > other {
      Some(Ordering::Greater)
    } else if self == other {
      Some(Ordering::Equal)
    } else {
      None
    }
  }
}

impl<T: ArchDataTrait> Ord for SleepingThread<T> {
  #[allow(clippy::comparison_chain)]
  fn cmp(&self, other: &Self) -> Ordering {
    if self < other {
      Ordering::Less
    } else if self > other {
      Ordering::Greater
    } else if self == other {
      Ordering::Equal
    } else {
      panic!("Unable to determine ordering for SleepingTask")
    }
  }
}
