use crate::arch::PhysAddr;

#[derive(Clone, Copy, Debug)]
pub struct Process {
  process_id: u32,
  paging_base_addr: PhysAddr,
}

impl Process {
  pub const fn new(process_id: u32, paging_base_addr: PhysAddr) -> Self {
    Self { process_id, paging_base_addr }
  }

  pub fn process_id(&self) -> u32 {
    self.process_id
  }

  pub fn paging_base_addr(&self) -> PhysAddr {
    self.paging_base_addr
  }
}
