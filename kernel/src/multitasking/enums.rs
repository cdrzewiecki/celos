#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SleepError {
  DurationTooLong,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TaskUnblockError {
  TaskNotFound,
  TaskNotBlocked,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum TaskState {
  Running,
  ReadyToRun,
  Blocked(BlockCause),
  Zombie,
}

impl TaskState {
  pub fn block_cause(&self) -> Option<BlockCause> {
    if let TaskState::Blocked(cause) = *self {
      Some(cause)
    } else {
      None
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum BlockCause {
  Sleeping(u64),
  // This is just here to silence compiler warnings. Remove as soon as there is more than one block cause.
  Foo,
}

impl BlockCause {
  #[allow(clippy::match_wildcard_for_single_variants)]
  pub fn wake_time(&self) -> u64 {
    match *self {
      BlockCause::Sleeping(wake_time) => wake_time,
      _ => 0,
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ThreadCreationError {
  NoStack,
  NoParentProcess,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ProcessCreationError {
  ThreadError(ThreadCreationError),
  PageTableCreationFailed,
}
