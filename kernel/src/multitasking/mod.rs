mod enums;
mod process;
mod sleeping_thread;
mod stack;
mod thread;

use alloc::{
  collections::{BTreeMap, VecDeque},
  vec::Vec,
};
use core::{
  mem::discriminant,
  sync::atomic::{AtomicU32, Ordering},
  time::Duration,
};

pub use enums::*;
use lazy_static::lazy_static;
use log::{debug, trace, warn};
pub use process::*;
pub use sleeping_thread::SleepingThread;
use spin::Mutex;
pub use stack::*;
pub use thread::*;
use x86_64::structures::paging::{PageSize, Size2MiB as Size2MB};

use crate::{
  arch,
  arch::multitasking::{
    create_initial_process, create_initial_thread, start_tick_timer, task_switch, teardown_process, teardown_thread,
    time_since_boot, ArchData,
  },
  cpu::cpu_index,
  util::data_structures::{BinaryHeap, HeapType},
};

lazy_static! {
  static ref TASK_QUEUE: Mutex<VecDeque<Thread<ArchData>>> = Mutex::new(VecDeque::new());
}

const NO_TASK: Option<Thread<ArchData>> = None;
static SLEEPING_TASKS: Mutex<BinaryHeap<SleepingThread<ArchData>>> = Mutex::new(BinaryHeap::new(HeapType::Min));
static ZOMBIE_TASKS: Mutex<Vec<Thread<ArchData>>> = Mutex::new(Vec::new());
static RUNNING_TASKS: Mutex<[Option<Thread<ArchData>>; 32]> = Mutex::new([NO_TASK; 32]);
static IDLE_TASKS: Mutex<[Option<Thread<ArchData>>; 32]> = Mutex::new([NO_TASK; 32]);
static PROCESS_IDS: Mutex<BinaryHeap<u32>> = Mutex::new(BinaryHeap::new(HeapType::Min));
pub static THREAD_IDS: Mutex<BinaryHeap<u32>> = Mutex::new(BinaryHeap::new(HeapType::Min));
pub static PROCESSES: Mutex<BTreeMap<u32, Process>> = Mutex::new(BTreeMap::new());
static NEXT_PID: AtomicU32 = AtomicU32::new(1);
static NEXT_TID: AtomicU32 = AtomicU32::new(1);

pub const KERNEL_PID: u32 = 0;
pub const IDLE_TASK_TID: u32 = 0;
pub const TICK_INTERVAL: Duration = Duration::from_millis(1);
pub const DEFAULT_TIMESLICE: Duration = Duration::from_millis(50);
#[allow(clippy::cast_possible_truncation)]
pub const DEFAULT_STACK_SIZE: usize = 4 * Size2MB::SIZE as usize;

pub fn init() {
  let kernel_process = create_initial_process();
  PROCESSES.lock().insert(kernel_process.process_id(), kernel_process);
  RUNNING_TASKS.lock()[cpu_index()] = Some(create_initial_thread());

  spawn_thread(KERNEL_PID, kill_zombies).expect("Unable to create zombie killer thread");
  start_tick_timer(TICK_INTERVAL);
}

/// Unlocks the scheduler mutexes. This should only ever be called by a task that is starting for the first time.
///
/// # Safety
/// The caller must be a task which is initializing for the first time.
#[no_mangle]
#[allow(clippy::missing_panics_doc)]
pub unsafe extern "C" fn unlock_scheduler() {
  if RUNNING_TASKS.is_locked() {
    RUNNING_TASKS.force_unlock();
  }

  if IDLE_TASKS.is_locked() {
    IDLE_TASKS.force_unlock();
  }

  if TASK_QUEUE.is_locked() {
    TASK_QUEUE.force_unlock();
  }

  if SLEEPING_TASKS.is_locked() {
    SLEEPING_TASKS.force_unlock();
  }

  if ZOMBIE_TASKS.is_locked() {
    ZOMBIE_TASKS.force_unlock();
  }

  debug!("Got to scheduler unlock function on thread {:?}", RUNNING_TASKS.lock()[cpu_index()].as_ref().unwrap());
}

/// Spawns a new process with a single thread, adding the thread to the back of the run queue.
///
/// # Errors
/// Returns `ProcessCreationError` if there was an issue creating the process
pub fn spawn_process(entry_point: extern "C" fn() -> !) -> Result<(), ProcessCreationError> {
  let pid = match PROCESS_IDS.lock().pop() {
    Some(pid) => pid,
    None => NEXT_PID.fetch_add(1, Ordering::AcqRel),
  };

  let process = arch::multitasking::new_process(pid)?;
  PROCESSES.lock().insert(pid, process);

  match spawn_thread(pid, entry_point) {
    Ok(_) => Ok(()),
    Err(thread_err) => Err(ProcessCreationError::ThreadError(thread_err)),
  }
}

/// Spawns a new thread, adding it to the back of the run queue.
///
/// # Errors
/// Returns `ThreadCreationError` if there was an issue creating the thread
pub fn spawn_thread(pid: u32, entry_point: extern "C" fn() -> !) -> Result<(), ThreadCreationError> {
  let tid = match THREAD_IDS.lock().pop() {
    Some(tid) => tid,
    None => NEXT_TID.fetch_add(1, Ordering::AcqRel),
  };

  let thread = arch::multitasking::new_thread(pid, tid, entry_point)?;
  TASK_QUEUE.lock().push_back(thread);

  Ok(())
}

pub fn terminate_thread(tid: u32) {
  arch::interrupts::without_interrupts(|| {
    let mut running_tasks = RUNNING_TASKS.lock();
    let mut task_queue = TASK_QUEUE.lock();
    let mut sleeping_tasks = SLEEPING_TASKS.lock();
    let mut thread_running = false;

    let thread =
      if let Some(sleepy_thread) = sleeping_tasks.iter_mut().find(|thread| thread.thread_ref().thread_id() == tid) {
        Some(sleepy_thread.thread_mut())
      } else if let Some(thread) = task_queue.iter_mut().find(|thread| thread.thread_id() == tid) {
        Some(thread)
      } else if let Some(thread) = running_tasks[cpu_index()].as_mut() {
        // TODO: need to also search other CPUs for the thread, for the shiny future when SMP support is a thing
        thread_running = true;
        Some(thread)
      } else {
        None
      };

    if let Some(thread) = thread {
      thread.set_state(TaskState::Zombie);
    }

    drop(sleeping_tasks);
    drop(task_queue);
    drop(running_tasks);

    if thread_running {
      schedule();
    }
  });
}

#[allow(clippy::missing_panics_doc)]
pub fn terminate_current_thread() {
  arch::interrupts::without_interrupts(|| {
    let thread_id = RUNNING_TASKS.lock()[cpu_index()].as_ref().unwrap().thread_id();
    terminate_thread(thread_id);
  });
}

pub fn terminate_process(pid: u32) {
  arch::interrupts::without_interrupts(|| {
    let mut running_tasks = RUNNING_TASKS.lock();
    let mut task_queue = TASK_QUEUE.lock();
    let mut sleeping_tasks = SLEEPING_TASKS.lock();

    let mut running_threads = false;

    if let Some(thread) = running_tasks[cpu_index()].as_mut()
      && thread.process_id() == pid
    {
      // TODO: need to also search other CPUs for process threads, for the shiny future when SMP support is a thing
      thread.set_state(TaskState::Zombie);
      running_threads = true;
    }

    for thread in task_queue.iter_mut().filter(|thread| thread.process_id() == pid) {
      thread.set_state(TaskState::Zombie);
    }

    for thread in sleeping_tasks.iter_mut().filter(|sleepy_thread| sleepy_thread.thread_ref().process_id() == pid) {
      thread.thread_mut().set_state(TaskState::Zombie);
    }

    drop(sleeping_tasks);
    drop(task_queue);
    drop(running_tasks);

    if running_threads {
      schedule();
    }
  });
}

#[allow(clippy::missing_panics_doc)]
pub fn terminate_current_process() {
  arch::interrupts::without_interrupts(|| {
    let pid = RUNNING_TASKS.lock()[cpu_index()].as_ref().unwrap().process_id();
    terminate_process(pid);
  });
}

#[allow(clippy::missing_panics_doc)]
pub fn schedule() {
  arch::interrupts::without_interrupts(|| {
    let running_tasks = RUNNING_TASKS.lock();
    let mut task_queue = TASK_QUEUE.lock();
    let mut idle_tasks = IDLE_TASKS.lock();

    let next_task = if task_queue.front().is_none() {
      // No tasks to switch to, so switch to the idle task if we aren't already on it
      if let Some(thread) = idle_tasks[cpu_index()].take() {
        thread
      } else {
        return;
      }
    } else {
      task_queue.pop_front().unwrap()
    };

    drop(idle_tasks);
    drop(task_queue);
    drop(running_tasks);
    switch_to_task(next_task, false);
  });
}

#[allow(clippy::missing_panics_doc)]
pub fn block_task(cause: BlockCause) {
  arch::interrupts::without_interrupts(|| {
    RUNNING_TASKS.lock()[cpu_index()].as_mut().unwrap().set_state(TaskState::Blocked(cause));

    schedule();
  });
}

/// Unblocks a thread by process+thread ID.
///
/// # Errors
/// `TaskUnblockError::TaskNotBlocked` if the task is not blocked
/// `TaskUnblockError::TaskNotFound` if the task cannot be found in the blocked tasks list
#[allow(clippy::missing_panics_doc)]
pub fn unblock_task(pid: u32, tid: u32) -> Result<(), TaskUnblockError> {
  arch::interrupts::without_interrupts(|| {
    let running_tasks = RUNNING_TASKS.lock();
    let mut task_queue = TASK_QUEUE.lock();

    if let Some(index) = task_queue.iter().position(|x| x.process_id() == pid && x.thread_id() == tid) {
      // core::mem::discriminant compares enum variants without respect to what is inside, so it doesn't matter what BlockCause we use here
      if discriminant(&task_queue[index].state()) == discriminant(&TaskState::Blocked(BlockCause::Sleeping(0))) {
        let mut task = task_queue.remove(index).unwrap();

        task.set_state(TaskState::ReadyToRun);

        // TODO: use some logic here to determine if we should preempt the running task
        let should_preempt = false;

        if should_preempt {
          drop(task_queue);
          drop(running_tasks);
          switch_to_task(task, true);
        } else {
          task_queue.push_back(task);
        }

        Ok(())
      } else {
        Err(TaskUnblockError::TaskNotBlocked)
      }
    } else {
      Err(TaskUnblockError::TaskNotFound)
    }
  })
}

#[allow(clippy::missing_panics_doc)]
pub fn wake_tasks() {
  if let Some(running_tasks) = RUNNING_TASKS.try_lock()
    && let Some(mut task_queue) = TASK_QUEUE.try_lock()
    && let Some(mut sleeping_tasks) = SLEEPING_TASKS.try_lock()
  {
    let current_time = time_since_boot();

    // TODO: decide this dynamically somehow
    let should_preempt = false;
    let mut next_task = None;

    while let Some(task) = sleeping_tasks.peek()
      && u128::from(task.wake_time()) <= current_time.as_nanos()
    {
      let mut task = sleeping_tasks.pop().unwrap().task();

      trace!("Task is ready to wake: {}.{}", task.process_id(), task.thread_id());
      task.set_state(TaskState::ReadyToRun);
      if should_preempt && next_task.is_none() {
        next_task = Some(task);
      } else {
        task_queue.push_front(task);
      }
    }

    if let Some(next_task) = next_task {
      // Preempt running task with one of the tasks that just woke
      drop(sleeping_tasks);
      drop(task_queue);
      drop(running_tasks);
      switch_to_task(next_task, true);
    }
  }
}

/// Updates the timeslice remaining for the currently running task
pub fn update_timeslice() {
  if let Some(mut running_tasks) = RUNNING_TASKS.try_lock() {
    let current_task = running_tasks[cpu_index()].as_mut().expect("There was no task running!");
    if !is_idle_task(current_task) {
      current_task.decrement_timeslice(TICK_INTERVAL);
    }
  }
}

/// Preempts the currently running task if it has used up its entire timeslice
pub fn preempt_running_task() {
  if let Some(running_tasks) = RUNNING_TASKS.try_lock() {
    if let Some(task_queue) = TASK_QUEUE.try_lock() {
      let current_task = running_tasks[cpu_index()].as_ref().expect("There was no task running!");

      if task_queue.front().is_some() && current_task.timeslice() == Duration::ZERO {
        debug!("Task {}.{} has used up all its time, switch", current_task.process_id(), current_task.thread_id());
        drop(task_queue);
        drop(running_tasks);
        schedule();
      }
    } else {
      debug!("Task queue was already locked");
    }
  } else {
    debug!("Running tasks were already locked");
  }
}

fn switch_to_task(task: Thread<ArchData>, preempting: bool) {
  let mut running_tasks_lock = RUNNING_TASKS.lock();
  let mut task_queue_lock = TASK_QUEUE.lock();
  let mut sleeping_tasks_lock = SLEEPING_TASKS.lock();
  let mut idle_tasks_lock = IDLE_TASKS.lock();
  let mut zombie_tasks_lock = ZOMBIE_TASKS.lock();

  let old_task = {
    // There should always be a value in the running tasks array, so we need to take it out of there, and move it into its new home.
    let current_task = running_tasks_lock[cpu_index()].take().unwrap();

    // Put the current task (which we are switching away from) into the appropriate home based on its state,
    // and return a reference to it.
    if let TaskState::Blocked(cause) = current_task.state() {
      // Task is blocked, don't push it to the normal task queue
      if discriminant(&cause) == discriminant(&BlockCause::Sleeping(0)) {
        // Current task is going to sleep, put it on the sleeping task queue
        let sleepy_task = SleepingThread::new(cause.wake_time(), current_task);
        sleeping_tasks_lock.push(sleepy_task).thread_mut()
      } else {
        // Current task is blocked for some other reason, put it on the other blocked tasks queue
        // TODO: don't put blocked tasks on the runnable task queue
        task_queue_lock.push_back(current_task);
        task_queue_lock.back_mut().unwrap()
      }
    } else if current_task.state() == TaskState::Zombie {
      zombie_tasks_lock.push(current_task);
      zombie_tasks_lock.last_mut().unwrap()
    } else {
      // Task is not blocked, put it on the front/back of the task queue (depending on whether we are preempting it)
      if is_idle_task(&current_task) {
        // This task is the idle task, don't just put it back on the task queue
        idle_tasks_lock[cpu_index()] = Some(current_task);
        idle_tasks_lock[cpu_index()].as_mut().unwrap()
      } else {
        // Not the idle task, so put it on the normal task queue
        if preempting {
          task_queue_lock.push_front(current_task);
          task_queue_lock.front_mut().unwrap()
        } else {
          task_queue_lock.push_back(current_task);
          task_queue_lock.back_mut().unwrap().set_timeslice(DEFAULT_TIMESLICE);
          task_queue_lock.back_mut().unwrap()
        }
      }
    }
  };

  // Now put the new task (that we are switching to) in the running tasks array, and get a reference to it.
  running_tasks_lock[cpu_index()] = Some(task);
  let next_task = running_tasks_lock[cpu_index()].as_mut().unwrap();

  old_task.update_running_time();

  task_switch(old_task, next_task);
}

pub fn sleep_until(ns_since_boot: u64) {
  block_task(BlockCause::Sleeping(ns_since_boot));
}

/// Puts the current running thread to sleep for a `Duration` of `sleep_time`.
///
/// # Errors
/// Returns an error if the wake-up time (in nanoseconds since boot) would be greater than `u64::MAX`
#[allow(clippy::missing_panics_doc)]
pub fn sleep(sleep_time: Duration) -> Result<(), SleepError> {
  arch::interrupts::without_interrupts(|| {
    let curr_time = time_since_boot();
    let wake_time = curr_time + sleep_time;
    // There is no point in having more granular wake times than the tick interval, so truncate everything to that value
    let wake_time_ns = (wake_time.as_nanos() / TICK_INTERVAL.as_nanos()) * TICK_INTERVAL.as_nanos();

    if wake_time_ns > u128::from(u64::MAX) {
      warn!("Given a sleep time of {:?}, but cannot sleep that long as current time is {:?} and the target time will not fit into a u64", sleep_time, curr_time);
      Err(SleepError::DurationTooLong)
    } else {
      sleep_until(wake_time_ns.try_into().unwrap());
      Ok(())
    }
  })
}

fn is_idle_task<T: ArchDataTrait>(task: &Thread<T>) -> bool {
  task.process_id() == KERNEL_PID && task.thread_id() == IDLE_TASK_TID
}

#[allow(clippy::missing_panics_doc)]
pub fn report_task_status() {
  arch::interrupts::without_interrupts(|| {
    if let Some(mut running_tasks_lock) = RUNNING_TASKS.try_lock()
      && let Some(task_queue_lock) = TASK_QUEUE.try_lock()
      && let Some(sleeping_tasks_lock) = SLEEPING_TASKS.try_lock()
      && let Some(mut idle_tasks_lock) = IDLE_TASKS.try_lock()
    {
      let current_task = if let Some(task) = running_tasks_lock[cpu_index()].as_mut() {
        task
      } else {
        idle_tasks_lock[cpu_index()].as_mut().unwrap()
      };

      current_task.update_running_time();
      let current_task_runtime = current_task.time_running();
      if is_idle_task(current_task) {
        debug!(
          "Current task: IDLE, time running: {}.{:0>9}s",
          current_task_runtime.as_secs(),
          current_task_runtime.subsec_nanos()
        );
      } else {
        debug!(
          "Current task: {}.{}, time running: {}.{:0>9}s",
          current_task.process_id(),
          current_task.thread_id(),
          current_task_runtime.as_secs(),
          current_task_runtime.subsec_nanos()
        );
      }

      if let Some(task) = idle_tasks_lock[cpu_index()].as_ref() {
        let task_runtime = task.time_running();
        debug!("Idle task pending, time running: {}.{:0>9}s", task_runtime.as_secs(), task_runtime.subsec_nanos());
      }

      for task in task_queue_lock.iter() {
        let task_runtime = task.time_running();
        debug!(
          "Pending task: {}.{}, time running: {}.{:0>9}s",
          task.process_id(),
          task.thread_id(),
          task_runtime.as_secs(),
          task_runtime.subsec_nanos()
        );
      }

      for task in sleeping_tasks_lock.iter() {
        let sleepy_task = task.thread_ref();
        let task_runtime = sleepy_task.time_running();
        debug!(
          "Sleeping task: {}.{}, time running: {}.{:0>9}s",
          sleepy_task.process_id(),
          sleepy_task.thread_id(),
          task_runtime.as_secs(),
          task_runtime.subsec_nanos()
        );
      }
    } else {
      debug!("Unable to report task status, one or more mutexes are locked");
    }
  });
}

pub extern "C" fn kill_zombies() -> ! {
  loop {
    let zombie_thread = ZOMBIE_TASKS.lock().pop();
    if let Some(thread) = zombie_thread {
      let pid = thread.process_id();
      let tid = thread.thread_id();

      debug!("Tearing down thread {thread:?}");
      teardown_thread(thread);

      let thread_match = |thread: &Thread<ArchData>| thread.process_id() == pid && thread.thread_id() != tid;

      let killed_last_thread = arch::interrupts::without_interrupts(|| {
        let running_tasks = RUNNING_TASKS.lock();
        let task_queue = TASK_QUEUE.lock();
        let sleeping_tasks = SLEEPING_TASKS.lock();
        let idle_tasks = IDLE_TASKS.lock();
        let zombies = ZOMBIE_TASKS.lock();

        let killed_last_thread = !(sleeping_tasks.iter().map(SleepingThread::thread_ref).any(thread_match)
          && task_queue.iter().any(thread_match)
          && running_tasks.iter().filter_map(Option::as_ref).any(thread_match)
          && zombies.iter().any(thread_match));

        drop(zombies);
        drop(idle_tasks);
        drop(sleeping_tasks);
        drop(task_queue);
        drop(running_tasks);

        killed_last_thread
      });

      if killed_last_thread {
        debug!("Killed last thread for process {pid}, terminating process");
        let process = PROCESSES.lock().remove(&pid).expect("Somehow process did not exist in processes list");
        teardown_process(process);
      }
    } else {
      trace!("No more zombies to kill, going to sleep");
      sleep(Duration::from_millis(500)).expect("Error sleeping zombie killer thread");
    }
  }
}
