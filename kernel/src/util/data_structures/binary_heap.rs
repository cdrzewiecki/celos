use alloc::alloc::{alloc, dealloc};
use core::{
  alloc::Layout,
  mem::{align_of, size_of},
  ops::{Deref, DerefMut},
  ptr::{copy_nonoverlapping, swap, NonNull},
};

const DEFAULT_SIZE: usize = 8;

pub struct BinaryHeap<T: Ord> {
  base_ptr: NonNull<T>,
  capacity: usize,
  size: usize,
  heap_type: HeapType,
}

impl<T: Ord> BinaryHeap<T> {
  pub const fn new(heap_type: HeapType) -> Self {
    Self {
      base_ptr: NonNull::dangling(),
      capacity: 0,
      size: 0,
      heap_type,
    }
  }

  /// Reserve enough space for `new_capacity` elements in the heap. If the current capacity is already
  /// at least equal to `new_capacity`, this function does nothing.
  #[allow(clippy::missing_panics_doc)]
  pub fn reserve(&mut self, new_capacity: usize) {
    if self.capacity < new_capacity {
      let old_layout = match Layout::from_size_align(self.capacity * size_of::<T>(), align_of::<T>()) {
        Ok(layout) => layout,
        Err(e) => panic!("Layout error: {:?}", e),
      };

      let new_layout = match Layout::from_size_align(new_capacity * size_of::<T>(), align_of::<T>()) {
        Ok(layout) => layout,
        Err(e) => panic!("Layout error: {:?}", e),
      };

      let new_ptr = unsafe { alloc(new_layout).cast::<T>() };
      assert!(!new_ptr.is_null(), "Unable to allocate memory to expand BinaryHeap");

      let old_ptr = self.base_ptr.as_ptr();
      self.base_ptr = unsafe { NonNull::new_unchecked(new_ptr) };

      if self.capacity > 0 {
        unsafe {
          copy_nonoverlapping::<T>(old_ptr, new_ptr, self.size);
        }

        unsafe {
          dealloc(old_ptr.cast::<u8>(), old_layout);
        }
      }

      self.capacity = new_capacity;
    }
  }

  #[allow(clippy::missing_panics_doc)]
  pub fn push(&mut self, item: T) -> &mut T {
    if self.capacity == self.size {
      let new_capacity = if self.capacity == 0 { 1 } else { 2 * self.capacity };

      self.reserve(new_capacity);
    }

    unsafe {
      let mut node_ptr = self.base_ptr.as_ptr().add(self.size);
      node_ptr.write(item);
      self.size += 1;

      if self.size > 1 {
        let mut parent_offset = (self.size / 2) - 1;
        let mut parent_ptr = self.base_ptr.as_ptr().add(parent_offset);

        let f = match self.heap_type {
          HeapType::Min => T::lt,
          HeapType::Max => T::gt,
        };

        while f(node_ptr.as_ref().unwrap(), parent_ptr.as_ref().unwrap()) {
          swap(node_ptr, parent_ptr);

          node_ptr = parent_ptr;
          if parent_offset == 0 {
            break;
          }

          parent_offset = if parent_offset % 2 == 0 {
            (parent_offset / 2).saturating_sub(1)
          } else {
            parent_offset / 2
          };
          parent_ptr = self.base_ptr.as_ptr().add(parent_offset);
        }
      }

      node_ptr.as_mut().unwrap()
    }
  }

  #[allow(clippy::missing_panics_doc)]
  #[allow(clippy::comparison_chain)]
  pub fn pop(&mut self) -> Option<T> {
    if self.size > 0 {
      unsafe {
        // Get item to pop from the heap, then move the most recently added item into the first position of the heap
        let returned_item = self.base_ptr.as_ptr().read();
        self.size -= 1;
        self.base_ptr.as_ptr().write(self.base_ptr.as_ptr().add(self.size).read());

        // Now (if there's more than one item left) walk the new first node down the heap until it is either greater than
        // or smaller than the nodes below it (depending on heap mode).
        if self.size == 2 {
          let child_ptr = self.base_ptr.as_ptr().add(1);

          let f = match self.heap_type {
            HeapType::Min => T::lt,
            HeapType::Max => T::gt,
          };

          if !f(self.base_ptr.as_ref(), child_ptr.as_ref().unwrap()) {
            swap(self.base_ptr.as_mut(), child_ptr);
          }
        } else if self.size > 2 {
          let mut node_ptr = self.base_ptr.as_ptr();
          let mut node_offset = 0;
          let mut left_child_ptr = self.base_ptr.as_ptr().add(node_offset + 1);
          let mut right_child_ptr = self.base_ptr.as_ptr().add(node_offset + 2);

          let f = match self.heap_type {
            HeapType::Min => T::lt,
            HeapType::Max => T::gt,
          };

          let mut chosen_child_ptr = if f(left_child_ptr.as_ref().unwrap(), right_child_ptr.as_ref().unwrap()) {
            node_offset += 1;
            left_child_ptr
          } else {
            node_offset += 2;
            right_child_ptr
          };

          while !f(node_ptr.as_ref().unwrap(), chosen_child_ptr.as_ref().unwrap()) {
            swap(node_ptr, chosen_child_ptr);
            node_ptr = chosen_child_ptr;

            // The offset of this node's children would be node_offset + (size / 2) and node_offset + (size / 2) + 1. If either
            // of those offsets is >= size, then that offset would point past the end of the array which is backing the heap.
            // (remember: zero based indexing, so the index of the last element is size - 1)
            //
            // Also note: if either child offset would be past the end of the array, they can't have children (since we fill
            // the tree from left to right on each level). So in that case, no need to update the variables the loop is using.
            let left_offset = (node_offset * 2) + 1;
            let right_offset = left_offset + 1;
            if left_offset < self.size && right_offset < self.size {
              left_child_ptr = self.base_ptr.as_ptr().add(left_offset);
              right_child_ptr = self.base_ptr.as_ptr().add(right_offset);

              chosen_child_ptr = if f(left_child_ptr.as_ref().unwrap(), right_child_ptr.as_ref().unwrap()) {
                node_offset = left_offset;
                left_child_ptr
              } else {
                node_offset = right_offset;
                right_child_ptr
              };
            } else {
              // No child nodes left, break out of the loop
              break;
            }
          }

          // At this point we've pushed the new top node as far down the tree as it needs to go.
        }

        Some(returned_item)
      }
    } else {
      None
    }
  }

  pub fn peek(&self) -> Option<&T> {
    if self.size > 0 {
      unsafe { Some(self.base_ptr.as_ref()) }
    } else {
      None
    }
  }
}

impl<T: Ord> Drop for BinaryHeap<T> {
  fn drop(&mut self) {
    if self.capacity > 0 {
      // Drop any elements still left in the heap
      while self.pop().is_some() {}

      // Deallocate the backing memory
      let layout =
        Layout::from_size_align(self.capacity * size_of::<T>(), align_of::<T>()).expect("Error building alignment");
      unsafe {
        dealloc(self.base_ptr.as_ptr().cast::<u8>(), layout);
      }
    }
  }
}

impl<T: Ord> Deref for BinaryHeap<T> {
  type Target = [T];

  fn deref(&self) -> &[T] {
    unsafe { core::slice::from_raw_parts(self.base_ptr.as_ptr(), self.size) }
  }
}

impl<T: Ord> DerefMut for BinaryHeap<T> {
  fn deref_mut(&mut self) -> &mut Self::Target {
    unsafe { core::slice::from_raw_parts_mut(self.base_ptr.as_ptr(), self.size) }
  }
}

unsafe impl<T: Send + Ord> Send for BinaryHeap<T> {}
unsafe impl<T: Sync + Ord> Sync for BinaryHeap<T> {}

#[derive(Clone, Copy, Debug)]
pub enum HeapType {
  Min,
  Max,
}
