pub mod data_structures;

pub fn power_of_two(x: u64) -> bool {
  (x & (x - 1)) == 0 && x != 0
}
