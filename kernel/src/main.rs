#![no_std]
#![no_main]

use core::{
  panic::PanicInfo,
  sync::atomic::{AtomicBool, Ordering},
};

use celos_kernel::{
  arch,
  arch::{PhysAddr, VirtAddr},
  cpu, interrupts, memory, multitasking, print, println, serial_println, vga_buffer,
};
use log::{debug, Level};

use crate::{concurrency::Locked, logging::KernelLogger};

mod concurrency;
mod logging;

static VIDEO_INITIALIZED: AtomicBool = AtomicBool::new(false);
static LOGGER: Locked<KernelLogger> = Locked::new(KernelLogger::new());
const LOG_LEVEL: Level = Level::Debug;

#[no_mangle]
pub extern "C" fn _start(
  multiboot_info_address: usize,
  kernel_virt_start: u64,
  kernel_phys_start: u64,
  kernel_virt_end: u64,
  kernel_phys_end: u64,
) -> ! {
  kernel_main(multiboot_info_address, kernel_virt_start, kernel_phys_start, kernel_virt_end, kernel_phys_end);
}

fn kernel_main(
  multiboot_info_address: usize,
  kernel_virt_start: u64,
  kernel_phys_start: u64,
  kernel_virt_end: u64,
  kernel_phys_end: u64,
) -> ! {
  LOGGER.lock().enable_serial();
  LOGGER.lock().set_level(LOG_LEVEL);
  log::set_logger(&LOGGER).expect("Unable to set logger");

  log::set_max_level(LOG_LEVEL.to_level_filter());

  let boot_info = unsafe { multiboot2::load(multiboot_info_address).expect("Error loading multiboot info struct") };

  let kernel_virt_start = VirtAddr::new(kernel_virt_start);
  let kernel_phys_start = PhysAddr::new(kernel_phys_start);
  let kernel_virt_end = VirtAddr::new(kernel_virt_end);
  let kernel_phys_end = PhysAddr::new(kernel_phys_end);

  cpu::init();
  interrupts::init();

  debug!("Kernel physical start {kernel_phys_start:#x}, end {kernel_phys_end:#x}");
  debug!("Kernel virtual start {kernel_virt_start:#x}, end {kernel_virt_end:#x}");
  debug!("Stack starts at {:?}", unsafe { init_translate_addr(VirtAddr::new(0xffff_ffff_ffff_fff0 - (4096 * 4))) });
  debug!("Stack ends at {:?}", unsafe { init_translate_addr(VirtAddr::new(0xffff_ffff_ffff_fff0)) });

  unsafe {
    memory::init(&boot_info, kernel_virt_start, kernel_virt_end, kernel_phys_start, kernel_phys_end);
  }

  // We have to reload the multiboot info struct because now the identity mapping from 0-1GB is unmapped, so it has to be loaded
  // with the physical memory offset set by the memory module.
  let boot_info = unsafe {
    multiboot2::load_with_offset(multiboot_info_address, memory::PHYSICAL_MEMORY_OFFSET.as_u64().try_into().unwrap())
      .expect("Error loading multiboot info struct")
  };

  // Init video
  let framebuffer_info = boot_info.framebuffer_tag().expect("No framebuffer tag found");

  unsafe {
    vga_buffer::init(&framebuffer_info);
  }

  VIDEO_INITIALIZED.store(true, Ordering::Release);

  LOGGER.lock().enable_console();

  println!("CelOS v{}", env!("CARGO_PKG_VERSION"));
  println!("Init interrupts...\t\t\t[ok]");
  println!("Init memory...\t\t\t\t[ok]");
  println!("Init video...\t\t\t\t[ok]");

  // On x86_64, we can use ACPI to get information about the hardware.
  #[cfg(target_arch = "x86_64")]
  {
    arch::acpi::init(&boot_info);
  }

  interrupts::init_stage2();

  print!("Init multitasking...");
  multitasking::init();
  println!("\t\t[ok]");

  arch::init_background_threads().expect("Error spawning arch-specific background threads");

  #[cfg(target_arch = "x86_64")]
  loop {
    multitasking::schedule();
    x86_64::instructions::interrupts::enable_and_hlt();
  }
}

#[cfg(target_arch = "x86_64")]
unsafe fn init_translate_addr(addr: VirtAddr) -> PhysAddr {
  use x86_64::{
    registers::control::Cr3,
    structures::paging::{PageTable, PageTableFlags},
  };

  let (p4_frame, _) = Cr3::read();
  let p4_address = p4_frame.start_address().as_u64();

  let p4 = &*(p4_address as *const PageTable);
  let p4_index = addr.p4_index();

  let p3_address = p4[p4_index].frame().unwrap().start_address().as_u64();
  let p3 = &*(p3_address as *const PageTable);
  let p3_index = addr.p3_index();

  let p3_entry = &p3[p3_index];
  let p3_flags = p3_entry.flags();
  if p3_flags.contains(PageTableFlags::HUGE_PAGE) {
    panic!("Huge page in P3 entry when translating kernel end");
  } else {
    let p2_address = p3_entry.frame().unwrap().start_address().as_u64();
    let p2 = &*(p2_address as *const PageTable);
    let p2_index = addr.p2_index();

    let p2_entry = &p2[p2_index];
    let p2_flags = p2_entry.flags();
    if p2_flags.contains(PageTableFlags::HUGE_PAGE) {
      let offset = addr.as_u64() & 0o777_7777;
      p2_entry.addr() + offset
    } else {
      let p1_address = p2_entry.frame().unwrap().start_address().as_u64();
      let p1 = &*(p1_address as *const PageTable);
      let p1_index = addr.p1_index();

      let offset = addr.as_u64() & 0o7777;
      p1[p1_index].addr() + offset
    }
  }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
  serial_println!("{}", info);
  if VIDEO_INITIALIZED.load(Ordering::Acquire) {
    println!("{}", info);
  }

  #[cfg(target_arch = "x86_64")]
  loop {
    x86_64::instructions::interrupts::without_interrupts(x86_64::instructions::hlt);
  }
}
